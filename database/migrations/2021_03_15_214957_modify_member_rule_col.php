<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyMemberRuleCol extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('member_group', function (Blueprint $table) {
            //
            $table->string('is_login',10)->default('on')->nullable()->after('is_public')->comment('登入管理');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('member_group', function (Blueprint $table) {
            //
        });
    }
}
