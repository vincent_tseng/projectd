<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMemberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member', function (Blueprint $table) {
            $table->id();
            $table->string('member_name',30)->comment('使用者帳號');
            $table->string('member_email',100)->comment('使用者Email');
            $table->string('member_group_id',10)->comment('使用者群組ID');
            $table->string('member_passwd',50)->comment('使用者密碼');
            $table->string('member_status',1)->comment('使用者狀態');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member');
    }
}
