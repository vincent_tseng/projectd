<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColInMember extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('member', function (Blueprint $table) {
            //
            $table->string('company_name',20)->nullable()->after('member_token');
            $table->string('contact_name',20)->nullable()->after('company_name');
            $table->string('contact_email',100)->nullable()->after('contact_name');
            $table->text('note')->nullable()->after('contact_email');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('member', function (Blueprint $table) {
            //
        });
    }
}
