<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class MemberGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('member_group')->insert([
            [
                'member_group_name' => 'Super Admin',
                'member_group_note' => 'System Admin',
                'member_group_role' => '{"brands":["ALL"],"allow_manager":["ALL"],"allow_actions":["1","2","3","4","5"]}',
                'is_public' => 'on'
            ],
            [
                'member_group_name' => 'New Account',
                'member_group_note' => 'New Account Request',
                'member_group_role' => '{"brands":[],"allow_manager":[],"allow_actions":[]}',
                'is_public' => Null
            ]
        ]);
    }
}
