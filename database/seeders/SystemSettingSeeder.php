<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class SystemSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('system_set')->insert(
            [
                [
                    'system_key' => 'maintain',
                    'system_value' => NULL,
                ],
                [
                    'system_key' => 'policy_url',
                    'system_value' => NULL,
                ],
                [
                    'system_key' => 'service_url',
                    'system_value' => NULL,
                ],
                [
                    'system_key' => 'service_email',
                    'system_value' => NULL,
                ],
            ]
        );
    }
}
