<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Hash;

class RootSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('member')->insert(
            [
                'member_name' => 'root',
                'member_email' => 'root@admin',
                'member_group_id' => 1,
                'member_passwd' => Hash::make('root@admin'),
                'member_status' => 'on'
            ]
        );

    }
}
