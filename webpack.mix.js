const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .postCss('resources/css/app.css', 'public/css', [
        //
    ]);

mix.styles(
    [
        'resources/css/bootstrap-datetimepicker.min.css',
        'resources/css/font-face.css',
        'resources/css/theme.css'
    ],
    'public/css/cooladmin.css'
);

mix.styles(
    [
        'resources/vendor/font-awesome-4.7/css/font-awesome.min.css',
        'resources/vendor/font-awesome-5/css/fontawesome-all.min.css',
        'resources/vendor/mdi-font/css/material-design-iconic-font.min.css',
        'resources/vendor/animsition/animsition.min.css',
        'resources/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css',
        'resources/vendor/wow/animate.css',
        'resources/vendor/css-hamburgers/hamburgers.min.css',
        'resources/vendor/slick/slick.css',
        'resources/vendor/select2/select2.min.css',
        'resources/vendor/perfect-scrollbar/perfect-scrollbar.css',
    ],
    'public/css/coolvender.css'
);

mix.js('resources/js/bootstrap-datetimepicker.js', 'public/datetimepicker.js');
mix.js('resources/js/main.js', 'public/cooladmin_main.js');
mix.js('resources/js/moment.js', 'public/moment.js');

mix.js('resources/vendor/*/*.js','public/coolvendor.js');
