# 🚀 專案安裝與使用說明

專案clone之後，依照下方所列指令依序執行，必須先行安裝composer 
```php
composer install 
cp .env.example .env
php artisan key:generate  
```

之後打開.env，將資料庫相關設定填入：

```
DB_CONNECTION=mysql
DB_HOST={{你的Mysql ip}}
DB_PORT=3306
DB_DATABASE={{資料庫名稱}}
DB_USERNAME={{你的資料庫帳號}}
DB_PASSWORD={{你的資料庫密碼}}
```

信箱寄信設定
```
MAIL_MAILER=smtp
MAIL_HOST={{你的信箱位置}}
MAIL_PORT={{你的信箱port}}
MAIL_USERNAME={{郵件伺服器帳號}}
MAIL_PASSWORD={{郵件伺服器密碼}}
MAIL_ENCRYPTION={{郵件伺服器加密方式}}
MAIL_FROM_ADDRESS={{顯示的email帳號}}
MAIL_FROM_NAME="${APP_NAME}"
```

設定完成之後，請接著執行

```
php artisan storage:link
php artisan migrate
php artisan db:seed --class=MemberGroupSeeder
php artisan db:seed --class=RootSeeder
php artisan db:seed --class=SystemSettingSeeder
```


上述步驟都完成之後，即可登入系統

預設root帳號: root@admin

預設root密碼: root@admin
