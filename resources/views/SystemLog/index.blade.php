@extends('layouts.content')

@section('sidebar')
    @include('layouts.sidebar')
@endsection

@section('main-content')
    <div class="dashboard-title position-relative">
        <h1>System Log</h1>
    </div>
    <div class="main-content list-content" id="app">
        <div class="d-flex justify-content-between mb-3">
            <form action="/systemlog/index" method="post" id="search_bar">
            @csrf
            <div class="d-flex dashboard-select">
                <div>
                    <label class="form-label" for="">Type</label>
                    <select class="form-select" name="log_type" id="log_type" onchange="search_bar()">
                            <option value="0" selected="selected">All TYPE</option>
                        @foreach($options as $key => $item)
                            @if($log_type == $item)
                                <option value="{{$key}}" selected>{{$item}}</option>
                            @else
                                <option value="{{$key}}">{{$item}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
{{--            <div class="d-flex align-items-center">--}}
{{--                <div class="search-input position-relative">--}}
{{--                    <input type="search" class="form-control" placeholder="Search keywords">--}}
{{--                    <img class="position-absolute" src="{{asset('images/icon-search.svg')}}" alt="">--}}
{{--                </div>--}}
{{--            </div>--}}
            </form>
        </div>
        <table class="table table-borderless table-striped">
            <thead>
            <tr>
                <th>Date</th>
                <th>Type</th>
                <th>Action</th>
                <th>User</th>
            </tr>
            </thead>
            <tbody>
            @if($logs)
                @foreach($logs as $item)
                    <tr class="tr-shadow">
                        <td>
                            @if($item->updated_at)
                                {{date('Y-m_d H:i:s',strtotime($item->updated_at))}}
                            @else
                                {{date('Y-m-d H:i:s',strtotime($item->created_at))}}
                            @endif
                        </td>
                        <td>
                            {{$item->log_type}}
                        </td>
                        <td>
                            <span class="block-email">{{$item->log_action}}</span>
                        </td>
                        <td>
                            {{$item->user_name}}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{$logs->appends(['log_type'=> $log_type ])->links()}}
        @else
            <div class="div_empty">
                Logs Are Empty.
            </div>
        @endif
    </div>
@endsection

@section('page-javascripts')
    <script>
        function search_bar()
        {
            $("#search_bar").submit();
        }
    </script>
@endsection
