@extends('layouts.content')

@section('sidebar')
    @include('layouts.sidebar')
@endsection

@section('main-content')
    <div class="dashboard-title position-relative">
        <h1>Brand Manager</h1>
        <a class="btn btn-primary position-absolute text-white" onclick="add_new()">ADD NEW</a>
    </div>
    <div class="main-content list-content" id="app">
        <h2>All Brands</h2>
        @if(count($brands) > 0 )
            <table class="table table-borderless table-striped">
                <thead>
                <tr>
                    <th style="width: 80px">
                        Order
                    </th>
                    <th style="width: 160px">Sort Name</th>
                    <th>Brand Name</th>
                    <th style="width: 100px">Status</th>
                    <th class="text-center" style="width: 90px">Manage</th>
                </tr>
                </thead>
                <tbody id="brand_table">

                    @foreach($brands as $item)
                        <tr id="{{$item['id']}}">
                            <td><img class="icon-20" src="{{asset('images/icon-sort.svg')}}" alt=""></td>
                            <td>{{$item['brand_short_name']}}</td>
                            <td>{{$item['brand_name']}}</td>
                            <td>
                                @if($item['is_public'] == 'on' )
                                    <div class="status-circle status-public"></div>
                                    <b class="status status-text-public">PUBLIC</b>
                                @else
                                    <div class="status-circle status-draft"></div>
                                    <b class="status status-text-draft">DRAFT</b>
                                @endif
                            </td>
                            <td class="text-center">
                                <a class="cursor-pointer" onclick="modify({{$item['id']}})">
                                    <svg class="svg-icon">
                                        <use xlink:href="#edit"></use>
                                    </svg>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </tbody>
            </table>
        @else
            <div class="div_empty">
                The Folder Is Empty!
            </div>
        @endif
    </div>
@endsection

@section('page-javascripts')
    <script language="javascript">

        $(document).ready(function(){
            $("#brand_table").tableDnD({
                onDragClass: "drag-gray-style",
                onDrop:function(table,row){
                    sort_order();
                }
            });
        })

        function sort_order()
        {
            let i = 0
            let neworder = []
            $("tbody > tr").each(function(){
                i = i + 1
                orderarray = {"brand_id": $(this).attr('id'), "neworder": i}
                neworder.push(orderarray)
            })

            change_arr(neworder)
        }

        function add_new()
        {
            window.location = '/brand/new'
        }

        function modify(idx)
        {
            window.location = '/brand/modify/'+idx
        }

        function change_arr(arr)
        {
            datas = JSON.stringify(arr)
            console.log(datas)
            //後續等後端加上資料庫，並使用AJAX更新
            $.ajax({
                type: 'POST',
                url: '/brand/order',
                data: {"_token": "{{ csrf_token() }}", "orders": datas},
                success: function(msg)
                {
                    console.log(msg)
                }
            })
        }
    </script>
@endsection
