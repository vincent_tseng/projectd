@extends('layouts.content')

@section('sidebar')
    @include('layouts.sidebar')
@endsection

@section('main-content')

    <div class="mask" id="mask">
        <div class="d-flex justify-content-center align-items-center">
            <div class="delete-confirm-modal text-center">
                <div>
                    <img style="width: 100px" src="{{asset('images/icon-close-danger.svg')}}" alt="">
                </div>
                <div class="confirm-words">Are you sure ?</div>
                <div style="margin-bottom: 60px;">Do you really want to delete this item?<br>This process cannot be undone.</div>
                <div>
                    <button class="btn btn-red" type="button" onclick="btn_delete()">DELETE</button>
                    <button class="btn btn-gray" type="button" onclick="btn_delete_cancel()">CANCEL</button>
                </div>
            </div>
        </div>
    </div>

    <div class="dashboard-title position-relative">
        <h1>Brand Manager <span>/ Edit Content</span></h1>
    </div>
    <div class="main-content" id="app">
        <form action="/brand/modify" id="new_brand" method="post" enctype="multipart/form-data" class="form-horizontal">
            @csrf
            <input type="hidden" id="renew" name="renew" value="0">
            <input type="hidden" id="brand_id" name="brand_id" value="{{$brand['id']}}">
            <div class="container-fluid p-0">
                <div class="row">
                    <div class="input-panel">
                        <div class="mb-3">
                            <label for="" class="form-label">Brand Name</label>
                            <input type="text" id="brand_name" name="brand_name" value="{{$brand['brand_name']}}"
                                   class="form-control" maxlength="20">
                            <span id="msg_brand_name"></span>
                        </div>
                        <div class="mb-3">
                            <label for="" class="form-label">Short Name</label>
                            <input type="text" id="brand_short_name" name="brand_short_name"
                                   value="{{$brand['brand_short_name']}}" class="form-control">
                            <span id="msg_brand_short_name" maxlength="20"></span>
                        </div>
                        <div class="mb-3">
                            <div class="light-area d-flex justify-content-between align-items-center">
                                <label for="" class="form-label m-0">Public?</label>
                                <label class="switch">
                                    <input type="checkbox" id="is_public" name="is_public">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="control-btns d-flex justify-content-center">
                <div>
                    <button class="btn btn-small btn-outline-primary" type="button" onclick="save_new()">SAVE & NEW</button>
                    <button class="btn btn-small btn-primary" type="button" onclick="btn_submit()">SAVE</button>
                </div>
                    <button class="btn-red btn btn-small btn-right" type="button" onclick="btn_delete_show()">DELETE</button>
            </div>
        </form>
    </div>
@endsection

@section('page-javascripts')
    <script language="javascript">

        $(document).ready(function () {
            var is_public = '{{$brand['is_public']}}'
            if (is_public == 'on') {
                $("#is_public").attr('checked', 'checked')
            }
        })

        function btn_submit() {
            let check_flag = submit_check()
            if (check_flag) {
                $("#new_brand").submit()
            }

            return false
        }

        function save_new() {
            let check_flag = submit_check()
            console.log(check_flag)
            if (check_flag) {
                $("#renew").val(1)
                $("#new_brand").submit()
            }

            return false
        }

        function submit_check() {
            if ($("#brand_name").val() == '') {
                $("#msg_brand_name").html('<small class="form-text text-muted"><font color="red"> Do not empty.</font></small>')
                return false
            }

            if ($("#brand_short_name").val() == '') {
                $("#msg_brand_short_name").html('<small class="form-text text-muted"><font color="red">Do not empty.</font></small>')
                return false
            }

            return true
        }

        function btn_delete()
        {
            $("#new_brand").attr('action','/brand/delete')
            $("#new_brand").submit()
        }

        function btn_delete_show()
        {
            $("#mask").fadeIn(300)
        }

        function btn_delete_cancel()
        {
            $("#mask").fadeOut(300)
        }


    </script>
@endsection
