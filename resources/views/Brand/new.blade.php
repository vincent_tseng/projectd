@extends('layouts.content')

@section('sidebar')
    @include('layouts.sidebar')
@endsection

@section('main-content')
    <div class="dashboard-title position-relative">
        <h1>Field Manager <span>/ New Content</span></h1>
    </div>
    <div class="main-content" id="app">
        <form action="/brand/new" id="new_brand" method="post" enctype="multipart/form-data" class="form-horizontal">
            @csrf
            <input type="hidden" id="renew" name="renew" value="0">
            <input type="hidden" name="brand_name_check" id="brand_name_check">
        <div class="container-fluid p-0">
            <div class="row">
                <div class="input-panel">
                    <div class="mb-3">
                        <label for="" class="form-label">Brand Name</label>
                        <input type="text" id="brand_name" name="brand_name" placeholder="Brand Name?" class="form-control" onblur="check_brand_name()" maxlength="20">
                        <span id="msg_brand_name"></span>
                    </div>
                    <div class="mb-3">
                        <label for="" class="form-label">Short Name</label>
                        <input type="text" id="brand_short_name" name="brand_short_name" placeholder="Short Name?" class="form-control" maxlength="20">
                        <span id="msg_brand_short_name"></span>

                    </div>
                    <div class="mb-3">
                        <div class="light-area d-flex justify-content-between align-items-center">
                            <label for="" class="form-label m-0">Public?</label>
                            <label class="switch">
                                <input type="checkbox" id="is_public" name="is_public">
                                <span class="slider round"></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="control-btns d-flex justify-content-center">
            <button class="btn btn-outline-primary btn-small" type="button" onclick="save_new()">SAVE & NEW</button>
            <button class="btn btn-primary btn-small" type="button" onclick="btn_submit()">SAVE</button>
        </div>
        </form>
    </div>
@endsection

@section('page-javascripts')
    <script language="javascript">
        function btn_submit()
        {
            let check_flag = submit_check()
            if(check_flag)
            {
                $("#new_brand").submit()
            }

            return false
        }

        function save_new()
        {
            let check_flag = submit_check()
            if(check_flag)
            {
                $("#renew").val(1)
                $("#new_brand").submit()
            }

            return false
        }

        function submit_check()
        {
            check_brand_name();

            if($("#brand_name").val() == '')
            {
                $("#msg_brand_name").html('<small class="form-text text-muted"><font color="red"> Do not empty.</font></small>')
                return false
            }
            $("#msg_brand_name").html('')

            if($("#brand_short_name").val() == '')
            {
                $("#msg_brand_short_name").html('<small class="form-text text-muted"><font color="red">Do not empty.</font></small>')
                return false
            }
            $("#msg_brand_short_name").html('')

            if($("#brand_name_check").val() == 0 )
            {
                return false
            }

            return true
        }

        function check_brand_name()
        {
            let brand_name = $("#brand_name").val();

            //後續等後端加上資料庫，並使用AJAX更新
            $.ajax({
                type: 'POST',
                url: '/brand/check',
                data: {"_token": "{{ csrf_token() }}", "brand_name": brand_name},
                success: function(msg)
                {
                    console.log(msg)
                    if(msg == 'Brand Name Already Exists.')
                    {
                        $("#msg_brand_name").html('<small class="form-text text-muted"><font color="red">'+msg+'</font></small>')
                        $("#brand_name_check").val(0)
                    }
                    else
                    {
                        $("#msg_brand_name").html('<small class="form-text text-muted"><font color="green">'+msg+'</font></small>')
                        $("#brand_name_check").val(1)
                    }
                }
            })
        }
    </script>
@endsection
