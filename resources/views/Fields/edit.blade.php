@extends('layouts.content')

@section('sidebar')
    @include('layouts.sidebar')
@endsection

@section('main-content')

    <div class="mask" id="mask">
        <div class="d-flex justify-content-center align-items-center">
            <div class="delete-confirm-modal text-center">
                <div>
                    <img style="width: 100px" src="{{asset('images/icon-close-danger.svg')}}" alt="">
                </div>
                <div class="confirm-words">Are you sure ?</div>
                <div style="margin-bottom: 60px;">Do you really want to delete this item?<br>This process cannot be undone.</div>
                <div>
                    <button class="btn btn-red" type="button" onclick="btn_delete()">DELETE</button>
                    <button class="btn btn-gray" type="button" onclick="btn_delete_cancel()">CANCEL</button>
                </div>
            </div>
        </div>
    </div>

    <div class="dashboard-title position-relative">
        <h1>Field Manager</h1>
    </div>
    <div class="main-content" id="app">
        <h2>New Field</h2>
        @if(session('message'))
            <div class="warn-area">
                {{session('message')}}
            </div>
        @endif
        <div class="container-fluid p-0">
            <form action="/field/modify" id='field_form' method="post" enctype="multipart/form-data"
                  class="form-horizontal">
                <div class="">
                    @csrf
                    <input type="hidden" id="renew" name="renew" value="0">
                    <input type="hidden" id="field_id" name="field_id" value="{{$field['id']}}">
                    <div class="row">
                        <div class="input-panel">
                            <div class="mb-3">
                                <label for="" class="form-label">Name</label>
                                <input type="text" id="field_name" name="field_name" value="{{$field['field_name']}}"
                                       class="form-control" maxlength="30">
                                <span id="msg_field_name"></span>
                            </div>
                            {{--                    <div class="mb-3">--}}
                            {{--                        <label for="" class="form-label">Type</label>--}}
                            {{--                        <select class="form-select" name="" id="">--}}
                            {{--                            <option value="">Textarea</option>--}}
                            {{--                        </select>--}}
                            {{--                    </div>--}}
                            <div class="mb-3">
                                <label for="" class="form-label">Note</label>
                                <input type="text" id="field_note" name="field_note" value="{{$field['field_note']}}"
                                       class="form-control">
                                <span id="msg_field_note"></span>
                            </div>
                            <div class="mb-3">
                                <div class="light-area d-flex justify-content-between align-items-center">
                                    <label for="" class="form-label m-0">Required Filed</label>
                                    <label class="switch">
                                        <input type="checkbox" name="is_require" id="is_require">
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="light-area d-flex justify-content-between align-items-center">
                                    <label for="" class="form-label m-0">Include in search</label>
                                    <label class="switch">
                                        <input type="checkbox" id="is_search" name="is_search">
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="light-area d-flex justify-content-between align-items-center">
                                    <label for="" class="form-label m-0">Hide field</label>
                                    <label class="switch">
                                        <input type="checkbox" id="is_hide" name="is_hide">
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>
                            {{--                    <div class="mb-3">--}}
                            {{--                        <label for="" class="form-label">Maximum characters</label>--}}
                            {{--                        <input type="text" class="form-control">--}}
                            {{--                    </div>--}}
                            {{--                    <div class="mb-3">--}}
                            {{--                        <label for="" class="form-label">Parent Category</label>--}}
                            {{--                        <div class="mb-2">--}}
                            {{--                            <label class="radio">--}}
                            {{--                                <input type="radio" value="" name="parent" />--}}
                            {{--                                <span>Simple format</span>--}}
                            {{--                            </label>--}}
                            {{--                        </div>--}}
                            {{--                        <div class="mb-2">--}}
                            {{--                            <label class="radio">--}}
                            {{--                                <input type="radio" value="" name="parent" />--}}
                            {{--                                <span>None</span>--}}
                            {{--                            </label>--}}
                            {{--                        </div>--}}
                            {{--                        <div class="mb-2">--}}
                            {{--                            <label class="radio">--}}
                            {{--                                <input type="radio" value="" name="parent" />--}}
                            {{--                                <span>HTML</span>--}}
                            {{--                            </label>--}}
                            {{--                        </div>--}}
                            {{--                    </div>--}}
                            {{--                    <div class="mb-3">--}}
                            {{--                        <label for="" class="form-label">Allowed content</label>--}}
                            {{--                        <div class="mb-2">--}}
                            {{--                            <label class="radio">--}}
                            {{--                                <input type="radio" value="" name="content" />--}}
                            {{--                                <span>All</span>--}}
                            {{--                            </label>--}}
                            {{--                        </div>--}}
                            {{--                        <div class="mb-2">--}}
                            {{--                            <label class="radio">--}}
                            {{--                                <input type="radio" value="" name="content" />--}}
                            {{--                                <span>Number</span>--}}
                            {{--                            </label>--}}
                            {{--                        </div>--}}
                            {{--                        <div class="mb-2">--}}
                            {{--                            <label class="radio">--}}
                            {{--                                <input type="radio" value="" name="content" />--}}
                            {{--                                <span>Integer</span>--}}
                            {{--                            </label>--}}
                            {{--                        </div>--}}
                            {{--                        <div class="mb-2">--}}
                            {{--                            <label class="radio">--}}
                            {{--                                <input type="radio" value="" name="content" />--}}
                            {{--                                <span>Decimal</span>--}}
                            {{--                            </label>--}}
                            {{--                        </div>--}}
                            {{--                    </div>--}}
                        </div>
                    </div>
                </div>
                <div class="control-btns d-flex justify-content-between">
                    <div>
                        <button class="btn btn-outline-primary btn-small" type="button" onclick="save_new()">SAVE &
                            NEW
                        </button>
                        <button class="btn btn-primary btn-small" type="button" onclick="btn_submit()">SAVE</button>
                    </div>
                    <button class="btn-red btn btn-small" type="button" onclick="btn_delete_show()">DELETE</button>
                </div>
            </form>
        </div>
        @endsection

        @section('page-javascripts')
            <script language="javascript">
                $(document).ready(function () {
                    var is_require = '{{$field['is_require']}}'
                    if (is_require == 'on') {
                        $("#is_require").attr('checked', 'checked')
                    }

                    var is_search = '{{$field['is_search']}}'
                    if (is_search == 'on') {
                        $("#is_search").attr('checked', 'checked')
                    }

                    var is_hide = '{{$field['is_hide']}}'
                    if (is_hide == 'on') {
                        $("#is_hide").attr('checked', 'checked')
                    }
                })

                function btn_submit() {
                    let flag = submit_check()
                    console.log(flag)
                    if (flag) {
                        $("#field_form").submit()
                    }
                }

                function save_new() {
                    let flag = submit_check()
                    if (flag) {
                        $("#renew").val(1)
                        $("#field_form").submit()
                    }
                }

                function btn_delete()
                {
                    let check_flag = confirm('Are You Sure To Delete This?')
                    if(check_flag)
                    {
                        $("#field_form").attr('action','/field/delete')
                        $("#field_form").submit()
                    }
                }

                function btn_delete_show()
                {
                    $("#mask").fadeIn(300)
                }

                function btn_delete_cancel()
                {
                    $("#mask").fadeOut(300)
                }

                function submit_check() {
                    if ($("#field_name").val() == '') {
                        $("#msg_field_name").html('<small class="form-text text-muted"><font color="red"> Do not empty.</font></small>')
                        return false
                    }
                    $("#msg_field_name").html('')

                    if ($("#field_note").val() == '') {
                        $("#msg_field_note").html('<small class="form-text text-muted"><font color="red">Do not empty.</font></small>')
                        return false
                    }
                    $("#msg_field_note").html('')

                    return true
                }
            </script>
@endsection
