@extends('layouts.content')

@section('sidebar')
    @include('layouts.sidebar')
@endsection

@section('main-content')
    <div class="dashboard-title position-relative">
        <h1>Field Manager</h1>
        <a class="btn btn-primary position-absolute text-white" onclick="add_new()">ADD NEW</a>
    </div>
    <div class="main-content" id="app">
        <div class="d-flex align-items-end justify-content-between mb-3">
            <div class="input-panel">
                <label for="" class="form-label">Type</label>
                <select class="form-select" name="" id="">
                    <option value="">Textarea</option>
                </select>
            </div>
            <div class="search-input position-relative">
                <input type="search" class="form-control" placeholder="Search keywords">
                <img class="position-absolute" src="{{asset('images/icon-search.svg')}}" alt="">
            </div>
        </div>
        @if(count($fields) > 0 )
        <table class="table table-borderless table-striped">
            <thead>
            <tr>
                <th style="width: 80px">
                    Order
                </th>
                <th>Name</th>
                <th class="text-center" style="width: 90px">Manage</th>
            </tr>
            </thead>
            <tbody id="field_table">
                @foreach($fields as $item)
                    <tr id="{{$item['id']}}">
                        <td><img class="icon-20" src="{{asset('images/icon-sort.svg')}}" alt=""></td>
                        <td>{{$item['field_name']}}</td>
                        <td class="text-center">
                            <a class="cursor-pointer" onclick="modify({{$item['id']}})">
                                <svg class="svg-icon">
                                    <use xlink:href="#edit"></use>
                                </svg>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @else
            <div class="div_empty">
                The Folder Is Empty!
            </div>
        @endif
    </div>
@endsection

@section('page-javascripts')
    <script language="javascript">
        $(document).ready(function () {
            $("#field_table").tableDnD({
                onDragClass: "drag-gray-style",
                onDrop: function (table, row) {
                    sort_order();
                }
            });
        })

        function sort_order() {
            let i = 0
            let neworder = []
            $("tbody > tr").each(function () {
                i = i + 1
                orderarray = {"field_id": $(this).attr('id'), "neworder": i}
                neworder.push(orderarray)
            })

            change_arr(neworder)
        }

        function add_new() {
            window.location = '/field/new'
        }

        function modify(idx) {
            window.location = '/field/modify/' + idx
        }

        function change_arr(arr) {
            datas = JSON.stringify(arr)
            console.log(datas)
            //後續等後端加上資料庫，並使用AJAX更新
            $.ajax({
                type: 'POST',
                url: '/field/order',
                data: {"_token": "{{ csrf_token() }}", "orders": datas},
                success: function (msg) {
                    console.log(msg)
                }
            })
        }
    </script>
@endsection
