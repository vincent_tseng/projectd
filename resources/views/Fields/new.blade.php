@extends('layouts.content')

@section('sidebar')
    @include('layouts.sidebar')
@endsection

@section('main-content')
    <div class="dashboard-title position-relative">
        <h1>Field Manager</h1>
    </div>
    <div class="main-content" id="app">
        <h2>New Field</h2>
        @if(session('message'))
            <div class="warn-area">
                {{session('message')}}
            </div>
        @endif
        <form action="/field/new" id='field_form' method="post" enctype="multipart/form-data" class="form-horizontal">
            @csrf
            <input type="hidden" id="renew" name="renew" value="0">
            <input type="hidden" id="check_field" name="check_field" value="0">
        <div class="container-fluid p-0">
            <div class="row">
                <div class="input-panel">
                    <div class="mb-3">
                        <label for="" class="form-label">Name</label>
                        <input type="text" id="field_name" name="field_name" id="field_name" placeholder="Name?" class="form-control" onblur="check_field_name()" maxlength="30">
                        <span id="msg_field_name"></span>
                    </div>
{{--                    <div class="mb-3">--}}
{{--                        <label for="" class="form-label">Type</label>--}}
{{--                        <select class="form-select" name="" id="">--}}
{{--                            <option value="">Textarea</option>--}}
{{--                        </select>--}}
{{--                    </div>--}}
                    <div class="mb-3">
                        <label for="" class="form-label">Note</label>
                        <input type="text" id="field_note" name="field_note" id="field_note" placeholder="Note?" class="form-control">
                        <span id="msg_field_note"></span>
                    </div>
                    <div class="mb-3">
                        <div class="light-area d-flex justify-content-between align-items-center">
                            <label for="" class="form-label m-0">Required Filed</label>
                            <label class="switch">
                                <input type="checkbox" name="is_require" id="is_require">
                                <span class="slider round"></span>
                            </label>
                        </div>
                    </div>
                    <div class="mb-3">
                        <div class="light-area d-flex justify-content-between align-items-center">
                            <label for="" class="form-label m-0">Include in search</label>
                            <label class="switch">
                                <input type="checkbox" id="is_search" name="is_search">
                                <span class="slider round"></span>
                            </label>
                        </div>
                    </div>
                    <div class="mb-3">
                        <div class="light-area d-flex justify-content-between align-items-center">
                            <label for="" class="form-label m-0">Hide field</label>
                            <label class="switch">
                                <input type="checkbox" name="is_hide" id="is_hide">
                                <span class="slider round"></span>
                            </label>
                        </div>
                    </div>
{{--                    <div class="mb-3">--}}
{{--                        <label for="" class="form-label">Maximum characters</label>--}}
{{--                        <input type="text" class="form-control">--}}
{{--                    </div>--}}
{{--                    <div class="mb-3">--}}
{{--                        <label for="" class="form-label">Parent Category</label>--}}
{{--                        <div class="mb-2">--}}
{{--                            <label class="radio">--}}
{{--                                <input type="radio" value="" name="parent" />--}}
{{--                                <span>Simple format</span>--}}
{{--                            </label>--}}
{{--                        </div>--}}
{{--                        <div class="mb-2">--}}
{{--                            <label class="radio">--}}
{{--                                <input type="radio" value="" name="parent" />--}}
{{--                                <span>None</span>--}}
{{--                            </label>--}}
{{--                        </div>--}}
{{--                        <div class="mb-2">--}}
{{--                            <label class="radio">--}}
{{--                                <input type="radio" value="" name="parent" />--}}
{{--                                <span>HTML</span>--}}
{{--                            </label>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="mb-3">--}}
{{--                        <label for="" class="form-label">Allowed content</label>--}}
{{--                        <div class="mb-2">--}}
{{--                            <label class="radio">--}}
{{--                                <input type="radio" value="" name="content" />--}}
{{--                                <span>All</span>--}}
{{--                            </label>--}}
{{--                        </div>--}}
{{--                        <div class="mb-2">--}}
{{--                            <label class="radio">--}}
{{--                                <input type="radio" value="" name="content" />--}}
{{--                                <span>Number</span>--}}
{{--                            </label>--}}
{{--                        </div>--}}
{{--                        <div class="mb-2">--}}
{{--                            <label class="radio">--}}
{{--                                <input type="radio" value="" name="content" />--}}
{{--                                <span>Integer</span>--}}
{{--                            </label>--}}
{{--                        </div>--}}
{{--                        <div class="mb-2">--}}
{{--                            <label class="radio">--}}
{{--                                <input type="radio" value="" name="content" />--}}
{{--                                <span>Decimal</span>--}}
{{--                            </label>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
        <div class="control-btns">
            <button class="btn btn-outline-primary btn-small" type="button" onclick="save_new()">SAVE & NEW</button>
            <button class="btn btn-primary btn-small" type="button" onclick="btn_submit()">SAVE</button>
        </div>
        </form>
    </div>
@endsection

@section('page-javascripts')
    <script language="javascript">
        function btn_submit()
        {
            let flag = submit_check()
            console.log(flag)
            if(flag)
            {
                $("#field_form").submit()
            }
        }

        function save_new()
        {
            let flag = submit_check()
            if(flag)
            {
                $("#renew").val(1)
                $("#field_form").submit()
            }
        }

        function submit_check()
        {
            check_field_name();

            if($("#field_name").val() == '')
            {
                $("#msg_field_name").html('<small class="form-text text-muted"><font color="red"> Do not empty.</font></small>')
                return false
            }
            $("#msg_field_name").html('')

            if($("#field_note").val() == '')
            {
                $("#msg_field_note").html('<small class="form-text text-muted"><font color="red">Do not empty.</font></small>')
                return false
            }
            $("#msg_field_note").html('')

            if($("#check_field").val() == 0 )
            {
                return false
            }

            return true
        }

        function check_field_name()
        {
            let field_name = $("#field_name").val();

            //後續等後端加上資料庫，並使用AJAX更新
            $.ajax({
                type: 'POST',
                url: '/field/check',
                data: {"_token": "{{ csrf_token() }}", "field_name": field_name},
                success: function(msg)
                {
                    console.log(msg)
                    if(msg == 'Field Name Already Exists.')
                    {
                        $("#msg_field_name").html('<small class="form-text text-muted"><font color="red">'+msg+'</font></small>')
                        $("#check_field").val(0)
                    }
                    else
                    {
                        $("#msg_field_name").html('<small class="form-text text-muted"><font color="green">'+msg+'</font></small>')
                        $("#check_field").val(1)
                    }
                }
            })
        }
    </script>
@endsection
