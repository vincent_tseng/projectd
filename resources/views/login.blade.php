@include('layouts.header')
<body class="login-bg" style="min-height: 937px;">
<div class="login-header text-center fixed-top">
    <img class="" src="{{asset('images/Logo_b.svg')}}" alt="">
</div>
<div>
    <div class="text-center">
        <div class="login-sub">
            <img src="{{asset('images/digital_w.svg')}}" alt="">
        </div>
    </div>
    <div class="login-panel">
        <form action="/login" method="POST" id="login_form">
            <h2 class="text-center">Sign In</h2>
            @csrf
            <div class="login-input">
                <label for="email" class="form-label">Email</label>
                <input type="email" class="form-control" id="email" name="email">
                <div id="msg_login_email" class="login-msg"></div>
            </div>
            <div class="login-input position-relative">
                <a class="position-absolute" href="/forget_passwd">Forgot You Password</a>
                <label for="password" class="form-label">Password</label>
                <input type="password" class="form-control" id="passwd" name="passwd">
                <div id="msg_login_passwd" class="login-msg"></div>
            </div>
            <div class="login-sepline"></div>
            <div class="d-grid sign-in-btn">
                <button class="btn btn-primary btn-lg" type="submit">Sign In</button>
            </div>
            <div class="text-center">
                <a class="" href="/partner">Become a partner</a>
            </div>
        </form>
    </div>
    <div class="text-center login-bottom">
        <a href="{{$value['policy_url']}}" target="_blank">Privacy Policy</a> and <a
            href="{{$value['service_url']}}"
            target="_blank">Terms of
            Service</a>
    </div>
</div>
<script language="javascript">
    $(document).ready(function(){
        @if(session('message'))
            t_style = 'form-control error-input'
            $('#email').attr('class',t_style);
            $('#msg_login_email').html("{{session('message')}}")
        @endif
    })

    $("#email").blur(function(){
        if($(this).val() == '')
        {
            $('#email').attr('class','form-control error-input');
            $('#msg_login_email').html('<p><strong><font color="red"> Email Do not empty. </font></p>')
        }
        else
        {
            $('#email').attr('class','form-control');
            $('#msg_login_email').html('')
        }
    })

    $("#passwd").blur(function(){
        if($(this).val() == '')
        {
            $('#passwd').attr('class','form-control error-input');
            $('#msg_login_passwd').html('<p><strong><font color="red"> Password Do not empty. </font></p>')
        }
        else
        {
            $('#passwd').attr('class','form-control');
            $('#msg_login_passwd').html('')
        }
    })

    $("#login_form").submit(function(){

        if ($("#email").val() == '') {
            $('#email').attr('class','form-control error-input');
            $("#msg_login_email").html('<p><strong><font color="red"> Email Do not empty. </font></p>')
            return false;
        }

        if ($("#passwd").val() == '') {
            t_style = $('#passwd').attr('class') + ' error-input'
            $('#passwd').attr('class',t_style);

            $("#msg_login_passwd").html('<p><strong><font color="red"> Password Do not empty. </font></p>')
            return false
        }
    })
</script>
</body>

</html>
