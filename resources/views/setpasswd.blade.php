@include('layouts.header')
<body class="login-bg">
<div class="login-header text-center fixed-top">
    <img class="" src="{{asset('images/Logo_b.svg')}}" alt="">
</div>
<div>
    <div class="text-center">
        <div class="login-sub">
            <img src="{{asset('images/digital_w.svg')}}" alt="">
        </div>
    </div>
    <div class="login-panel">
        <form action="/forget_passwd/setting" method="post" id="setting">
            @csrf
            <input type="hidden" name="member_id" value="{{$member_id}}">
            <h1 class="text-center">Setting your password</h1>
            <div class="login-input">
                <label for="" class="form-label">Enter your password</label>
                <input type="password" class="form-control" id="passwd" name="passwd">
            </div>
            <div class="login-input">
                <label for="" class="form-label">Enter password again</label>
                <input type="password" class="form-control" id="confirm_passwd" name="confirm_passwd"
                       onblur="check_passwd()">
                <span id="check_passwd"></span>
            </div>
            <div class="d-grid">
                <button class="btn btn-primary btn-lg" type="button" onclick="btn_submit()">RESET PASSWORD</button>
            </div>
        </form>
    </div>
    <div class="text-center login-bottom">
        <a href="{{$value['policy_url']}}" target="_blank">Privacy Policy</a> and <a href="{{$value['service_url']}}"
                                                                                     target="_blank">Terms of
            service</a>
    </div>
</div>
</body>
<script>
    function btn_submit() {
        if (check_passwd()) {
            $("#setting").submit()
        }

        return false
    }

    function check_passwd() {
        let passwd = $("#passwd").val()
        let confirm_passwd = $("#confirm_passwd").val()

        if ((confirm_passwd != '') && (passwd != '')) {
            if (passwd == confirm_passwd) {
                $("#check_passwd").html('<small class="form-text text-muted"><font color="green">Checked Password Success.</font></small>')
                return 'OK'
            }
            $("#check_passwd").html('<small class="form-text text-muted"><font color="red">Checked Password False.</font></small>')
            return 'False'
        } else {
            $("#check_passwd").html('<small class="form-text text-muted"><font color="red">Password Do Not Empty.</font></small>')
        }
        return 'False'
    }
</script>
