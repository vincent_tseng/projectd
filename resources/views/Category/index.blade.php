@extends('layouts.content')

@section('sidebar')
    @include('layouts.sidebar')
@endsection

@section('main-content')
    <div class="dashboard-title position-relative">
        <h1>Category Manager</h1>
        <a class="btn btn-primary position-absolute text-white" onclick="add_new()">ADD NEW</a>
    </div>
    <div class="main-content list-content" id="app">
        <h2>All Categories</h2>
        @if(count($category) > 0)
            <table class="table table-borderless table-striped" id="category_table">
                <thead>
                <tr id="0">
                    <th style="width: 80px">
                        Order
                    </th>
                    <th>Name</th>
                    <th class="text-center" style="width: 90px">Manage</th>
                </tr>
                </thead>
                <tbody>
                @foreach($category as $item)
                    <tr id="{{$item['id']}}">
                        <td><img class="icon-20" src="{{asset('images/icon-sort.svg')}}" alt=""></td>
                        <td>{{$item['category_name']}}</td>
                        <td class="text-center">
                            <a class="cursor-pointer" onclick="modify({{$item['id']}})">
                                <svg class="svg-icon">
                                    <use xlink:href="#edit"></use>
                                </svg>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <div class="div_empty">
                The Folder Is Empty!
            </div>
        @endif
    </div>
@endsection

@section('page-javascripts')
    <script language="javascript">
        $(document).ready(function(){
            $("#category_table").tableDnD({
                onDragClass: "drag-gray-style",
                onDrop:function(table,row){
                    sort_order();
                }
            });
        })

        function sort_order()
        {
            let i = 0
            let neworder = []
            $("tbody > tr").each(function(){
                i = i + 1
                orderarray = {"category_id": $(this).attr('id'), "neworder": i}
                neworder.push(orderarray)
            })

            change_arr(neworder)
        }

        function change_arr(arr)
        {
            datas = JSON.stringify(arr)
            console.log(datas)
            //後續等後端加上資料庫，並使用AJAX更新
            $.ajax({
                type: 'POST',
                url: '/category/order',
                data: {"_token": "{{ csrf_token() }}", "orders": datas},
                success: function(msg)
                {
                    console.log(msg)
                }
            })
        }

        function add_new()
        {
            window.location = '/category/new'
        }

        function modify(idx)
        {
            window.location = '/category/modify/'+idx
        }

        function change_arr(arr)
        {
            datas = JSON.stringify(arr)
            console.log(datas)
            //後續等後端加上資料庫，並使用AJAX更新
            $.ajax({
                type: 'POST',
                url: '/category/order',
                data: {"_token": "{{ csrf_token() }}", "orders": datas},
                success: function(msg)
                {
                    console.log(msg)
                }
            })
        }
    </script>
@endsection
