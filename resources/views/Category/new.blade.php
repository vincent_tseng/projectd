@extends('layouts.content')

@section('sidebar')
    @include('layouts.sidebar')
@endsection

@section('main-content')
    <div class="dashboard-title position-relative">
        <h1>Category Manager <span>/ New Category</span></h1>
    </div>
    <div class="main-content" id="app">
        <form action="/category/new" id="category_form" method="post" enctype="multipart/form-data" class="form-horizontal">
            @csrf
            <input type="hidden" id="renew" name="renew" value="0">
            <input type="hidden" id="check_category" name="check_category" value="0">
        <div class="container-fluid p-0">
            <div class="row">
                <div class="input-panel">
                    <div class="mb-3">
                        <label for="" class="form-label">Category Name</label>
                        <input type="text" id="category_name" name="category_name" id="category_name" placeholder="Name?" class="form-control" onblur="check_category_name()" maxlength="20">
                        <span id="msg_category_name"></span>
                    </div>
                    <div class="mb-3">
                        <label for="" class="form-label">Note</label>
                        <input type="text" id="category_note" name="category_note" id="category_note" placeholder="Note?" class="form-control">
                        <span id="msg_category_note"></span>
                    </div>
{{--                    <div class="mb-3">--}}
{{--                        <label for="" class="form-label">Fields</label>--}}
{{--                        @foreach($fields as $item)--}}
{{--                            <div class="mb-2">--}}
{{--                                <label class="checkbox">--}}
{{--                                    <input type="checkbox" id="field" name="category_fields[]" value={{$item['id']}} class="form-check-input">--}}
{{--                                    <span>{{$item['field_name']}}</span>--}}
{{--                                </label>--}}
{{--                            </div>--}}
{{--                        @endforeach--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
        <div class="control-btns d-flex justify-content-center">
            <button class="btn btn-outline-primary" type="button" onclick="save_new()">SAVE & NEW</button>
            <button class="btn btn-primary" type="button" onclick="btn_submit()">SAVE</button>
        </div>
        </form>
    </div>
@endsection

@section('page-javascripts')
    <script language="javascript">
        function btn_submit()
        {
            let check_flag = submit_check()
            console.log(check_flag)
            if(check_flag)
            {
                $("#category_form").submit()
            }

            return false
        }

        function add_new()
        {
            window.location = '/category/new'
        }

        function save_new()
        {
            let check_flag = submit_check()
            if(check_flag)
            {
                $("#renew").val(1)
                $("#category_form").submit()
            }

            return false
        }

        function submit_check()
        {
            check_category_name();

            if($("#category_name").val() == '')
            {
                $("#msg_category_name").html('<small class="form-text text-muted"><font color="red"> Do not empty.</font></small>')
                return false
            }
            $("#msg_category_name").html('')

            if($("#category_note").val() == '')
            {
                $("#msg_category_note").html('<small class="form-text text-muted"><font color="red">Do not empty.</font></small>')
                return false
            }
            $("#msg_category_note").html('')

            // if($("input[name='category_fields[]']:checked").length <= 0)
            // {
            //     $("#msg_category_fields").html('<small class="form-text text-muted"><font color="red">Please Choose One.</font></small>')
            //     return false
            // }

            if($("#check_category").val() == 0 )
            {
                return false
            }

            return true
        }

        function check_category_name()
        {
            let category_name = $("#category_name").val();

            //後續等後端加上資料庫，並使用AJAX更新
            $.ajax({
                type: 'POST',
                url: '/category/check',
                data: {"_token": "{{ csrf_token() }}", "category_name": category_name},
                success: function(msg)
                {
                    console.log(msg)
                    if(msg == 'Category Name Already Exists.')
                    {
                        $("#msg_category_name").html('<small class="form-text text-muted"><font color="red">'+msg+'</font></small>')
                        $("#check_category").val(0)
                    }
                    else
                    {
                        $("#msg_category_name").html('<small class="form-text text-muted"><font color="green">'+msg+'</font></small>')
                        $("#check_category").val(1)
                    }
                }
            })
        }
    </script>
@endsection
