@extends('layouts.content')

@section('sidebar')
    @include('layouts.sidebar')
@endsection

@section('main-content')

    <div class="mask" id="mask">
        <div class="d-flex justify-content-center align-items-center">
            <div class="delete-confirm-modal text-center">
                <div>
                    <img style="width: 100px" src="{{asset('images/icon-close-danger.svg')}}" alt="">
                </div>
                <div class="confirm-words">Are you sure ?</div>
                <div style="margin-bottom: 60px;">Do you really want to delete this item?<br>This process cannot be undone.</div>
                <div>
                    <button class="btn btn-red" type="button" onclick="btn_delete()">DELETE</button>
                    <button class="btn btn-gray" type="button" onclick="btn_delete_cancel()">CANCEL</button>
                </div>
            </div>
        </div>
    </div>

    <div class="dashboard-title position-relative">
        <h1>Category Manager <span>/ Edit Category</span></h1>
    </div>
    <div class="main-content" id="app">
        <form action="/category/modify" id="category_form" method="post" enctype="multipart/form-data" class="form-horizontal">
            @csrf
            <input type="hidden" id="renew" name="renew" value="0">
            <input type="hidden" id="category_id" name="category_id" value="{{$category['id']}}">
        <div class="container-fluid p-0">
            <div class="row">
                <div class="input-panel">
                    <div class="mb-3">
                        <label for="" class="form-label">Brand Name</label>
                        <input type="text" id="category_name" name="category_name" value="{{$category['category_name']}}" class="form-control" maxlength="20">
                        <span id="msg_category_name"></span>
                    </div>
                    <div class="mb-3">
                        <label for="" class="form-label">Note</label>
                        <input type="text" id="category_note" name="category_note" value="{{$category['category_note']}}" class="form-control">
                        <span id="msg_category_note"></span>
                    </div>
{{--                    <div class="mb-3">--}}
{{--                        <label for="" class="form-label">Fields</label>--}}
{{--                        @foreach($fields as $item)--}}
{{--                            <div class="mb-2">--}}
{{--                                <label class="checkbox">--}}
{{--                                    <input type="checkbox" id="field" name="category_fields[]" value={{$item['id']}} class="form-check-input">--}}
{{--                                    <span>{{$item['field_name']}}</span>--}}
{{--                                </label>--}}
{{--                            </div>--}}
{{--                        @endforeach--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
        <div class="control-btns d-flex justify-content-center">
            <div>
                <button class="btn btn-outline-primary btn-small" type="button" onclick="save_new()">SAVE & NEW</button>
                <button class="btn btn-primary btn-small" type="button" onclick="btn_submit()">SAVE</button>
            </div>
            <button class="btn-red btn btn-small btn-right" type="button" onclick="btn_delete_show()">DELETE</button>

        </div>
        </form>
    </div>
@endsection

@section('page-javascripts')
    <script language="javascript">

        $(document).ready(function() {
            var fields = {!! $category['category_fields'] !!}
            $("input:checkbox").each(function(){
                if( $.inArray($(this).val(),fields) != -1)
                {
                    $(this).attr('checked','checked')
                }
                else
                {
                    $(this).attr('checked',false)
                }
            })
        })

        function btn_submit()
        {
            let check_flag = submit_check()
            console.log(check_flag)
            if(check_flag)
            {
                $("#category_form").submit()
            }

            return false
        }

        function save_new()
        {
            let check_flag = submit_check()
            if(check_flag)
            {
                $("#renew").val(1)
                $("#category_form").submit()
            }

            return false
        }

        function submit_check()
        {
            if($("#category_name").val() == '')
            {
                $("#msg_category_name").html('<small class="form-text text-muted"><font color="red"> Do not empty.</font></small>')
                return false
            }
            $("#msg_category_name").html('')

            if($("#category_note").val() == '')
            {
                $("#msg_category_note").html('<small class="form-text text-muted"><font color="red">Do not empty.</font></small>')
                return false
            }
            $("#msg_category_note").html('')

            // if($("input[name='category_fields[]']:checked").length <= 0)
            // {
            //     $("#msg_category_fields").html('<small class="form-text text-muted"><font color="red">Please Choose One.</font></small>')
            //     return false
            // }

            return true
        }

        function btn_delete()
        {
            $("#category_form").attr('action','/category/delete')
            $("#category_form").submit()
        }

        function btn_delete_show()
        {
            $("#mask").fadeIn(300)
        }

        function btn_delete_cancel()
        {
            $("#mask").fadeOut(300)
        }
    </script>
@endsection
