@extends('layouts.content')

@section('sidebar')
    @include('layouts.sidebar')
@endsection

@section('main-content')
    <div class="dashboard-title position-relative">
        <h1>Member Manager <span>/ New Member</span></h1>
    </div>
    <div class="main-content" id="app">
        <form action="/member/new" id="member_form" method="post" enctype="multipart/form-data" class="form-horizontal">
            @csrf
            <input type="hidden" name="renew" id="renew" value="0">
            <input type="hidden" name="member_check" id="member_check">
        <div class="container-fluid p-0">
            <div class="row">
                <div class="input-panel">
                    <div class="mb-3">
                        <label for="" class="form-label">Name</label>
                        <input type="text" id="member_name" name="member_name" placeholder="Name?" class="form-control" maxlength="30">
                        <span id="msg_member_name"></span>
                    </div>
                    <div class="mb-3">
                        <label for="" class="form-label">Member Group</label>
                        <select name="member_group_id" id="member_group_id" class="form-control">
                            <option value="0">Please select</option>
                            @foreach($memberGroups as $item)
                                <option value="{{$item['id']}}">{{$item['member_group_name']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3">
                        <div class="light-area d-flex justify-content-between align-items-center">
                            <label for="" class="form-label m-0">Member Status</label>
                            <label class="switch">
                                <input type="checkbox" name="member_status" id="member_status">
                                <span class="slider round"></span>
                            </label>
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="" class="form-label">Email</label>
                        <input type="text" id="member_email" name="member_email" placeholder="Email?" class="form-control" onblur="check_member()" maxlength="100">
                        <span id="msg_member_email"></span>
                    </div>
                    <div class="mb-3">
                        <label for="" class="form-label">Company Name</label>
                        <input type="text" id="company_name" name="company_name" class="form-control" maxlength="20">
                        <span id="msg_company_name"></span>
                    </div>
                    <div class="mb-3">
                        <label for="" class="form-label">Contact Name</label>
                        <input type="text" id="contact_name" name="contact_name" class="form-control" maxlength="20">
                        <span id="msg_contact_name"></span>
                    </div>
                    <div class="mb-3">
                        <label for="" class="form-label">Contact Email?</label>
                        <input type="text" id="contact_email" name="contact_email" class="form-control" maxlength="100">
                        <span id="msg_contact_email"></span>
                    </div>
                    <div class="mb-3">
                        <label for="" class="form-label">note</label>
                        <input type="text" id="note" name="note" class="form-control">
                        <span id="msg_note"></span>
                    </div>
                    <div class="mb-3">
                        <label for="" class="form-label">Password</label>
                        <input type="password" id="member_passwd" name="member_passwd" class="form-control">
                    </div>
                    <div class="mb-3">
                        <label for="" class="form-label">Confirm Password</label>
                        <input type="password" id="confirm_passwd" name="confirm_passwd" class="form-control" onblur="check_passwd()">
                        <span id="check_passwd"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="control-btns d-flex justify-content-center">
            <button class="btn btn-outline-primary btn-small" type="button" onclick="save_new()">SAVE & NEW</button>
            <button class="btn btn-primary btn-small" type="button" onclick="btn_submit()">SAVE</button>
        </div>
        </form>
    </div>
@endsection

@section('page-javascripts')
    <script language="javascript">
        function btn_submit()
        {
            let checks  = check_submit()
            if(checks)
            {
                $("#member_form").submit()
            }

            return false
        }

        function save_new()
        {
            $("#renew").val(1)
            let checks  = check_submit()
            if(checks)
            {
                $("#member_form").submit()
            }
            return false
        }

        function check_member()
        {
            let member_email = $("#member_email").val();

            //後續等後端加上資料庫，並使用AJAX更新
            $.ajax({
                type: 'POST',
                url: '/member/check_member',
                data: {"_token": "{{ csrf_token() }}", "member_email": member_email},
                success: function(msg)
                {
                    console.log(msg)
                    if(msg == 'Member Email Already Exists.')
                    {
                        $("#msg_member_email").html('<small class="form-text text-muted"><font color="red">'+msg+'</font></small>')
                        $("#member_check").val(0)
                    }
                    else
                    {
                        $("#msg_member_email").html('<small class="form-text text-muted"><font color="green">'+msg+'</font></small>')
                        $("#member_check").val(1)
                    }
                }
            })
        }

        function check_submit()
        {
            check_member();

            if($("#member_name").val() == '' )
            {
                $("#msg_member_name").html('<small class="form-text text-muted"><font color="red">Do not empty.</font></small>')
                return false
            }
            $("#msg_member_name").html('')

            if($("#member_group_id").val() == '0')
            {
                $("#msg_member_group").html('<small class="form-text text-muted"><font color="red">Please Choose One.</font></small>')
                return false
            }
            $("#msg_member_group").html('')

            if($("#member_email").val() == '' )
            {
                $("#msg_member_email").html('<small class="form-text text-muted"><font color="red">Do not empty.</font></small>')
                return false
            }
            $("#msg_member_email").html('')

            if($("#company_name").val() == '' )
            {
                $("#msg_company_name").html('<small class="form-text text-muted"><font color="red">Do not empty.</font></small>')
                return false
            }
            $("#msg_company_name").html('')

            if($("#contact_name").val() == '' )
            {
                $("#msg_contact_name").html('<small class="form-text text-muted"><font color="red">Do not empty.</font></small>')
                return false
            }
            $("#msg_contact_name").html('')

            if($("#contact_email").val() == '' )
            {
                $("#msg_contact_email").html('<small class="form-text text-muted"><font color="red">Do not empty.</font></small>')
                return false
            }
            $("#msg_contact_email").html('')

            if(check_passwd() == 'False')
            {
                return false
            }

            console.log($("#member_check").val())
            if($("#member_check").val() == 0 )
            {
                return false
            }

            return true
        }

        function check_passwd()
        {
            let passwd = $("#member_passwd").val()
            let confirm_passwd = $("#confirm_passwd").val()

            if( (confirm_passwd != '') && (passwd != ''))
            {
                if(passwd == confirm_passwd)
                {
                    $("#check_passwd").html('<small class="form-text text-muted"><font color="green">Checked Password Success.</font></small>')
                    return 'OK'
                }
                $("#check_passwd").html('<small class="form-text text-muted"><font color="red">Checked Password False.</font></small>')
                return 'False'
            }
            else
            {
                $("#check_passwd").html('<small class="form-text text-muted"><font color="red">Password Do Not Empty.</font></small>')
            }
            return 'False'
        }
    </script>
@endsection
