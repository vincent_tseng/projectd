@extends('layouts.content')

@section('sidebar')
    @include('layouts.sidebar')
@endsection

@section('main-content')
    <div class="dashboard-title position-relative">
        <h1>Member Manager</h1>
        <a class="btn btn-primary position-absolute text-white" onclick="add_new()">ADD NEW</a>
    </div>
    <div class="main-content list-content" id="app">
        <form action="/member/index" method="post" id="member_search">
        @csrf
        <div class="d-flex justify-content-between mb-3">
            <div class="d-flex dashboard-select">
                <div>
                    <label class="form-label" for="">Member Group</label>
                    <select class="form-select" name="member_groups" id="member_groups" onchange="search_bar()">
                        <option value="ALL">ALL Groups</option>
                        @foreach($memberGroups as $row)
                            @if((isset($used_member_group)) && ($used_member_group == $row['id']))
                                <option value="{{$row['id']}}" selected>{{$row['member_group_name']}}</option>
                            @else
                                <option value="{{$row['id']}}">{{$row['member_group_name']}}</option>
                            @endif

                        @endforeach
                    </select>
                </div>
                <div>
                    <label class="form-label" for="">Status</label>
                    <select class="form-select" name="member_status" id="member_status" onchange="search_bar()">
                        <option value="ALL">All Status</option>
                        @if((isset($used_member_status)) && ($used_member_status=='on'))
                            <option value="on" selected>ACTIVE</option>
                        @else
                            <option value="on">ACTIVE</option>
                        @endif

                        @if((isset($used_member_status)) && ($used_member_status=='0'))
                            <option value="0" selected>INACTIVE</option>
                        @else
                            <option value="0">INACTIVE</option>
                        @endif
                        @if((isset($used_member_status)) && ($used_member_status=='DELETED'))
                            <option value="DELETED" selected>DELETED</option>
                        @else
                            <option value="DELETED">DELETED</option>
                        @endif
                    </select>
                </div>
            </div>
            <div class="d-flex align-items-end">
                <div class="search-input position-relative">
                    <input type="search" class="form-control" placeholder="Search keywords" name="search_txt" value="{{$used_member_search_txt}}" onblur="search_bar()">
                    <img class="position-absolute" src="{{asset('images/icon-search.svg')}}" alt="">
                </div>
            </div>
        </div>
        </form>
        @if(count($members) > 0 )
        <table class="table table-borderless table-striped">
            <thead>
            <tr>
                <th style="width: 60px">ID</th>
                <th style="width: 80px">Name</th>
                <th style="width: 160px">Dates</th>
                <th>Member Group</th>
                <th style="width: 100px">Status</th>
                <th class="text-center" style="width: 90px">Manage</th>
            </tr>
            </thead>
            <tbody>
                @foreach($members as $item)
                    <tr>
                        <td>
                            {{$item['id']}}
                        </td>
                        <td>
                            <div class="table-data__info">
                                <h6>{{$item['member_name']}}</h6>
                                <span><a href="#">{{$item['member_email']}}</a></span>
                            </div>
                        </td>
                        <td>
                            @if(!is_null($item['updated_at']))
                                {{date('Y-m-d H:i',strtotime($item['updated_at']))}}
                            @else
                                {{date('Y-m-d H:i',strtotime($item['created_at']))}}
                            @endif
                        </td>
                        <td>
                            {{$item['member_group_name']}}
                        </td>
                        <td>
                            @if($item['is_delete'] == 0 )
                                @if($item['member_status'] == 'on')
                                    <div class="status-circle status-public"></div>
                                    <b class="status status-text-public member-status">ACTIVE</b>
                                @else
                                    <div class="status-circle status-banned"></div>
                                    <b class="status status-text-draft member-status">INACTIVE</b>
                                @endif
                            @else
                                <div class="status-circle status-banned"></div>
                                <b class="status status-text-banned member-status">DELETED</b>
                            @endif
                        </td>
                        <td class="text-center">
                            <a class="cursor-pointer" onclick="modify({{$item['id']}})">
                                <svg class="svg-icon">
                                    <use xlink:href="#edit"></use>
                                </svg>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{$members->appends($link_search)->links()}}
        @else
            <div class="div_empty">
                The Folder Is Empty!
            </div>
        @endif
    </div>
@endsection

@section('page-javascripts')
    <script language="javascript">
        function add_new()
        {
            window.location = '/member/new'
        }

        function modify(idx)
        {
            window.location = '/member/modify/'+idx
        }

        function search_bar()
        {
            $("#member_search").submit();
        }

    </script>
@endsection
