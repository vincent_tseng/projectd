@extends('layouts.content')

@section('sidebar')
    @include('layouts.sidebar')
@endsection

@section('main-content')

    <div class="mask" id="mask">
        <div class="d-flex justify-content-center align-items-center">
            <div class="delete-confirm-modal text-center">
                <div>
                    <img style="width: 100px" src="{{asset('images/icon-close-danger.svg')}}" alt="">
                </div>
                <div class="confirm-words">Are you sure ?</div>
                <div style="margin-bottom: 60px;">Do you really want to delete this item?<br>This process cannot be undone.</div>
                <div>
                    <button class="btn btn-red" type="button" onclick="btn_delete()">DELETE</button>
                    <button class="btn btn-gray" type="button" onclick="btn_delete_cancel()">CANCEL</button>
                </div>
            </div>
        </div>
    </div>

    <div class="dashboard-title position-relative">
        <h1>Member Manager <span>/ Edit Member</span></h1>
    </div>
    <div class="main-content" id="app">
        <form action="/member/modify" id="member_form" method="post" enctype="multipart/form-data" class="form-horizontal">
            @csrf
            <input type="hidden" name="renew" id="renew" value="0">
            <input type="hidden" name="member_id" value="{{$member['id']}}">
        <div class="container-fluid p-0">
            <div class="row">
                <div class="input-panel">
                    <div class="mb-3">
                        <label for="" class="form-label">Name</label>
                        <input type="text" id="member_name" name="member_name" placeholder="Name?" value="{{$member['member_name']}}" class="form-control" maxlength="30">
                        <span id="msg_member_name"></span>
                    </div>
                    <div class="mb-3">
                        <label for="" class="form-label">Member Group</label>
                        <select class="form-control" name="member_group_id" id="member_group_id">
                            <option value="0">Please select</option>
                            @foreach($memberGroups as $item)
                                @if($member['member_group_id'] == $item['id'])
                                    <option value="{{$item['id']}}" selected>{{$item['member_group_name']}}</option>
                                @else
                                    <option value="{{$item['id']}}">{{$item['member_group_name']}}</option>
                                @endif
                            @endforeach
                        </select>
                        @if($newaccount['id'] == $member['member_group_id'])
                            <span id="check_passwd"><small class="form-text text-muted"><font color="red">New Account Must Need A Role Group.</font></small></span>
                        @endif
                        <span id="msg_member_group"></span>
                    </div>
                    <div class="mb-3">
                        <div class="light-area d-flex justify-content-between align-items-center">
                            <label for="" class="form-label m-0">Member Status</label>
                            <label class="switch">
                                <input type="checkbox" name="member_status" id="member_status">
                                <span class="slider round"></span>
                            </label>
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="" class="form-label">Email</label>
                        <input type="text" id="member_email" name="member_email" placeholder="Email?" class="form-control" value="{{$member['member_email']}}" readonly maxlength="100">
                        <span id="msg_member_email"></span>
                    </div>

                    <div class="mb-3">
                        <label for="" class="form-label">Company Name</label>
                        <input type="text" id="company_name" name="company_name" class="form-control" value="{{$member['company_name']}}" maxlength="20">
                        <span id="msg_company_name"></span>
                    </div>
                    <div class="mb-3">
                        <label for="" class="form-label">Contact Name</label>
                        <input type="text" id="contact_name" name="contact_name" class="form-control" value="{{$member['contact_name']}}" maxlength="20">
                        <span id="msg_contact_name"></span>
                    </div>
                    <div class="mb-3">
                        <label for="" class="form-label">Contact Email?</label>
                        <input type="text" id="contact_email" name="contact_email" class="form-control" value="{{$member['contact_email']}}" maxlength="100">
                        <span id="msg_contact_email"></span>
                    </div>
                    <div class="mb-3">
                        <label for="" class="form-label">note</label>
                        <input type="text" id="note" name="note" class="form-control" value="{{$member['note']}}">
                        <span id="msg_note"></span>
                    </div>

                    <div class="mb-3">
                        <label for="" class="form-label">Password</label>
                        <input type="password" id="member_passwd" name="member_passwd" class="form-control">
                    </div>
                    <div class="mb-3">
                        <label for="" class="form-label">Confirm Password</label>
                        <input type="password" id="confirm_passwd" name="confirm_passwd" class="form-control" onblur="check_passwd()">
                        @if($newaccount['id'] == $member['member_group_id'])
                            <span id="check_passwd"><small class="form-text text-muted"><font color="red">New Account MUST Change Password.</font></small></span>
                        @else
                            <span id="check_passwd"><small class="form-text text-muted"><font color="green">If you don't want to change Password, Please Keep Empty.</font></small></span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="control-btns d-flex justify-content-center">
            <div>
                <button class="btn btn-outline-primary btn-small" type="button" onclick="save_new()">SAVE & NEW</button>
                <button class="btn btn-primary btn-small" type="button" onclick="btn_submit()">SAVE</button>
                @if($admincheck == 1 && $member['is_delete'] == 1 )
                    <button class="btn btn-primary btn-small btn-right" type="button" onclick="btn_restore()">RESTORE ACCOUNT</button>
                @endif
            </div>
            <button class="btn-red btn btn-small" onclick="btn_delete_show()" type="button">DELETE</button>

        </div>
        </form>
    </div>
@endsection

@section('page-javascripts')
    <script language="javascript">
        $(document).ready(function(){
            var member_status = '{{$member['member_status']}}'
            if( member_status == 'on' )
            {
                $("#member_status").attr('checked','checked')
            }
        })

        function btn_submit()
        {
            console.log(12345)
            let checks  = check_submit()
            if(checks)
            {
                $("#member_form").submit()
            }

            return false
        }

        function save_new()
        {
            $("#renew").val(1)
            let checks  = check_submit()
            if(checks)
            {
                $("#member_form").submit()
            }
            return false
        }

        function check_submit()
        {
            if($("#member_name").val() == '' )
            {
                $("#msg_member_name").html('<small class="form-text text-muted"><font color="red">Do not empty.</font></small>')
                return false
            }
            $("#msg_member_name").html('')

            if($("#member_group_id").val() == '0')
            {
                $("#msg_member_group").html('<small class="form-text text-muted"><font color="red">Please Choose One.</font></small>')
                return false
            }
            $("#msg_member_group").html('')

            if($("#member_email").val() == '' )
            {
                $("#msg_member_email").html('<small class="form-text text-muted"><font color="red">Do not empty.</font></small>')
                return false
            }
            $("#msg_member_email").html('')

            if($("#company_name").val() == '' )
            {
                $("#msg_company_name").html('<small class="form-text text-muted"><font color="red">Do not empty.</font></small>')
                return false
            }
            $("#msg_company_name").html('')

            if($("#contact_name").val() == '' )
            {
                $("#msg_contact_name").html('<small class="form-text text-muted"><font color="red">Do not empty.</font></small>')
                return false
            }
            $("#msg_contact_name").html('')

            if($("#contact_email").val() == '' )
            {
                $("#msg_contact_email").html('<small class="form-text text-muted"><font color="red">Do not empty.</font></small>')
                return false
            }
            $("#msg_contact_email").html('')

            if(check_passwd() == 'False')
            {
                return false
            }

            return true
        }

        function check_member()
        {
            let member_email = $("#member_email").val();

            //後續等後端加上資料庫，並使用AJAX更新
            $.ajax({
                type: 'POST',
                url: '/member/check_member',
                data: {"_token": "{{ csrf_token() }}", "member_email": member_email},
                success: function(msg)
                {
                    console.log(msg)
                    if(msg == 'Member Email Already Exists.')
                    {
                        $("#msg_member_email").html('<small class="form-text text-muted"><font color="red">'+msg+'</font></small>')
                        $("#member_check").val(0)
                    }
                    else
                    {
                        $("#msg_member_email").html('<small class="form-text text-muted"><font color="green">'+msg+'</font></small>')
                        $("#member_check").val(1)
                    }
                }
            })
        }

        function check_passwd()
        {
            let passwd = $("#member_passwd").val()
            let confirm_passwd = $("#confirm_passwd").val()
            if(passwd == confirm_passwd)
            {
                $("#check_passwd").html('<small class="form-text text-muted"><font color="green">Checked Password Success.</font></small>')
                return 'OK'
            }
            $("#check_passwd").html('<small class="form-text text-muted"><font color="red">Checked Password False.</font></small>')
            return 'False'
        }

        function btn_delete()
        {
            $("#member_form").attr('action','/member/delete')
            $("#member_form").submit()
        }

        function btn_restore()
        {
            $("#member_form").attr('action','/member/restore')
            $("#member_form").submit()
        }

        function btn_delete_show()
        {
            $("#mask").fadeIn(300)
        }

        function btn_delete_cancel()
        {
            $("#mask").fadeOut(300)
        }

    </script>
@endsection
