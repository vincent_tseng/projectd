@include('layouts.header')
<body class="login-bg">
<div class="login-header text-center fixed-top">
    <img class="" src="{{asset('images/Logo_b.svg')}}" alt="">
</div>
<div>
    <div class="text-center">
        <div class="login-sub">
            <img src="{{asset('images/digital_w.svg')}}" alt="">
        </div>
    </div>
    <div class="login-panel">
        <h1 class="text-center">Thank you!</h1>
        <div class="text-center">{{ $message }}</div>
        <div class="d-grid" style="margin-top: 40px;">
            <a class="btn btn-primary btn-lg text-white justify-content-center btn-block d-flex align-items-center"
               role="button" href="/">BACK TO LOGIN</a>
        </div>
    </div>
</div>
</body>
<script>

</script>
