@if ($paginator->hasPages())
    <nav role="navigation" aria-label="{{ __('Pagination Navigation') }}" class="flex items-center justify-between">
        <div class="pages d-flex justify-content-center">
            <div class="d-flex align-items-center">
{{--                <span>Total:{{ $paginator->total() }}</span>--}}

                {{-- Previous Page Link --}}
                @if ($paginator->onFirstPage())
                    <img class="page-arrow" src="{{asset('images/icon-prev.svg')}}" alt="">
                @else
                    <a href="{{ $paginator->previousPageUrl() }}" rel="prev" class=""
                       aria-label="{{ __('pagination.previous') }}">
                        <img class="page-arrow" src="{{asset('images/icon-prev.svg')}}" alt="">
                    </a>
                @endif

{{--                @foreach ($elements as $element)--}}
{{--                    --}}{{-- "Three Dots" Separator --}}
{{--                    @if (is_string($element))--}}
{{--                        <span aria-disabled="true">--}}
{{--                                <a><img src="{{asset('images/icon-more.svg')}}" alt=""></a>--}}
{{--                            </span>--}}
{{--                    @endif--}}

{{--                    --}}{{-- Array Of Links --}}
{{--                    @if (is_array($element))--}}
{{--                        @foreach ($element as $page => $url)--}}
{{--                            @if ($page == $paginator->currentPage())--}}
{{--                                <span aria-current="page" class="page-item active">{{ $page }}</span>--}}
{{--                            @else--}}
{{--                                <a href="{{ $url }}" class="page-item"--}}
{{--                                   aria-label="{{ __('Go to page :page', ['page' => $page]) }}">--}}
{{--                                    {{ $page }}--}}
{{--                                </a>--}}
{{--                            @endif--}}
{{--                        @endforeach--}}
                            <div>
                                @if($paginator->currentPage() == 1 )
                                    1 - {{ ($paginator->currentPage()*10) }}
                                @else
                                    {{ (($paginator->currentPage()-1)*10) +1 }} - {{ (($paginator->currentPage())*10) }}
                                @endif
                            </div>
                            <div class="text-nowrap ms-1"> of {{ $paginator->total() }}</div>
{{--                    @endif--}}
{{--                @endforeach--}}
                {{-- Next Page Link --}}
                @if ($paginator->hasMorePages())
                    <a href="{{ $paginator->nextPageUrl() }}" rel="next" class=""
                       aria-label="{{ __('pagination.next') }}">
                        <img class="page-arrow" src="{{asset('images/icon-next.svg')}}" alt="">
                    </a>
                @else
                    <img class="page-arrow" src="{{asset('images/icon-next.svg')}}" alt="">
                @endif

{{--                <div class="text-nowrap">jump to <input type="text" class="" onblur="jumpto(this.value)"></div>--}}
            </div>
        </div>
    </nav>
@endif
