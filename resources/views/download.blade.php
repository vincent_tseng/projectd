@include('layouts.header')
<body>
<svg class="d-none">
    <symbol id="left-arrow" viewBox="0 0">
        <path fill="currentColor" d="M9.8,13.3c-0.1,0-0.3,0-0.4-0.1L3.8,7.6C3.7,7.4,3.7,7,3.8,6.8l5.7-5.7c0.2-0.2,0.5-0.2,0.7,0s0.2,0.5,0,0.7
		L4.9,7.2l5.2,5.2c0.2,0.2,0.2,0.5,0,0.7C10.1,13.3,9.9,13.3,9.8,13.3z"/>
    </symbol>
    <symbol id="right-arrow" viewBox="0 0">
        <path fill="currentColor" d="M4.2,13.4c-0.1,0-0.3,0-0.4-0.1c-0.2-0.2-0.2-0.5,0-0.7l5.3-5.3L3.8,1.9c-0.2-0.2-0.2-0.5,0-0.7s0.5-0.2,0.7,0
		l5.7,5.7c0.2,0.2,0.2,0.5,0,0.7l-5.7,5.7C4.5,13.4,4.3,13.4,4.2,13.4z"/>
    </symbol>
    <symbol id="view" viewBox="0 0 24 24">
        <g>
            <path d="M3 13.5V13.5C6.44074 5.66276 17.5593 5.66276 21 13.5V13.5" stroke="currentColor" stroke-width="2"
                  fill="none"/>
            <circle cx="12" cy="14" r="4" fill="currentColor"/>
        </g>
    </symbol>
    <symbol id="download" viewBox="0 0 24 24">
        <path fill-rule="evenodd" clip-rule="evenodd" d="M3 19H21V21H3V19Z" fill="currentColor"/>
        <path d="M7 11L12 16L17 11" stroke="currentColor" fill="none" stroke-width="2"/>
        <path d="M12 16V4" stroke="currentColor" fill="none" stroke-width="2" stroke-linejoin="round"/>
    </symbol>
</svg>
<div class="container">
    <header class="control-header d-flex justify-content-between">
        <div class="d-flex align-items-center position-relative download-page-logo" style="height: 44px">
            <img class="control-logo" src="{{asset('images/Logo_b.svg')}}" alt="">
            <span class="control-title"><img src="{{asset('images/control-title.svg')}}" alt=""></span>
        </div>
        <div class="d-flex control-right align-items-center">
            @if($access_control == 'on')
                <a href="{{$control_url}}">Control Panel</a>
            @endif
            <a href="/logout">Sign Out</a>
        </div>
    </header>
    <div class="row">
        <aside class="col-3 download-aside">
            <div class="control-subtitle">
                Brand
            </div>
            <ul class="list-unstyled control-side-list" id="brand_id">
                @foreach( $brands as $brand)
                    @if($user['member_group']['brands'][0] == 'ALL')
                        @if(in_array($brand['id'],$used_brand_id))
                            <li brandid="{{$brand['id']}}" class="active cpointer">{{$brand['brand_name']}}</li>
                        @else
                            <li brandid="{{$brand['id']}}" class="cpointer">{{$brand['brand_name']}}</li>
                        @endif
                    @else
                        @if(in_array($brand['id'],$user['member_group']['brands']))
                            @if(in_array($brand['id'],$used_brand_id))
                                <li brandid="{{$brand['id']}}" class="active">{{$brand['brand_name']}}</li>
                            @else
                                <li brandid="{{$brand['id']}}">{{$brand['brand_name']}}</li>
                            @endif
                        @endif
                    @endif
                @endforeach
            </ul>
            <div class="control-subtitle">
                Category
            </div>
            <ul class="list-unstyled control-side-list" id="category_id">
                @foreach( $categories as $category)
                    @if(in_array($category['id'],$used_category_id))
                        <li categoryid="{{$category['id']}}" class="active cpointer">{{$category['category_name']}}</li>
                    @else
                        <li categoryid="{{$category['id']}}" class="cpointer">{{$category['category_name']}}</li>
                    @endif
                @endforeach
            </ul>
        </aside>
        <div class="col-9">
            <form action="/download" method="post" id="download_search">
                @csrf
                <input type="hidden" id="sand_brand_id" name="brand_id">
                <input type="hidden" id="sand_category_id" name="category_id">
                <div class="control-subtitle no-border">
                    Search
                </div>
                <div class="input-group control-search">
                    <input type="text" class="form-control" id="download_search" name="download_search"
                           placeholder="Keywords"
                           value="{{$download_search}}">
                    <button class="btn" type="button" onclick="search_submit()"><img width="24px"
                                                                                     src="{{asset('images/icon-search-c.svg')}}"
                                                                                     alt=""></button>
                </div>
                @if(count($data)>0)
                    <table class="table table-borderless control-table download-table" id="download_table">
                        <thead>
                        <tr class="control-thead">
                            <th class="table-check"><label class="checkbox">
                                    <input type="checkbox" id="btn_click" onclick="click_all()"><span></span>
                                </label></th>
                            <th class="text-nowrap">Brand <img class="table-sorting"
                                                               src="{{asset('images/icon-down.svg')}}" alt=""></th>
                            <th>Category</th>
                            <th>Asset</th>
                            <th>Date</th>
                            <th style="width:110px;"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $item)
                            <tr>
                                <td><label class="checkbox">
                                        <input type="checkbox" contentid="{{$item->id}}"><span></span>
                                    </label></td>
                                <td>{{$item->content_brand_name}}</td>
                                <td>{{$item->content_category_name}}</td>
                                <td>{{$item->content_title}}</td>
                                <td>
                                    @if( !is_null($item->updated_at) )
                                        {{date('Y-m-d H:i', strtotime($item->updated_at) )}}
                                    @else
                                        {{date('Y-m-d H:i', strtotime($item->created_at) )}}
                                    @endif
                                </td>
                                <td>
                                    <div class="download-icons">
                                        @if($item->content_sample_pic)
                                            <a data-fancybox="gallery" href="{{asset($item->content_sample_pic)}}"><svg class="list-icon mr-16"><use xlink:href="#view"></use></svg></a>
                                        @else
                                            {{--                                            <svg class="list-icon">--}}
                                            {{--                                                <use xlink:href="#view"></use>--}}
                                            {{--                                            </svg>--}}
                                        @endif
                                        <a href="/download/single/{{$item->id}}"><svg class="list-icon"><use xlink:href="#download"></use></svg></a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{$data->links()}}
                @else
                    <div class="div_empty">
                        The Folder Is Empty!
                    </div>
                @endif
            </form>
            <div style="height: 100px"></div>
        </div>
    </div>
</div>
<form action="/download/files" method="post" id="download_file">
    @csrf
    <input type="hidden" name="download_id" id="download_id">
    <div class="download-action position-fixed">
        <div class="d-flex justify-content-center align-items-center">
            <div style="margin-right: 40px"><span id="checkedCount"></span> items selected</div>
            <div><a onclick="file_submit()"><b class="position-relative" style="margin-right: 20px; top: 2px">DOWNLOAD</b><img src="{{asset('images/icon-download.svg')}}" alt=""></a>
            </div>
        </div>
    </div>
</form>
<script>
    let checked = 0;
    let category = [];
    let brand = [];

    $(document).ready(function () {
        $("tbody tr :checked").closest("tr").addClass("checked")
        $("#brand_id li").each(function () {
            if ($(this).hasClass("active")) {
                tmp_arr = $(this).attr('brandid')
                brand.push(tmp_arr)
            }
        })
        $("#sand_brand_id").val(JSON.stringify(brand))

        $("#category_id li").each(function () {
            if ($(this).hasClass("active")) {
                tmp_arr = $(this).attr('categoryid')
                category.push(tmp_arr)
            }
        })
        $("#sand_category_id").val(JSON.stringify(category))


        $("#download_table").tablesorter({
            theme: "bootstrap",
            headers: {
                0: {
                    sorter: false
                },
                2: {
                    sorter: false
                },
                3: {
                    sorter: false
                },
                4: {
                    sorter: false
                },
                5: {
                    sorter: false
                },
                6: {
                    sorter: false
                }
            }
        });
    })

    $("#brand_id li").click(function () {
        if ($(this).hasClass("active")) {
            //取消勾選
            tmp_arr = $(this).attr('brandid')
            brand = $.grep(brand, function (value) {
                return value != tmp_arr
            })
            $(this).toggleClass("active")
        } else {
            //勾選該項
            tmp_arr = $(this).attr('brandid')
            brand.push(tmp_arr)
            $(this).toggleClass("active")
        }
        $("#sand_brand_id").val(JSON.stringify(brand))
        search_submit()
    })

    $("#category_id li").click(function () {
        if ($(this).hasClass("active")) {
            //取消勾選
            tmp_arr = $(this).attr('categoryid')
            category = $.grep(category, function (value) {
                return value != tmp_arr
            })
            $(this).toggleClass("active")
        } else {
            //勾選該項
            tmp_arr = $(this).attr('categoryid')
            category.push(tmp_arr)
            $(this).toggleClass("active")
        }
        // console.log(category)
        $("#sand_category_id").val(JSON.stringify(category))
        search_submit()
    })

    $("tbody :checkbox").click(function () {
        checked = $("tbody :checked").length;
        if (checked > 0) {
            $(".download-action").show()
        } else {
            $(".download-action").hide()
        }
        $("tbody tr").removeClass("checked")
        $("tbody tr :checked").closest("tr").addClass("checked")

        $("#checkedCount").text(checked)
    })

    function click_all() {
        if ($("#btn_click").prop("checked")) {
            console.log('on')
            $("tbody :checkbox").prop("checked", true)
            checked = $("tbody :checked").length;
            $("#checkedCount").text(checked)
            $(".download-action").show()
        } else {
            console.log('off')
            $("tbody :checked").prop("checked", false)
            checked = $("tbody :checked").length;
            console.log(checked)
            $("#checkedCount").text(checked)
            $(".download-action").hide()
        }
    }

    function search_submit() {
        $("#download_search").submit();
    }

    function file_submit() {
        let download = [];

        $("tbody :checked").each(function () {
            download.push($(this).attr('contentid'))
        })
        datas = JSON.stringify(download)
        $("#download_id").val(datas)

        $("#download_file").submit()
    }
</script>
<style tpye="text/css">
.cpointer {
   cursor: pointer !important;
}
</style>

</body>
</html>
