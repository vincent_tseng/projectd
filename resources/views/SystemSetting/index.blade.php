@extends('layouts.content')

@section('sidebar')
    @include('layouts.sidebar')
@endsection

@section('main-content')
    <form action="/systemsetting/index" method="post" id="setingtable">
        @csrf
        <table class="table table-borderless table-striped">
            <tbody>
            <tr>
                <td>Maintenance？</td>
                <td>
                   <label class="switch">
                      <input type="checkbox" name="maintain" id="maintain">
                      <span class="slider round"></span>
                   </label>
                </td>
            </tr>
            <tr>
                <td>Privacy Policy URL:</td>
                <td><input type="text" id="policy_url" name="policy_url" value="{{$value['policy_url']}}" placeholder="Privacy Policy URL is?" class="form-control"></td>
            </tr>
            <tr>
                <td>Terms of service URL:</td>
                <td><input type="text" id="service_url" name="service_url" value="{{$value['service_url']}}" placeholder="Terms of service URL is?" class="form-control"></td>
            </tr>
            <tr>
                <td>Service Email:</td>
                <td><input type="text" id="service_email" name="service_email" value="{{$value['service_email']}}" placeholder="Service Email is?" class="form-control"></td>
            </tr>
            <tr>
                <td>Personal information:</td>
                <td><input type="text" id="url_pi" name="url_pi" value="{{$value['url_pi'] }}" placeholder="Personal information is?" class="form-control"></td>
            </tr>
            </tbody>
        </table>
        <div class="control-btns d-flex justify-content-center">
            <button class="btn btn-outline-primary" type="button" onclick="submit()">APPLY</button>
        </div>
    </form>
@endsection

@section('page-javascripts')
    <script>
        $(document).ready(function(){
            var is_public = '{{$value['maintain']}}'
            if( is_public == 'on' )
            {
                $("#maintain").attr('checked','checked')
            }
        })

        function submit()
        {
            $("#setingtable").submit()
        }
    </script>
@endsection
