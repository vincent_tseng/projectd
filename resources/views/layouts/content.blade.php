@include('layouts.header')
    <body>
    @if(session('message'))
    <div class="alert-msg position-fixed" id="alertmsg">
        <div class="position-relative">
            @if(session('message')['type'] == 'error')
                <div class="alert-content alert-con-danger" id="alertmessage">{{session('message')['msg']}}</div>
            @else
                <div class="alert-content alert-con-info" id="alertmessage">{{session('message')['msg']}}</div>
            @endif
            <img class="position-absolute alert-close" src="{{asset('images/icon-close.svg')}}" alt="" onclick="btn_close()">
        </div>
    </div>
    @endif
    <svg class="d-none">
        <symbol id="edit">
            <g>
                <path class="" d="M20.2,24.9H6.9c-1.5,0-2.7-1.2-2.7-2.7V8.9c0-1.5,1.2-2.7,2.7-2.7h6.7c0.4,0,0.8,0.3,0.8,0.8S14,7.8,13.6,7.8
            H6.9c-0.6,0-1.2,0.5-1.2,1.2v13.3c0,0.6,0.5,1.2,1.2,1.2h13.3c0.6,0,1.2-0.5,1.2-1.2v-6.7c0-0.4,0.3-0.8,0.8-0.8s0.8,0.3,0.8,0.8
            v6.7C22.9,23.7,21.7,24.9,20.2,24.9z"/>
            </g>
            <g>
                <path class="" d="M10.7,19.2c-0.2,0-0.4-0.1-0.5-0.2c-0.2-0.2-0.3-0.5-0.2-0.7l1-3.8c0-0.1,0.1-0.3,0.2-0.3l9-9l0,0
            c1-1,2.9-1,3.9,0c0.5,0.5,0.8,1.2,0.8,2s-0.3,1.4-0.8,2l-9,9c-0.1,0.1-0.2,0.2-0.3,0.2l-3.8,1C10.8,19.2,10.8,19.2,10.7,19.2z
             M12.3,15l-0.6,2.4l2.4-0.6L23,7.9c0.2-0.2,0.4-0.6,0.4-0.9S23.3,6.4,23,6.1c-0.5-0.5-1.3-0.5-1.8,0L12.3,15z"/>
            </g>
        </symbol>
    </svg>
    @yield('sidebar')
    <main class="control-panel">
        <div class="page-top position-relative d-flex justify-content-end">
            <div class="user-account d-flex align-items-center">
                <a href="/download">Go To Download Page</a>
                <a href="/logout">Sign Out</a>
                <a class="user-content">
                    <div class="user-name">{{Session::get('user')['name']}}</div>
                    <div class="user-type">{{Session::get('user')['member_group_name']}}</div>
                </a>
            </div>
        </div>
        @yield('main-content')
    </main>
    <script>
        function btn_close()
        {
            $("#alertmsg").hide();
        }
    </script>

@include('layouts.footer')
