<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{$title}}</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <!-- Jquery include -->
    <script
        src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
        crossorigin="anonymous"></script>
{{--    <script src="{{asset('js/general.js')}}"></script>--}}
    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!-- Table Drag -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/TableDnD/0.9.1/jquery.tablednd.js" integrity="sha256-d3rtug+Hg1GZPB7Y/yTcRixO/wlI78+2m08tosoRn7A=" crossorigin="anonymous"></script>
    <!-- Table Sort -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.31.3/js/jquery.tablesorter.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.31.3/css/theme.bootstrap_4.min.css"></link>
    <!-- FancyBox -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>

    <link rel="stylesheet" href="{{asset('css/style.css')}}">

    <!-- 跳頁處理 -->
{{--    <script>--}}
{{--        function jumpto(pageid)--}}
{{--        {--}}
{{--            let path = '/{{Request::path()}}'--}}
{{--            --}}{{--let params = $.parseJSON('{!! $page_data !!}')--}}
{{--            let urls = ''--}}
{{--            $.each(params,function(key, value){--}}
{{--                urls += key+'='+value+'&'--}}
{{--            })--}}

{{--            // urls = urls.substr(0,(urls.length - 1));--}}
{{--            // console.log(urls)--}}
{{--            // window.location = path + '?page=' + pageid + '&' + urls--}}
{{--            window.location.href = path + '?' + urls + 'page=' + pageid--}}
{{--        }--}}
{{--    </script>--}}
</head>
