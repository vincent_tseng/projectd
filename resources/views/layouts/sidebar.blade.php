<aside class="main-aside d-flex">
    @if(in_array('ALL',Session::get('user')['member_group']['allow_manager']))
        <div>
            <div class="dashboard-logo">
                <img class="dashboard-title-top" src="{{asset('images/Sidebar_Head.svg')}}" alt="">
            </div>
            <nav>
                <div>
                    <div class="side-menu-title">Content</div>
                    <ul class="list-unstyled side-menu">
                        <li {!! Request::is('content/*') ? "class='active'" : ''  !!}><a href="/content/index">File Manager</a></li>
                        <li {!! Request::is('brand/*') ? "class='active'" : ''  !!}><a href="/brand/index">Brand Manager</a></li>
                        <li {!! Request::is('category/*') ? "class='active'" : ''  !!}><a href="/category/index">Category Manager</a></li>
{{--                        <li {!! Request::is('field/*') ? "class='active'" : ''  !!}><a href="/field/index">Field Manager</a></li>--}}
                    </ul>
                </div>
                <div>
                    <div class="side-menu-title">Member</div>
                    <ul class="list-unstyled side-menu">
                        <li {!! Request::is('member/*') ? "class='active'" : ''  !!}><a href="/member/index">Member Manager</a></li>
                        <li {!! Request::is('member_group/*') ? "class='active'" : ''  !!}><a href="/member_group/index">Members Rules</a></li>
                    </ul>
                </div>
                <div>
                    <div class="side-menu-title">System</div>
                    <ul class="list-unstyled side-menu">
                        <li {!! Request::is('systemsetting/*') ? "class='active'" : ''  !!}><a href="/systemsetting/index">System Setting</a></li>
                        <li {!! Request::is('systemlog/*') ? "class='active'" : ''  !!}><a href="/systemlog/index">System Log</a></li>
                    </ul>
                </div>
            </nav>
        </div>
        <div class="dashboard-logo text-center">
            <img src="{{asset('images/Logo.svg')}}" alt="">
        </div>
    @else
        <div>
            <div class="dashboard-logo text-center">
                <img src="{{asset('images/Sidebar_Head.svg')}}" alt="">
            </div>
            <div>
                <div class="side-menu-title">Content</div>
                <ul class="list-unstyled side-menu">
                    @if(in_array('CTM',Session::get('user')['member_group']['allow_manager']))
                        <li {!! Request::is('content/*') ? "class='active'" : ''  !!}><a href="/content/index">File Manager</a></li>
                    @endif
                    @if(in_array('BM',Session::get('user')['member_group']['allow_manager']))
                        <li {!! Request::is('brand/*') ? "class='active'" : ''  !!}><a href="/brand/index">Brand Manager</a></li>
                    @endif
                    @if(in_array('CAM',Session::get('user')['member_group']['allow_manager']))
                        <li {!! Request::is('category/*') ? "class='active'" : ''  !!}><a href="/category/index">Category Manager</a></li>
                    @endif
                    @if(in_array('FM',Session::get('user')['member_group']['allow_manager']))
{{--                        <li {!! Request::is('field/*') ? "class='active'" : ''  !!}><a href="/field/index">Field Manager</a></li>--}}
                    @endif
                </ul>
            </div>
            @if(in_array('MM',Session::get('user')['member_group']['allow_manager']))
                <div>
                    <div class="side-menu-title">Member</div>
                    <ul class="list-unstyled side-menu">
                        <li {!! Request::is('member/*') ? "class='active'" : ''  !!}><a href="/member/index">Member Manager</a></li>
                        <li {!! Request::is('member_group/*') ? "class='active'" : ''  !!}><a href="/member_group/index">Members Rules</a></li>
                    </ul>
                </div>
            @endif
            @if(in_array('SS',Session::get('user')['member_group']['allow_manager']) || in_array('SL',Session::get('user')['member_group']['allow_manager']))
            <div>
                <div class="side-menu-title">System</div>
                <ul class="list-unstyled side-menu">
                    @if(in_array('SS',Session::get('user')['member_group']['allow_manager']))
                        <li {!! Request::is('systemsetting/*') ? "class='active'" : ''  !!}><a href="/systemsetting/index">System Setting</a></li>
                    @endif
                    @if(in_array('SL',Session::get('user')['member_group']['allow_manager']))
                        <li {!! Request::is('systemlog/*') ? "class='active'" : ''  !!}><a href="/systemlog/index">System Log</a></li>
                    @endif
                </ul>
            </div>
            @endif
            </nav>
        </div>
        <div class="dashboard-logo text-center">
            <img src="{{asset('images/Logo.svg')}}" alt="">
        </div>
    @endif
</aside>
<script>
    $(".submenu-btn").click(function(){
        $(this).next(".submenu").toggleClass("active")
    })

    $(".submenu-btn-admin").click(function(){
        $(this).next(".submenu-admin").toggleClass("active")
    })

</script>
