@extends('layouts.content')

@section('sidebar')
    @include('layouts.sidebar')
@endsection

@section('main-content')

    <div class="mask" id="mask">
        <div class="d-flex justify-content-center align-items-center">
            <div class="delete-confirm-modal text-center">
                <div>
                    <img style="width: 100px" src="{{asset('images/icon-close-danger.svg')}}" alt="">
                </div>
                <div class="confirm-words">Are you sure ?</div>
                <div style="margin-bottom: 60px;">Do you really want to delete this item?<br>This process cannot be undone.</div>
                <div>
                    <button class="btn btn-red" type="button" onclick="btn_delete()">DELETE</button>
                    <button class="btn btn-gray" type="button" onclick="btn_delete_cancel()">CANCEL</button>
                </div>
            </div>
        </div>
    </div>

    <div class="dashboard-title position-relative">
        <h1>Member Rules <span>/ Edit Member Rules</span></h1>
    </div>
    <div class="main-content" id="app">
        <div class="warn-area">
            Warning: Please be very careful with the access privileges you grant.<br>
            Any setting marked with <img src="{{asset('images/icon-lock.svg')}}" alt=""> should only be granted to
            people you trust implicitly.
        </div>
        <form action="/member_group/modify" id="member_group_form" method="post" enctype="multipart/form-data"
              class="form-horizontal">
            @csrf
            <input type="hidden" name="renew" value="0">
            <input type="hidden" name="membergroup_id" value="{{$memberGroup['id']}}">
            <div class="container-fluid p-0">
                <div class="row">
                    <div class="input-panel">
                        <div class="mb-3">
                            <label for="" class="form-label">Group Name</label>
                            <input type="text" id="member_group_name" name="member_group_name" placeholder="Name?"
                                   value="{{$memberGroup['member_group_name']}}" class="form-control" maxlength="20">
                            <span id="msg_member_group_name"></span>
                        </div>
                        <div class="mb-3">
                            <label for="" class="form-label">Note</label>
                            <textarea name="member_group_note" id="member_group_note" rows="9" placeholder="Content..."
                                      class="form-control">{{$memberGroup['member_group_note']}}</textarea>
                            <span id="msg_member_group_note"></span>
                        </div>
                        <div class="mb-3">
                            <label for="" class="form-label">Allowed access brand <img
                                    src="{{asset('images/icon-lock.svg')}}" alt=""></label>
                            @foreach($brands as $item)
                                <div class="mb-2">
                                    <label class="checkbox">
                                        @if(in_array($item['id'], $memberGroup['brands']))
                                            <input type="checkbox" id="brands{{$item['id']}}" name="brands[]"
                                                   value="{{$item['id']}}" class="form-check-input" checked>
                                            <span>{{$item['brand_name']}}</span>
                                        @else
                                            <input type="checkbox" id="brands{{$item['id']}}" name="brands[]"
                                                   value="{{$item['id']}}" class="form-check-input">
                                            <span>{{$item['brand_name']}}</span>
                                        @endif
                                    </label>
                                </div>
                            @endforeach
                            <span id="msg_brands"></span>
                        </div>
                    </div>
                </div>
                <div class="mb-3">
                    <div class="light-area d-flex justify-content-between align-items-center">
                        <label for="" class="form-label m-0">Login Control Status </label>
                        <label class="switch">
                            <input type="checkbox" name="is_login" id="is_login">
                            <span class="slider round"></span>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="">
                            <div class="light-area d-flex justify-content-between align-items-center">
                                <label for="" class="form-label m-0">Access control panel <img
                                        src="{{asset('images/icon-lock.svg')}}" alt=""></label>
                                <label class="switch">
                                    <input type="checkbox" id="is_public" name="is_public">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>
                        <div class="dashboard-panel">
                            <div class="row">
                                <div class="col-6">
                                    <label for="" class="form-label m-0">Allowed access managers <img
                                            src="{{asset('images/icon-lock.svg')}}" alt=""></label>
                                    @foreach($allow_managers as $keys => $rows)
                                        <div class="mb-2">
                                            <label class="checkbox">
                                                @if(in_array($rows['values'],$memberGroup['allow_manager']))
                                                    <input type="checkbox" id="allow_manager{{$keys}}"
                                                           name="allow_manager[]" value="{{$rows['values']}}"
                                                           class="form-check-input" checked>
                                                    <span>{{$rows['name']}}</span>
                                                @else
                                                    <input type="checkbox" id="allow_manager{{$keys}}"
                                                           name="allow_manager[]" value="{{$rows['values']}}"
                                                           class="form-check-input">
                                                    <span>{{$rows['name']}}</span>
                                                @endif
                                            </label>
                                        </div>
                                    @endforeach
                                    <span id="msg_allow_manager"></span>
                                </div>
                                <div class="col-6">
                                    <label for="" class="form-label m-0">Allow actions <img
                                            src="{{asset('images/icon-lock.svg')}}" alt=""></label>
                                    @foreach($allow_actions as $key2 => $rows2)
                                        <div class="mb-2">
                                            <label class="checkbox">
                                                @if(in_array($rows2['values'],$memberGroup['allow_actions']))
                                                    <input type="checkbox" id="allow_actions{{$key2}}"
                                                           name="allow_actions[]" value="{{$rows2['values']}}"
                                                           class="form-check-input" checked>
                                                    <span>{{$rows2['name']}}</span>
                                                @else
                                                    <input type="checkbox" id="allow_actions{{$key2}}"
                                                           name="allow_actions[]" value="{{$rows2['values']}}"
                                                           class="form-check-input">
                                                    <span>{{$rows2['name']}}</span>
                                                @endif
                                            </label>
                                        </div>
                                    @endforeach
                                    <span id="msg_allow_actions"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="control-btns d-flex justify-content-between">
                <div></div>
                <div>
                    <button class="btn btn-primary btn-small" type="button" onclick="btn_submit()">SAVE MEMBER GROUP
                    </button>
                </div>
                <button class="btn-red btn btn-small" type="button" onclick="btn_delete_show()">DELETE</button>
            </div>
        </form>
    </div>
@endsection

@section('page-javascripts')
    <script language="javascript">
        $(document).ready(function () {
            var is_public = '{{$memberGroup['is_public']}}'
            var is_login = '{{$memberGroup['is_login']}}'
            if (is_public == 'on') {
                $("#is_public").attr('checked', 'checked')
            }
            if (is_login == 'on') {
                $("#is_login").attr('checked', 'checked')
            }})

        function btn_submit() {
            let flag = ''
            flag = submit_check()
            if (flag) {
                $("#member_group_form").submit()
            }
        }

        function save_new() {
            let flag = ''
            flag = submit_check()
            if (flag) {
                $("#renew").val(1)
                $("#member_group_form").submit()
            }
        }

        function submit_check() {

            if($("#member_group_name").val() == '')
            {
                $('#member_group_name').focus()
                $("#msg_member_group_name").html('<small class="form-text text-muted"><font color="red"> Do not empty.</font></small>')
                return false
            }
            $("#msg_field_name").html('')

            if($("#member_group_note").val() == '')
            {
                $('#member_group_note').focus()
                $("#msg_member_group_note").html('<small class="form-text text-muted"><font color="red">Do not empty.</font></small>')
                return false
            }
            $("#msg_member_group_note").html('')

            if($("input[name='brands[]']:checked").length <= 0)
            {
                $("#msg_brands").html('<small class="form-text text-muted"><font color="red">Please Choose One.</font></small>')
                return false
            }
            $("#msg_brands").html('')

            //如果Access Control panel有開啟才檢查下列項目：
            if($("#is_public").prop('checked'))
            {
                if($("input[name='allow_manager[]']:checked").length <= 0)
                {
                    $("#msg_allow_manager").html('<small class="form-text text-muted"><font color="red">Please Choose One.</font></small>')
                    return false
                }
                $("#msg_allow_manager").html('')

                if($("input[name='allow_actions[]']:checked").length <= 0)
                {
                    $("#msg_allow_actions").html('<small class="form-text text-muted"><font color="red">Please Choose One.</font></small>')
                    return false
                }
                $("#msg_allow_actions").html('')
            }

            if($("#check_group_name").val() == 0 )
            {
                return false
            }

            return true
        }



        function btn_delete()
        {
            let check_flag = confirm('Are You Sure To Delete This?')
            if(check_flag)
            {
                $("#member_group_form").attr('action','/member_group/delete')
                $("#member_group_form").submit()
            }
        }

        function btn_delete_show()
        {
            $("#mask").fadeIn(300)
        }

        function btn_delete_cancel()
        {
            $("#mask").fadeOut(300)
        }
    </script>
@endsection
