@extends('layouts.content')

@section('sidebar')
    @include('layouts.sidebar')
@endsection

@section('main-content')
    <div class="dashboard-title position-relative">
        <h1>Member Rules <span>/ New Member Rules</span></h1>
    </div>
    <div class="main-content" id="app">
        <form action="/member_group/new" id="member_group_form" method="post" enctype="multipart/form-data" class="form-horizontal">
            @csrf
            <input type="hidden" id="renew" name="renew" value="0">
            <input type="hidden" id="check_group_name" name="check_group_name" value="0">
        <div class="warn-area">
            Warning: Please be very careful with the access privileges you grant.<br>
            Any setting marked with <img src="{{asset('images/icon-lock.svg')}}" alt=""> should only be granted to people you trust implicitly.
        </div>
        <div class="container-fluid p-0">
            <div class="row">
                <div class="input-panel">
                    <div class="mb-3">
                        <label for="" class="form-label">Group Name</label>
                        <input type="text" id="member_group_name" name="member_group_name" placeholder="Name?" class="form-control" onblur="check_name_group()" maxlength="20">
                        <span id="msg_member_group_name"></span>
                    </div>
                    <div class="mb-3">
                        <label for="" class="form-label">Note</label>
                        <textarea name="member_group_note" id="member_group_note" rows="9" placeholder="Content..." class="form-control"></textarea>
                        <span id="msg_member_group_note"></span>
                    </div>
                    <div class="mb-3">
                        <label for="" class="form-label">Allowed access brand <img src="{{asset('images/icon-lock.sv')}}g" alt=""></label>
                        @foreach($brands as $item)
                            <div class="mb-2">
                                <label class="checkbox">
                                    <input type="checkbox" id="brands{{$item['id']}}" name="brands[]" value="{{$item['id']}}" class="form-check-input">
                                    <span>{{$item['brand_name']}}</span>
                                </label>
                            </div>
                        @endforeach
                    </div>
                    <span id="msg_brands"></span>
                    </div>
                </div>
            </div>
            <div class="mb-3">
                <div class="light-area d-flex justify-content-between align-items-center">
                    <label for="" class="form-label m-0">Login Control Status </label>
                    <label class="switch">
                        <input type="checkbox" name="is_login" id="is_login">
                        <span class="slider round"></span>
                    </label>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="">
                        <div class="light-area d-flex justify-content-between align-items-center">
                            <label for="" class="form-label m-0">Access control panel <img src="{{asset('images/icon-lock.svg')}}" alt=""></label>
                            <label class="switch">
                                <input type="checkbox" id="is_public" name="is_public">
                                <span class="slider round"></span>
                            </label>
                        </div>
                    </div>
                    <div class="dashboard-panel">
                        <div class="row">
                            <div class="col-6">
                                <label for="" class="form-label m-0">Allowed access managers <img src="{{asset('images/icon-lock.svg')}}" alt=""></label>
                                @foreach($allow_managers as $keys => $rows)
                                    <div class="mb-2">
                                        <label class="checkbox">
                                            <input type="checkbox" id="allow_manager{{$keys}}" name="allow_manager[]" value="{{$rows['values']}}" class="form-check-input">
                                            <span>{{$rows['name']}}</span>
                                        </label>
                                    </div>
                                @endforeach
                                <span id="msg_allow_manager"></span>
                            </div>
                            <div class="col-6">
                                <label for="" class="form-label m-0">Allow actions <img src="{{asset('images/icon-lock.svg')}}" alt=""></label>
                                @foreach($allow_actions as $key2 => $rows2)
                                    <div class="mb-2">
                                        <label class="checkbox">
                                            <input type="checkbox" id="allow_actions{{$key2}}" name="allow_actions[]" value="{{$rows2['values']}}" class="form-check-input">
                                            <span>{{$rows2['name']}}</span>
                                        </label>
                                    </div>
                                @endforeach
                                <span id="msg_allow_actions"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <div class="control-btns d-flex justify-content-center">
            <button class="btn btn-primary btn-small" type="button" onclick="btn_submit()">SAVE MEMBER GROUP</button>
        </div>
    </form>
    </div>
@endsection

@section('page-javascripts')
    <script language="javascript">
        function btn_submit()
        {
            let flag=''
            flag = submit_check()
            if(flag)
            {
                $("#member_group_form").submit()
            }
        }

        function save_new()
        {
            let flag = ''
            flag = submit_check()
            if(flag)
            {
                $("#renew").val(1)
                $("#member_group_form").submit()
            }
        }

        function submit_check()
        {
            check_name_group();

            if($("#member_group_name").val() == '')
            {
                $('#member_group_name').focus()
                $("#msg_member_group_name").html('<small class="form-text text-muted"><font color="red"> Do not empty.</font></small>')
                return false
            }
            $("#msg_field_name").html('')

            if($("#member_group_note").val() == '')
            {
                $('#member_group_note').focus()
                $("#msg_member_group_note").html('<small class="form-text text-muted"><font color="red">Do not empty.</font></small>')
                return false
            }
            $("#msg_member_group_note").html('')

            if($("input[name='brands[]']:checked").length <= 0)
            {
                $("#msg_brands").html('<small class="form-text text-muted"><font color="red">Please Choose One.</font></small>')
                return false
            }
            $("#msg_brands").html('')

            //如果Access Control panel有開啟才檢查下列項目：
            if($("#is_public").prop('checked'))
            {
                if ($("input[name='allow_manager[]']:checked").length <= 0) {
                    $("#msg_allow_manager").html('<small class="form-text text-muted"><font color="red">Please Choose One.</font></small>')
                    return false
                }
                $("#msg_allow_manager").html('')

                if ($("input[name='allow_actions[]']:checked").length <= 0) {
                    $("#msg_allow_actions").html('<small class="form-text text-muted"><font color="red">Please Choose One.</font></small>')
                    return false
                }
                $("#msg_allow_actions").html('')
            }

            if($("#check_group_name").val() == 0 )
            {
                return false
            }

            return true
        }

        function check_name_group()
        {
            let member_group_name = $("#member_group_name").val();

            //後續等後端加上資料庫，並使用AJAX更新
            $.ajax({
                type: 'POST',
                url: '/member_group/check',
                data: {"_token": "{{ csrf_token() }}", "member_group_name": member_group_name},
                success: function(msg)
                {
                    console.log(msg)
                    if(msg == 'Group Name Already Exists.')
                    {
                        $("#msg_member_group_name").html('<small class="form-text text-muted"><font color="red">'+msg+'</font></small>')
                        $("#check_group_name").val(0)
                    }
                    else
                    {
                        $("#msg_member_group_name").html('<small class="form-text text-muted"><font color="green">'+msg+'</font></small>')
                        $("#check_group_name").val(1)
                    }
                }
            })
        }
    </script>
@endsection
