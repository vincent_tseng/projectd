@extends('layouts.content')

@section('sidebar')
    @include('layouts.sidebar')
@endsection

@section('main-content')
    <div class="dashboard-title position-relative">
        <h1>Member Rules</h1>
        <a class="btn btn-primary position-absolute text-white" onclick="add_new()">ADD NEW</a>
    </div>
    <div class="main-content list-content" id="app">
        <div class="d-flex justify-content-between mb-3">
            <h2 class="mb-0">Member Rules</h2>
        </div>
        @if(count($memberGroups) > 0)
            <table class="table table-borderless table-striped">
                <thead>
                <tr>
                    <th style="width: 80px">ID</th>
                    <th>Group Title</th>
                    <th style="width: 100px">Login Control Status</th>
                    <th style="width: 100px">Access Control panel Status</th>
                    <th class="text-center" style="width: 90px">Manage</th>
                </tr>
                </thead>
                <tbody>
                @foreach($memberGroups as $item)
                    <tr class="tr-shadow">
                        <td>{{$item['id']}}</td>
                        <td class="desc">{{$item['member_group_name']}}</td>
                        <td>
                            @if($item['is_login']=='on')
                                <div class="status-circle status-public"></div>
                                <b class="status status-text-public member-status">ACTIVE</b>
                            @else
                                <div class="status-circle status-banned"></div>
                                <b class="status status-text-draft member-status">INACTIVE</b>
                            @endif
                        </td>
                        <td>
                            @if($item['is_public']=='on')
                                <div class="status-circle status-public"></div>
                                <b class="status status-text-public member-status">ACTIVE</b>
                            @else
                                <div class="status-circle status-banned"></div>
                                <b class="status status-text-draft member-status">INACTIVE</b>
                            @endif
                        </td>
                        <td class="text-center">
                            @if(($item['id'] != 1) && ($item['member_group_name'] != 'New Account'))
                                <a class="cursor-pointer" onclick="modify({{$item['id']}})">
                                    <svg class="svg-icon">
                                        <use xlink:href="#edit"></use>
                                    </svg>
                                </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <div class="div_empty">
                The Folder Is Empty!
            </div>
        @endif
    </div>
@endsection

@section('page-javascripts')
    <script language="javascript">
        function add_new()
        {
            window.location = '/member_group/new'
        }

        function modify(idx)
        {
            window.location = '/member_group/modify/'+idx
        }

    </script>
@endsection
