@include('layouts.header')
<script src="https://www.google.com/recaptcha/api.js"></script>
<body>
<div class="container">
    <header class="control-header d-flex justify-content-between">
        <div class="d-flex align-items-center position-relative download-page-logo" style="height: 44px">
            <img class="control-logo" src="{{asset('images/Logo_b.svg')}}" alt="">
            <span class="control-title"><img src="{{asset('images/control-title.svg')}}" alt=""></span>
        </div>
        <div class="d-flex control-right align-items-center">
            <a href="/">Login</a>
        </div>
    </header>
    <div class="partner-form">
        <form action="/partner" id="partner" method="post">
            @csrf
            <input type="hidden" name="member_check" id="member_check">
            <div class="row justify-content-center">
                <div class="col-md-4 account-request">
                    <h1 class="text-center">Account Request</h1>
                    <div class="mb-2">
                        <label for="" class="form-label">What is your name? *</label>
                        <input type="text" class="form-control" name="member_name" id="member_name">
                        <span id="msg_member_name"></span>
                    </div>
                    <div class="mb-2">
                        <label for="" class="form-label">What is your email address? <font color="red">MUST BE A Using Email.</font>*</label>
                        <input type="text" class="form-control" name="member_email" id="member_email" onblur="check_member()">
                        <span id="msg_member_email"></span>
                    </div>
                    <div class="mb-2">
                        <label for="" class="form-label">What is the name of you company? *</label>
                        <input type="text" class="form-control" name="company_name" id="company_name">
                        <span id="msg_company_name"></span>
                    </div>
                    <div class="mb-2">
                        <label for="" class="form-label">What is your contact at Gogoro? *</label>
                        <input type="text" class="form-control" name="contact_name" id="contact_name">
                        <span id="msg_contact_name"></span>
                    </div>
                    <div class="mb-2">
                        <label for="" class="form-label">And their email address? *</label>
                        <input type="text" class="form-control" name="contact_email" id="contact_email">
                        <span id="msg_contact_email"></span>
                    </div>
                    <div class="mb-2">
                        <label for="" class="form-label">Anything else we should know? (Optional)</label>
                        <textarea class="form-control" rows="6" name="note"></textarea>
                    </div>
                    <div class="terms text-center">
                        This site is protected by reCAPTCHA and the Google <a class="link" href="{{$value['policy_url']}}">Privacy Policy</a> and <a class="link" href="{{$value['service_url']}}">Terms of Service</a> apply.
                        <a class="link" href="{{$value['policy_url']}}">Personal information</a>
                    </div>
                    <div class="mb-2 text-center">
                        <input type="checkbox" id="pi_agree" onclick="check_pi()"> I Agree Personal information
                        <br>
                        <br>
                    </div>
                    <div class="mb-5 text-center">
                        <button class="btn btn-primary" id="btn_sub" type="submit">SUBMIT</button>
    {{--                    <button class="btn btn-primary"--}}
    {{--                            data-sitekey="reCAPTCHA_site_key"--}}
    {{--                            data-callback='onSubmit'--}}
    {{--                            data-action='btn_submit'>SUBMIT</button>--}}
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
<script language="javascript">

    $(document).ready(function(){
        $("#btn_sub").prop('disabled', true)
        // $("#btn_sub").disable()
    })

    function check_pi()
    {
        if($("#pi_agree").prop('checked'))
        {
            $("#btn_sub").prop('disabled', false)
        }
        else
        {
            $("#btn_sub").prop('disabled', true)
        }

    }

    function check_member()
    {
        let member_email = $("#member_email").val();

        if(member_email != '' )
        {
            emailRule = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]+$/
            if(emailRule.test(member_email))
            {
                //後續等後端加上資料庫，並使用AJAX更新
                $.ajax({
                    type: 'POST',
                    url: '/partner/check_member',
                    data: {"_token": "{{ csrf_token() }}", "member_email": member_email},
                    success: function(msg)
                    {
                        console.log(msg)
                        if(msg == 'Member Email Already Exists.')
                        {
                            $("#msg_member_email").html('<small class="form-text text-muted"><font color="red">'+msg+'</font></small>')
                            $("#member_email").attr('class','form-control error-input');
                            $("#member_check").val(0)
                        }
                        else
                        {
                            $("#msg_member_email").html('<small class="form-text text-muted"><font color="green">'+msg+'</font></small>')
                            $("#member_email").attr('class','form-control');
                            $("#member_check").val(1)
                        }
                    }
                })
            }
            else
            {
                $("#msg_member_email").html('<p class="form-text text-muted"><strong><font color="red"> WRONG EMAIL FORMAT. </font></p>')
                $("#member_email").attr('class','form-control error-input');
                return false;
            }
        }
        else
        {
            $("#msg_member_email").html('<p class="form-text text-muted"><strong><font color="red"> PLEASE DO NOT EMPTY. </font></p>')
            $("#member_email").attr('class','form-control error-input');
            return false;
        }
    }
    $(document).on('blur','.partner-form',function(event){
        const target_id = event.target.id;
        if( event.target.value == '')
        {
            if(target_id != 'btn_sub')
            {
                event.target.attributes.class.value = 'form-control error-input';
                $('#'+ 'msg_'+ target_id).html('<p class="form-text text-muted"><strong><font color="red">PLEASE Do not empty. </font></p>');
            }
        }
        else
        {
            event.target.attributes.class.value = 'form-control';
            $('#'+ 'msg_'+ target_id).html('');
        }

        if(target_id == 'member_email')
        {
            console.log(1234)
            check_member();
        }
    })

    $('#partner').submit(function(){
        check_member();

        if($("#member_name").val() == '')
        {
            $("#member_name").attr('class','form-control error-input');
            $("#msg_member_name").html('<p class="form-text text-muted"><strong><font color="red"> Name Do not empty. </font></p>')
            return false
        }
        $("#member_name").attr('class','form-control');
        $("#msg_member_name").html('')

        if($("#member_email").val() == '')
        {
            $("#member_email").attr('class','form-control error-input');
            $("#msg_member_email").html('<p class="form-text text-muted"><strong><font color="red"> Email Do not empty. </font></p>')
            return false
        }
        $("#member_email").attr('class','form-control');
        $("#msg_member_email").html('')

        if($("#company_name").val() == '')
        {
            $("#company_name").attr('class','form-control error-input');
            $("#msg_company_name").html('<p class="form-text text-muted"><strong><font color="red"> Company Name Do not empty. </font></p>')
            return false
        }
        $("#company_name").attr('class','form-control');
        $("#msg_company_name").html('')

        if($("#contact_name").val() == '')
        {
            $("#contact_name").attr('class','form-control error-input');
            $("#msg_contact_name").html('<p class="form-text text-muted"><strong><font color="red"> Contact Do not empty. </font></p>')
            return false
        }
        $("#contact_name").attr('class','form-control');
        $("#msg_contact_name").html('')

        if($("#contact_email").val() == '')
        {
            $("#contact_name").attr('class','form-control error-input');
            $("#msg_contact_email").html('<p class="form-text text-muted"><strong><font color="red"> Contact Name Do not empty. </font></p>')
            return false
        }
        $("#contact_email").attr('class','form-control');
        $("#msg_contact_email").html('')

        if($("#member_check").val() == 0 )
        {
            return false
        }
    })
</script>
</html>
