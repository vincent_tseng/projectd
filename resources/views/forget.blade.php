@include('layouts.header')
<body class="login-bg">
<div class="login-header text-center fixed-top">
    <img class="" src="{{asset('images/Logo_b.svg')}}" alt="">
</div>
<div>
    <div class="text-center">
        <div class="login-sub">
            <img src="{{asset('images/digital_w.svg')}}" alt="">
        </div>
    </div>

    <div class="login-panel">
        <h1 class="text-center">Reset password</h1>
        <form action="/forget_passwd" method="post" id="forget">
            @csrf
            <div class="login-input">
                <label for="email" class="form-label">Please enter your E-mail address</label>
                <input type="email" class="form-control" id="email" name="email">
            </div>
            <div class="d-grid">
                <button class="btn btn-primary btn-lg" type="button" onclick="btn_submit()">RESET PASSWORD</button>
            </div>
        </form>
    </div>
    <div class="text-center login-bottom">
        <a href="{{$value['policy_url']}}" target="_blank">Privacy Policy</a> and <a href="{{$value['service_url']}}"
                                                                                     target="_blank">Terms of
            service</a>
    </div>
</div>
</body>
<script>
    function btn_submit() {
        $("#forget").submit();
    }
</script>
