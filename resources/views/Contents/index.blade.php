@extends('layouts.content')

@section('sidebar')
    @include('layouts.sidebar')
@endsection

@section('main-content')
    <div class="dashboard-title position-relative">
        <h1>File Manager</h1>
        @if(in_array('1',Session::get('user')['member_group']['allow_actions']))
            <a class="btn btn-primary position-absolute text-white" onclick="add_new()">ADD NEW</a>
        @endif
    </div>
    <div class="main-content list-content" id="app">
        <form action="/content/index" method="POST" id="search_bar">
            @csrf
            <div class="d-flex justify-content-between mb-3">
                <div class="d-flex dashboard-select">
                    <div>
                        <label class="form-label" for="">Brand</label>
                        <select class="form-select" name="s_brand" id="s_brand" onchange="search_bar()">
                            <option value="0">All Brand</option>
                            @foreach($brands as $item )
                                @if((isset($used_brand_id)) && ($used_brand_id == $item['id']))
                                    <option value="{{$item['id']}}" selected>{{$item['brand_name']}}</option>
                                @else
                                    <option value="{{$item['id']}}">{{$item['brand_name']}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div>
                        <label class="form-label" for="">Category</label>
                        <select class="form-select" name="s_category" id="s_category" onchange="search_bar()">
                            <option value="0">All Categories</option>
                            @foreach($categorys as $item )
                                @if((isset($used_category_id)) && ($used_category_id == $item['id']))
                                    <option value="{{$item['id']}}" selected>{{$item['category_name']}}</option>
                                @else
                                    <option value="{{$item['id']}}">{{$item['category_name']}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div>
                        <label class="form-label" for="">Status</label>
                        <select class="form-select" name="s_status" id="s_status" onchange="search_bar()">
                            <option value='ALL'>All Status</option>
                            @if((isset($used_status)) && ($used_status=='on'))
                                <option value="on" selected>Public</option>
                            @else
                                <option value="on">Public</option>
                            @endif

                            @if((isset($used_status)) && ($used_status=='0'))
                                <option value="0" selected>Draft</option>
                            @else
                                <option value="0">Draft</option>
                            @endif

                        </select>
                    </div>
                    {{--                <div>--}}
                    {{--                    <label class="form-label" for="">Date</label>--}}
                    {{--                    <select class="form-control" name="" id="">--}}
                    {{--                        <option value="">ALL</option>--}}
                    {{--                    </select>--}}
                    {{--                </div>--}}
                </div>
                <div class="d-flex align-items-end">
                    <div class="search-input position-relative">
                        <input type="search" class="form-control" placeholder="Search keywords" name="txt_search"
                               value="{{$used_txt_search}}" onblur="search_bar()">
                        <img class="position-absolute" src="{{asset('images/icon-search.svg')}}" alt="">
                    </div>
                </div>
            </div>
        </form>
        @if(count($contents) > 0 )
        <table class="table table-borderless table-striped table_content" id="table_content">
            <thead>
            <tr>
                <th id="clk_brand">Brand <img class="table-sorting"
                                              src="{{asset('images/icon-sort-arrow.svg')}}" alt=""></th>
                <th id="clk_categroy">Category <img class="table-sorting"
                                                    src="{{asset('images/icon-sort-arrow.svg')}}" alt=""></th>
                <th id="clk_title">Title <img class="table-sorting"
                                              src="{{asset('images/icon-sort-arrow.svg')}}" alt=""></th>
                <th id="clk_date">Upload date <img class="table-sorting"
                                                   src="{{asset('images/icon-sort-arrow.svg')}}" alt=""></th>
                <th>Status</th>
                <th class="text-center" style="width: 90px">Manage</th>
            </tr>
            </thead>
            <tbody>
                @foreach($contents as $item)
                    @if($user['member_group']['brands'][0] == 'ALL')
                        <tr>
                            <td class="blue-text">{{$item['content_brand_name']}}</td>
                            <td>{{$item['content_category_name']}}</td>
                            <td>{{$item['content_title']}}</td>
                            <td>
                                @if( !is_null($item['updated_at']) )
                                    {{date('Y-m-d H:i', strtotime($item['updated_at']) )}}
                                @else
                                    {{date('Y-m-d H:i', strtotime($item['created_at']) )}}
                                @endif
                            </td>
                            <td>
                                @if($item['is_public'] == 'on')
                                    <div class="status-circle status-public"></div>
                                    <b class="status status-text-public">PUBLIC</b>
                                @else
                                    <div class="status-circle status-draft"></div>
                                    <b class="status status-text-draft">DRAFT</b>
                                @endif
                            </td>
                            <td class="text-center">
                                @if((
                                    ($item['created_by'] == Session::get('user')['id']) && (in_array('2',Session::get('user')['member_group']['allow_actions']))
                                )
                                ||
                                (in_array('4',Session::get('user')['member_group']['allow_actions']) ))
                                    <a class="cursor-pointer" onclick="modify({{$item['id']}})">
                                        <svg class="svg-icon">
                                            <use xlink:href="#edit"></use>
                                        </svg>
                                    </a>
                                @endif
                            </td>
                        </tr>
                    @else
                        @if(in_array($item['content_brand_id'],$user['member_group']['brands']))
                            <tr>
                                <td><a href="">{{$item['content_brand_name']}}</a></td>
                                <td>{{$item['content_category_name']}}</td>
                                <td>{{$item['content_title']}}</td>
                                <td>
                                    @if( !is_null($item['updated_at']) )
                                        {{date('Y-m-d H:i', strtotime($item['updated_at']) )}}
                                    @else
                                        {{date('Y-m-d H:i', strtotime($item['created_at']) )}}
                                    @endif
                                </td>
                                <td>
                                    @if($item['is_public'] == 'on')
                                        <div class="status-circle status-public"></div>
                                        <b class="status status-text-public">PUBLIC</b>
                                    @else
                                        <div class="status-circle status-draft"></div>
                                        <b class="status status-text-draft">DRAFT</b>
                                    @endif
                                </td>
                                <td class="text-center">
                                    @if((
                                        ($item['created_by'] == Session::get('user')['id']) && (in_array('2',Session::get('user')['member_group']['allow_actions']))
                                    )
                                    ||
                                    (in_array('4',Session::get('user')['member_group']['allow_actions']) ))
                                        <a class="cursor-pointer" onclick="modify({{$item['id']}})">
                                            <svg class="svg-icon">
                                                <use xlink:href="#edit"></use>
                                            </svg>
                                        </a>
                                    @endif
                                </td>
                            </tr>
                        @endif
                    @endif
                @endforeach
            {{$contents->appends($link_search)->links()}}
            </tbody>
        </table>
        @else
            <div class="div_empty">
                The Folder Is Empty!
            </div>
        @endif
    </div>
@endsection
@section('page-javascripts')
    <script>
        let checked = 0;
        $("tbody :checkbox").click(function () {
            checked = $("tbody :checked").length;
            $("#checkedCount").text(checked)
            if (checked > 0) {
                $("#checkedAction").show();
            } else {
                $("#checkedAction").hide();
            }
        })

        function search_bar() {
            $("#search_bar").submit();
        }

        function add_new() {
            window.location = '/content/new'
        }

        function modify(idx) {
            window.location = '/content/modify/' + idx
        }

        $(function () {
            // widgets: ['zebra'] 這個參數，能對表格的奇偶數列作分色處理
            $("#table_content").tablesorter({
                theme : "bootstrap",
                headers: {
                    4:{
                        sorter: false
                    },
                    5:{
                        sorter: false
                    }
                }
            });
        });



    </script>
@endsection
