@extends('layouts.content')

@section('sidebar')
    @include('layouts.sidebar')
@endsection

@section('main-content')
    <div class="dashboard-title position-relative">
        <h1>File Manager <span>/ New File</span></h1>
    </div>
    <div class="main-content" id="app">
        <form action="/content/new" id="content_table" method="post" enctype="multipart/form-data" class="form-horizontal">
        @csrf
        <input type="hidden" name="renew" id="renew" value="0">
            <input type="hidden" name="title_check" id="title_check">
            <div class="container-fluid p-0">
                <div class="row">
                    <div class="input-panel">
                        <div class="mb-3">
                            <label for="" class="form-label">Asset Title</label>
                            <input type="text" name="content_title" id="content_title" class="form-control" onblur="check_title()" maxlength="50">
                            <span id="msg_content_title"></span>
                        </div>
                        <div class="mb-3">
                            <div class="light-area d-flex justify-content-between align-items-center">
                                <label for="" class="form-label m-0">Public?</label>
                                <label class="switch">
                                    <input type="checkbox" name="is_public">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="" class="form-label">Brand</label>
                            <select name="content_brand_id" id="content_brand_id" class="form-select">
                                <option value="0">Please select</option>
                                @foreach($brands as $brand)
                                    <option value="{{$brand['id']}}">{{$brand['brand_name']}}</option>
                                @endforeach
                            </select>
                            <span id="msg_content_brand"></span>
                        </div>
                        <div class="mb-3">
                            <label for="" class="form-label">Category</label>
                            <select name="content_category_id" id="content_category_id" class="form-select">
                                <option value="0">Please select</option>
                                @foreach($categorys as $category)
                                    <option value="{{$category['id']}}">{{$category['category_name']}}</option>
                                @endforeach
                            </select>
                            <span id="msg_content_category"></span>
                        </div>
                        <div class="mb-3">
                            <label for="" class="form-label">Asset File</label>
                            <div class="d-flex align-items-center">
                                <button type="button" class="btn btn-small btn-outline-primary me-2" @click="$refs.file.click()">UPLOAD</button>
                                <input type="file" name="upload_data" id="upload_data" class="d-none" ref="file" v-on:change="handleFileUpload()">
                                <div v-if="file.name!=null">@{{file.name}} <a class="delete-file" @click="deleteFile">Delete</a></div>
                            </div>
                            <span id="msg_content_file"></span>
                        </div>
                        <div class="mb-3">
                            <label for="" class="form-label">Thumbnail</label>
                            <div class="d-flex align-items-center">
                                <button type="button" class="btn btn-small btn-outline-primary me-2 text-nowrap" @click="$refs.sample_pic.click()">UPLOAD</button>
                                <input type="file" class="d-none" name="sample_pic" id="sample_pic" ref="sample_pic" v-on:change="samplePicUpload()">
                                <div v-if="sample_pic.name!=null">@{{sample_pic.name}} <a class="delete-file" @click="deleteSamplePic">Delete</a></div>
                                <div v-if="sample_msg!=null">@{{ sample_msg }}</div>
                            </div>
                            @error('sample_pic')
                                {{$message}}
                            @enderror
                            <span id="msg_sample_pic"></span>
                        </div>
                        <div class="mb-3">
                            <label for="" class="form-label">Description</label>
                            <textarea name="content_note" id="content_note" class="form-control" rows="10"></textarea>
                            <span id="msg_content_note"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="control-btns d-flex justify-content-center">
                <button class="btn btn-outline-primary btn-small" type="button" onclick="save_new()">SAVE & NEW</button>
                <button class="btn btn-primary btn-small" type="button" onclick="btn_submit()">SAVE & CLOSE</button>
            </div>
        </form>
    </div>
@endsection
@section('page-javascripts')
    <script>
        let app=new Vue({
            el: "#app",
            data:{
                file: "",
                sample_pic: "",
                sample_msg: "",
                pics: "",
            },
            methods: {
                handleFileUpload: function () {
                    this.file = this.$refs.file.files[0];
                },
                deleteFile(){
                    this.$refs.file.files[0]=null;
                    this.file="";
                },
                samplePicUpload: function(){
                    this.sample_pic = this.$refs.sample_pic.files[0];
                    if((this.sample_pic.type =='image/png') || (this.sample_pic.type =='image/jpeg')){
                        this.sample_msg = null;
                    }
                    else
                    {
                        this.sample_msg = 'Format Error, Please ReUpload.';
                        this.sample_pic = "";
                    }
                },
                deleteSamplePic: function(){
                    this.$refs.sample_pic.files[0]=null;
                    this.sample_pic="";
                }
            }
        })

        function add_new()
        {
            window.location = '/content/new'
        }

        function btn_submit()
        {
            let flag = check_submit()

            if(flag)
            {
                $("#content_table").submit()
            }
        }

        function save_new()
        {
            let flag = check_submit()

            if(flag)
            {
                $("#renew").val(1)
                $("#content_table").submit()
            }
        }

        function check_submit()
        {
            check_title();

            if($("#content_title").val() == '' )
            {
                $("#msg_content_title").html('<small class="form-text text-muted"><font color="red">Do not empty.</font></small>')
                return false
            }
            $("#msg_content_title").html('')

            if($("#content_brand_id").val() == '0')
            {
                $("#msg_content_brand").html('<small class="form-text text-muted"><font color="red">Please Choose One.</font></small>')
                return false
            }
            $("#msg_content_brand").html('')

            if($("#content_category_id").val() == '0')
            {
                $("#msg_content_category").html('<small class="form-text text-muted"><font color="red">Please Choose One.</font></small>')
                return false
            }
            $("#msg_content_category").html('')

            if($("#upload_data").val() == '' )
            {
                $("#msg_content_file").html('<small class="form-text text-muted"><font color="red">Do not empty.</font></small>')
                return false
            }
            $("#msg_content_file").html('')

            if($("#content_note").val() == '' )
            {
                $("#msg_content_note").html('<small class="form-text text-muted"><font color="red">Do not empty.</font></small>')
                return false
            }
            $("#msg_content_note").html('')

            if($("#title_check").val() == 0 )
            {
                return false
            }

            if( $("#sample_pic").val() != '' )
            {
                let sample_pic_name = $("#sample_pic").val();
                let allow = ['jpg','jpeg','png'];
                console.log(allow.indexOf( sample_pic_name.split('.')[1] ))
                if( allow.indexOf( sample_pic_name.split('.')[1] ) == -1  )
                {
                    console.log('format error')
                    return false
                }

                return true
            }

            return true
        }

        function check_title()
        {
            let content_title = $("#content_title").val();

            //後續等後端加上資料庫，並使用AJAX更新
            $.ajax({
                type: 'POST',
                url: '/content/check',
                data: {"_token": "{{ csrf_token() }}", "content_title": content_title},
                success: function(msg)
                {
                    console.log(msg)
                    if(msg == 'Title Already Exists.')
                    {
                        $("#msg_content_title").html('<small class="form-text text-muted"><font color="red">'+msg+'</font></small>')
                        $("#title_check").val(0)
                    }
                    else
                    {
                        $("#msg_content_title").html('<small class="form-text text-muted"><font color="green">'+msg+'</font></small>')
                        $("#title_check").val(1)
                    }
                }
            })
        }
    </script>
@endsection
