@extends('layouts.content')

@section('sidebar')
    @include('layouts.sidebar')
@endsection

@section('main-content')

    <div class="mask" id="mask">
        <div class="d-flex justify-content-center align-items-center">
            <div class="delete-confirm-modal text-center">
                <div>
                    <img style="width: 100px" src="{{asset('images/icon-close-danger.svg')}}" alt="">
                </div>
                <div class="confirm-words">Are you sure ?</div>
                <div style="margin-bottom: 60px;">Do you really want to delete this item?<br>This process cannot be undone.</div>
                <div>
                    <button class="btn btn-red" type="button" onclick="btn_delete()">DELETE</button>
                    <button class="btn btn-gray" type="button" onclick="btn_delete_cancel()">CANCEL</button>
                </div>
            </div>
        </div>
    </div>

    <div class="dashboard-title position-relative">
        <h1>File Manager <span>/ Edit File</span></h1>
    </div>
    <div class="main-content" id="app">
        @if(session('message'))
            <div class="warn-area">
                {{session('message')}}
            </div>
        @endif
        <form action="/content/modify" id="content_table" method="post" enctype="multipart/form-data"
              class="form-horizontal">
            @csrf
            <input type="hidden" name="renew" id="renew" value="0">
            <input type="hidden" name="content_id" id="content_id" value="{{$content['id']}}">
            <div class="container-fluid p-0">
                <div class="row">
                    <div class="input-panel">
                        <div class="mb-3">
                            <label for="" class="form-label">Asset Title</label>
                            <input type="text" id="content_title" name="content_title"
                                   value="{{$content['content_title']}}" class="form-control" maxlength="50">
                            <span id="msg_content_title"></span>
                        </div>
                        <div class="mb-3">
                            <div class="light-area d-flex justify-content-between align-items-center">
                                <label for="" class="form-label m-0">Public?</label>
                                <label class="switch">
                                    <input type="checkbox" name="is_public" id="is_public">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="" class="form-label">Brand</label>
                            <select name="content_brand_id" id="content_brand_id" class="form-select">
                                @foreach($brands as $brand)
                                    @if($brand['id'] == $content['content_brand_id'])
                                        <option value="{{$brand['id']}}" selected>{{$brand['brand_name']}}</option>
                                    @else
                                        <option value="{{$brand['id']}}">{{$brand['brand_name']}}</option>
                                    @endif
                                @endforeach
                            </select>
                            <span id="msg_content_brand"></span>
                        </div>
                        <div class="mb-3">
                            <label for="" class="form-label">Category</label>
                            <select name="content_category_id" id="content_category_id" class="form-select">
                                <option value="0">Please select</option>
                                @foreach($categorys as $category)
                                    @if($category['id'] == $content['content_category_id'])
                                        <option value="{{$category['id']}}"
                                                selected>{{$category['category_name']}}</option>
                                    @else
                                        <option value="{{$category['id']}}">{{$category['category_name']}}</option>
                                    @endif
                                @endforeach
                            </select>
                            <span id="msg_content_category"></span>
                        </div>
                        <div class="mb-3">
                            <label for="" class="form-label">Asset File</label>
                            <div class="d-flex align-items-center">
                                <button type="button" class="btn btn-outline-primary btn-small me-2"
                                        @click="$refs.file.click()">UPLOAD
                                </button>
                                <input type="file" name="upload_data" id="upload_data" class="d-none" ref="file"
                                       v-on:change="handleFileUpload()">
                                <div v-if="file.name!=null">@{{file.name}} <a class="delete-file" @click="deleteFile">Delete</a></div>
                            </div>
                            <small class="form-text text-muted">If you don't Upload File, Please Keep Empty.</small>
                        </div>
                        <span id="msg_content_file"></span>

                        <div class="mb-3">
                            <label for="" class="form-label">Thumbnail</label>
                            <div class="d-flex align-items-center">
                                <button type="button" class="btn btn-small btn-outline-primary me-2 text-nowrap" @click="$refs.sample_pic.click()">UPLOAD</button>
                                <input type="file" class="d-none" name="sample_pic" id="sample_pic" ref="sample_pic" v-on:change="samplePicUpload()">

                                <div v-if="sample_pic.name!=null">@{{sample_pic.name}} <a class="delete-file" @click="deleteSamplePic">Delete</a></div>
                                <div v-if="sample_msg!=null">@{{ sample_msg }} <a class="delete-file" @click="samplemsgdelete">Delete</a></div>
                            </div>
                            @error('sample_pic')
                            {{$message}}
                            @enderror
                            <span id="msg_sample_pic"></span>
                        </div>
                        <div class="mb-3">
                            <label for="" class="form-label">Description</label>
                            <textarea id="content_note" name="content_note" rows="9" placeholder="File Note"
                                      class="form-control">{{$content['content_note']}}</textarea>
                            <span id="msg_content_note"></span>
                        </div>
{{--                        <div class="mb-3">--}}
{{--                            <label for="" class="form-label">Temp Download Link</label>--}}
{{--                            @if($content['content_temp_download_path'])--}}
{{--                                <input type="text" id="content_temp_download_path" name="content_temp_download_path" value="{{url("/tmp_download/{$content['content_temp_download_path']}")}}" class="form-control" readonly>--}}
{{--                            @else--}}
{{--                                <input type="text" id="content_temp_download_path" name="content_temp_download_path" value="" class="form-control" readonly>--}}
{{--                            @endif--}}
{{--                            <span id="msg_content_title"></span>--}}
{{--                        </div>--}}
                    </div>
                </div>
            </div>
    </div>
    <div class="control-btns d-flex justify-content-center">
        <div>
            <button class="btn btn-outline-primary btn-small" type="button" onclick="save_new()">SAVE & NEW</button>
            <button class="btn btn-primary btn-small" type="button" onclick="btn_submit()">SAVE & CLOSE</button>
{{--            <button class="btn btn-outline-primary btn-small" type="button" onclick="create_link()">CREATE DOWNLOAD LINK</button>--}}
        </div>
        <button class="btn btn-red btn-small btn-right" type="button" onclick="btn_delete_show()">DELETE</button>
    </div>
    </form>
    </div>
@endsection
@section('page-javascripts')
    <script>
        let test_txt
        @if($content['content_sample_pic'])
            test_txt = {'name': '{{$content['content_sample_pic']}}'}
        @else
            test_txt = ''
        @endif
        let app = new Vue({
            el: "#app",
            data: {
                file: "",
                sample_pic: test_txt,
                sample_msg: null
            },
            methods: {
                handleFileUpload: function () {
                    this.file = this.$refs.file.files[0];
                    console.log(this.file)
                },
                deleteFile(){
                    this.$refs.file.files[0]=null;
                    this.file="";
                },
                samplePicUpload: function(){
                    this.sample_pic = this.$refs.sample_pic.files[0];
                    if((this.sample_pic.type =='image/png') || (this.sample_pic.type =='image/jpeg')){
                        this.sample_msg = null;
                    }
                    else
                    {
                        this.sample_msg = 'Format Error, Please ReUpload.';
                        this.sample_pic = "";
                    }
                },
                samplemsgdelete: function(){
                    this.sample_msg = null;
                    this.$refs.sample_pic.files[0]=null;
                },
                deleteSamplePic: function(){
                    this.$refs.sample_pic.files[0]=null;
                    this.sample_pic="";

                    //後端處理圖片
                    //後續等後端加上資料庫，並使用AJAX更新
                    $.ajax({
                        type: 'POST',
                        url: '/content/delete/pic',
                        data: {"_token": "{{ csrf_token() }}", "content_id": "{{$content['id']}}" },
                        success: function(msg)
                        {
                            console.log(msg)
                            $("#alert_msg").show();
                            $("#alertmessage").html('Sample Pic Deleted.');
                        }
                    })
                }
            }
        })

        $(document).ready(function () {
            var is_public = '{{$content['is_public']}}'
            if (is_public == 'on') {
                $("#is_public").attr('checked', 'checked')
            }
        })

        function btn_submit() {
            let flag = check_submit()

            if (flag) {
                $("#content_table").submit()
            }
        }

        function save_new() {
            let flag = check_submit()

            if (flag) {
                $("#renew").val(1)
                $("#content_table").submit()
            }
        }

        function btn_delete_show()
        {
            $("#mask").fadeIn(300)
        }

        function btn_delete()
        {
            $("#content_table").attr('action','/content/delete')
            $("#content_table").submit()
        }

        function create_link()
        {
            $("#content_table").attr('action','/content/create_link')
            $("#content_table").submit()
        }

        function btn_delete_cancel()
        {
            $("#mask").fadeOut(300)
        }

        function check_submit() {
            if ($("#content_title").val() == '') {
                $("#msg_content_title").html('<small class="form-text text-muted"><div color="red">Do not empty.</div></small>')
                return false
            }
            $("#msg_content_title").html('')

            if ($("#content_brand_id").val() == '0') {
                $("#msg_content_brand").html('<small class="form-text text-muted"><div color="red">Please Choose One.</div></small>')
                return false
            }
            $("#msg_content_brand").html('')

            if ($("#content_category_id").val() == '0') {
                $("#msg_content_category").html('<small class="form-text text-muted"><div color="red">Please Choose One.</div></small>')
                return false
            }
            $("#msg_content_category").html('')

            if ($("#content_note").val() == '') {
                $("#msg_content_note").html('<small class="form-text text-muted"><div color="red">Do not empty.</div></small>')
                return false
            }
            $("#msg_content_note").html('')

            if( $("#sample_pic").val() != '' )
            {
                let sample_pic_name = $("#sample_pic").val();
                let allow = ['jpg','jpeg','png'];
                console.log(allow.indexOf( sample_pic_name.split('.')[1] ))
                if( allow.indexOf( sample_pic_name.split('.')[1] ) == -1  )
                {
                    console.log('format error')
                    return false
                }

                return true
            }

            return true
        }
    </script>
@endsection
