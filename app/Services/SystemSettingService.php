<?php
namespace App\Services;

use App\Repositories\SystemSettingRepository;

class SystemSettingService
{
    public function __construct(SystemSettingRepository $systemSettingRepository)
    {
        $this->_rep_setting = $systemSettingRepository;
    }

    public function get_value()
    {
        $result = $this->_rep_setting->get_value();

        $return_arr = [];

        foreach($result as $item)
        {
            $return_arr[$item['system_key']] = $item['system_value'];
        }

        return $return_arr;
    }

    public function update_value($params)
    {
        $result = $this->_rep_setting->update_value($params);

        return $result;
    }
}
