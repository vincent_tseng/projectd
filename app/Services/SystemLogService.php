<?php
namespace App\Services;

use App\Repositories\MemberRepository;
use App\Repositories\SystemLogRepository;

class SystemLogService
{
    public function __construct(
        SystemLogRepository $systemLogRepository,
        MemberRepository $memberRepository
    )
    {
        $this->_rep_syslog = $systemLogRepository;
        $this->_rep_member = $memberRepository;
    }

    public function get_logs()
    {
        $logs = $this->_rep_syslog->get_logs();
        foreach($logs as &$item )
        {
            if($item->log_userid)
            {
                $member = $this->_rep_member->get_member($item->log_userid);
                $item->user_name = $member['member_name'];
            }
        }

        return $logs;
    }

    public function get_logs_by_search($option)
    {
        $logs = $this->_rep_syslog->get_logs_by_search($option);
        foreach($logs as &$item )
        {
            if(!is_null($item->log_userid))
            {
                $member = $this->_rep_member->get_member($item->log_userid);
                $item->user_name = $member['member_name'];
            }
            else
            {
                $item->user_name = '';
            }
        }

        return $logs;
    }
}
