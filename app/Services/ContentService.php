<?php
namespace App\Services;

use App\Repositories\BrandRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\ContentRepository;

class ContentService
{
    public function __construct(
        ContentRepository $contentRepository,
        CategoryRepository $categoryRepository,
        BrandRepository $brandRepository
    )
    {
        $this->_rep_content = $contentRepository;
        $this->_rep_category = $categoryRepository;
        $this->_rep_brand = $brandRepository;
    }

    public function get_content($id = '')
    {
        if($id)
        {
            $contents = $this->_rep_content->get_content($id);
        }
        else
        {
            $contents = $this->_rep_content->get_contents();
            foreach($contents as &$item )
            {
                $arr_category = $this->_rep_category->get_category($item['content_category_id']);
                if($arr_category)
                {
                    $item['content_category_name'] = $arr_category['category_name'];
                }
                else
                {
                    $item['content_category_name'] = "N/A";
                }


                $arr_brand = $this->_rep_brand->get_brand($item['content_brand_id']);
                if($arr_brand)
                {
                    $item['content_brand_name'] = $arr_brand['brand_name'];
                }
                else
                {
                    $item['content_brand_name'] = "N/A";
                }
            }
        }

        return $contents;
    }

    //download用
    public function get_public_content($user)
    {
        $contents = $this->_rep_content->get_public_content($user);
        foreach($contents as &$item )
        {
            $arr_category = $this->_rep_category->get_category($item['content_category_id']);
            if($arr_category)
            {
                $item['content_category_name'] = $arr_category['category_name'];
            }
            else
            {
                $item['content_category_name'] = "N/A";
            }


            $arr_brand = $this->_rep_brand->get_brand($item['content_brand_id']);
            if($arr_brand)
            {
                $item['content_brand_name'] = $arr_brand['brand_name'];
            }
            else
            {
                $item['content_brand_name'] = "N/A";
            }
        }

        return $contents;
    }

    public function get_download_file($params, $type)
    {
        switch($type)
        {
            case 'multi':
                $contents = $this->_rep_content->get_file_path($params);
                break;
            case 'single':
                $contents = $this->_rep_content->get_signle_file($params);
                break;
        }

        return $contents;
    }

    public function get_brand($user)
    {
        $brand = $this->_rep_brand->get_public_brands($user);

        return $brand;
    }

    public function get_category()
    {
        $category = $this->_rep_category->get_categorys();

        return $category;
    }

    public function get_content_by_search($params)
    {
        $contents = $this->_rep_content->get_content_by_search($params);

        if($contents)
        {
            foreach($contents as &$item )
            {
                $arr_category = $this->_rep_category->get_category($item['content_category_id']);
                if($arr_category)
                {
                    $item['content_category_name'] = $arr_category['category_name'];
                }
                else
                {
                    $item['content_category_name'] = "N/A";
                }


                $arr_brand = $this->_rep_brand->get_brand($item['content_brand_id']);
                if($arr_brand)
                {
                    $item['content_brand_name'] = $arr_brand['brand_name'];
                }
                else
                {
                    $item['content_brand_name'] = "N/A";
                }
            }

            return $contents;
        }
        return false;
    }

    public function get_content_by_download($params)
    {
        $contents = $this->_rep_content->get_content_by_download($params);

        foreach($contents as &$item )
        {
            $arr_category = $this->_rep_category->get_category($item['content_category_id']);
            if($arr_category)
            {
                $item['content_category_name'] = $arr_category['category_name'];
            }
            else
            {
                $item['content_category_name'] = "N/A";
            }


            $arr_brand = $this->_rep_brand->get_brand($item['content_brand_id']);
            if($arr_brand)
            {
                $item['content_brand_name'] = $arr_brand['brand_name'];
            }
            else
            {
                $item['content_brand_name'] = "N/A";
            }
        }

        return $contents;
    }

    public function insert_content($params)
    {
        $insert_params = [
            'content_title' => $params['content_title'],
            'content_note' => $params['content_note'],
            'content_brand_id' => $params['content_brand_id'],
            'content_category_id' => $params['content_category_id'],
            'content_upload_path' => $params['content_upload_path'],
            'created_by' => $params['created_by'],
            'created_at' => date('Y-m-d H:i:s')
        ];

        if(isset($params['content_sample_pic']))
        {
            $insert_params['content_sample_pic'] = $params['content_sample_pic'];
        }

        if(isset($params['is_public']))
        {
            $insert_params['is_public'] = $params['is_public'];
        }

        $result = $this->_rep_content->insert_content($insert_params);

        return $result;
    }

    public function update_content($params)
    {
        $update_params = [
            'content_title' => $params['content_title'],
            'content_note' => $params['content_note'],
            'content_brand_id' => $params['content_brand_id'],
            'content_category_id' => $params['content_category_id'],
            'id' => $params['content_id']
        ];
        if(isset($params['content_upload_path']))
        {
            $update_params['content_upload_path'] = $params['content_upload_path'];
        }

        if(isset($params['content_sample_pic']))
        {
            $update_params['content_sample_pic'] = $params['content_sample_pic'];
        }

        if(isset($params['is_public']))
        {
            $update_params['is_public'] = $params['is_public'];
        }
        else
        {
            $update_params['is_public'] = null;
        }

        $result = $this->_rep_content->update_content($update_params);

        return $result;
    }

    public function check_title($params)
    {
        $result = $this->_rep_content->check_title($params);

        return $result;
    }

    public function delete($id)
    {
        $result = $this->_rep_content->delete($id);

        return $result;
    }

    public function delete_pic($id)
    {
        $result = $this->_rep_content->delete_pic($id);

        return $result;
    }

    public function get_download_path($param)
    {
        $token = [
            'content_id' => $param['content_id'],
            'time'=>time()
        ];

        //$user轉成JSON字串用base64加密 -> 進入資料庫
        $token = base64_encode(json_encode($token));

        $this->_rep_content->download_path($token,$param['content_id']);
    }

    public function check_download_token($token)
    {
        $files = $this->_rep_content->get_file_path_bytoken($token);
        if($files)
        {
            $arr_token = json_decode(base64_decode($token),true);
            $now_time = time();
            $diff = $now_time - $arr_token['time'];

            //密碼重置
            if($diff >= 86400 )
            {
                return 'EXPIRED';
            }

            $download = [
                'content_id' => $files['content_id'],
                'content_upload_path' => $files['content_upload_path']
            ];

            return $download;
        }

        return false;
    }
}
