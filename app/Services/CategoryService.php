<?php

namespace App\Services;

use App\Repositories\CategoryRepository;
use App\Repositories\FieldsRepository;

class CategoryService
{
    public function __construct(
        CategoryRepository $categoryRepository,
        FieldsRepository $fieldsRepository
    )
    {
        $this->_rep_category = $categoryRepository;
        $this->_rep_fields = $fieldsRepository;
    }

    public function get_category($id = '')
    {
        if($id)
        {
            $categorys = $this->_rep_category->get_category($id);
        }
        else
        {
            $categorys = $this->_rep_category->get_categorys();
        }

        return $categorys;
    }

    public function get_fields()
    {
        $fields = $this->_rep_fields->get_fields();

        return $fields;
    }


    public function insert_category($params)
    {
        $max_order = $this->_rep_category->get_category_maxorder();

        $category_order = $max_order + 1;
        //處理field

//        $category_fields = json_encode($params['category_fields']);

        $insert_params = [
            'category_name' => $params['category_name'],
            'category_note' => $params['category_note'],
            'category_order' => $category_order,
//            'category_fields' => $category_fields,
            'created_at' => date('Y-m-d H:i:s')
        ];

        $result = $this->_rep_category->insert_category($insert_params);

        return $result;
    }

    public function update_category($params)
    {
//        $category_fields = json_encode($params['category_fields']);

        $update_arr = [
            'category_name' => $params['category_name'],
            'category_note' => $params['category_note'],
//            'category_fields' => $category_fields,
            'id' => $params['category_id']
        ];

        $result = $this->_rep_category->update_category($update_arr);

        return $result;
    }

    public function update_category_order($params)
    {
        $result = $this->_rep_category->update_category_order($params);

        return $result;
    }

    public function check_category_name($params)
    {
        $result = $this->_rep_category->check_category_name($params);

        return $result;
    }

    public function delete($id)
    {
        $result = $this->_rep_category->delete($id);

        return $result;
    }
}
