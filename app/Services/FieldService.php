<?php
namespace App\Services;

use App\Repositories\FieldsRepository;

class FieldService
{
    public function __construct(
        FieldsRepository $fieldsRepository
    )
    {
        $this->_rep_field = $fieldsRepository;
    }

    public function get_field($id = '')
    {
        if($id)
        {
            $fields = $this->_rep_field->get_field($id);
        }
        else
        {
            $fields = $this->_rep_field->get_fields();
        }

        return $fields;
    }

    public function check_field($params)
    {
        $result = $this->_rep_field->check_field($params);

        return $result;
    }

    public function insert_field($params)
    {
        $max_order = $this->_rep_field->get_field_maxorder();

        $field_order = $max_order + 1;

        $insert_params = [
            'field_name' => $params['field_name'],
            'field_note' => $params['field_note'],
            'field_order' => $field_order,
            'created_at' => date('Y-m-d H:i:s')
        ];

        if(isset($params['is_require']))
        {
            $insert_params['is_require'] = $params['is_require'];
        }
        if(isset($params['is_search']))
        {
            $insert_params['is_search'] = $params['is_search'];
        }
        if(isset($params['is_hide']))
        {
            $insert_params['is_hide'] = $params['is_hide'];
        }

        $result = $this->_rep_field->insert_field($insert_params);

        return $result;
    }

    public function update_field($params)
    {
        $update_arr = [
            'field_name' => $params['field_name'],
            'field_note' => $params['field_note'],
            'id' => $params['field_id']
        ];

        if(isset($params['is_require']))
        {
            $update_arr['is_require'] = $params['is_require'];
        }
        else
        {
            $update_arr['is_require']  = null;
        }


        if(isset($params['is_search']))
        {
            $update_arr['is_search'] = $params['is_search'];
        }
        else
        {
            $update_arr['is_search']  = null;
        }


        if(isset($params['is_hide']))
        {
            $update_arr['is_hide'] = $params['is_hide'];
        }
        else
        {
            $update_arr['is_hide']  = null;
        }


        $result = $this->_rep_field->update_field($update_arr);

        return $result;
    }

    public function update_field_order($params)
    {
        $result = $this->_rep_field->update_field_order($params);

        return $result;
    }

    public function delete($id)
    {
        $result = $this->_rep_field->delete($id);

        return $result;
    }
}
