<?php
namespace App\Services;

use App\Repositories\MemberRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class MemberService
{
    private $_rep_member;

    public function __construct(
        MemberRepository $memberRepository,
        Request $request
    )
    {
        $this->_rep_member = $memberRepository;
        $this->_request = $request;
    }

    public function sp_login($params)
    {
        $result = $this->action_sp_login($params);

        return $result;
    }

    public function check_member($account)
    {
        $result = $this->_rep_member->member_check($account);
        return $result;
    }

    public function check_group_name($params)
    {
        $result = $this->_rep_member->check_group_name($params);

        return $result;
    }

    public function check_login($account, $passwd)
    {
        $member = $this->_rep_member->get_member_byemail($account);

        if ($member) {

            //如果使用者帳號被刪除
            if($member['is_delete'] == 1)
            {
                return 'deleted';
            }

            //如果 member rule banned
            $member_rule = $this->_rep_member->get_memberGroup($member['member_group_id']);
            if($member_rule['is_login'] != 'on')
            {
                return 'Lfalse';
            }

            //檢查密碼與是否狀態為INACTIVE
            if(Hash::check($passwd,$member['member_passwd']) && $member['member_status'] == 'on')
            {
                $token = [
                    'id'=> $member['id'],
                    'time'=>time()
                ];

                //$user轉成JSON字串用base64加密 -> 進入資料庫
                $token = base64_encode(json_encode($token));

                $this->_rep_member->update_member_token($token, $member['id']);
                $return_token = [
                    'token' => $token,
                    'member_group_id' => $member['member_group_id'],
                    'member_name' => $member['member_name'],
                ];

                return $return_token;
            }
        }

        return false;
    }

    public function verify_login_token($token, $type = '')
    {
        $arr_token = json_decode(base64_decode($token),true);
        $now_time = time();
        $diff = $now_time - $arr_token['time'];

        $member = $this->_rep_member->get_member_by_token($token);

        if($member)
        {
            if($type == 'Reset')
            {
                //密碼重置
                if($diff >= 86400 )
                {
                    return 'EXPIRED';
                }

                $user = [
                    'id' => $member['id']
                ];

                $this->_request->session()->put('user',$user);
                $this->_request->session()->save();
            }
            else
            {
                if($diff >= 300 )
                {
                    return 'EXPIRED';
                }

                //登入驗證
                $user = [
                    'id' => $member['id'],
                    'name' => $member['member_name'],
                    'email' => $member['member_email'],
                    'member_group' => $member['member_group_role'],
                    'access_control' => $member['access_control'],
                    'member_group_name' => $member['member_group_name'],
                    'times' => time()
                ];

                $this->_request->session()->put('user',$user);
                $this->_request->session()->save();
            }

            return 'OK';
        }

        return 'FALSE';
    }

    public function get_member_by_search($params)
    {
        if($params['member_status'] == 'DELETED')
        {
            $params['is_delete'] = 1;
            unset($params['member_status']);
        }

        $members = $this->_rep_member->get_member_by_search($params);

        if($members)
        {
            foreach($members as &$item)
            {
                $membergroup = $this->_rep_member->get_memberGroup($item['member_group_id']);
                if(!empty($membergroup))
                {
                    $item['member_group_name'] = $membergroup['member_group_name'];
                }
                else
                {
                    $item['member_group_name'] = "N/A";
                }
            }

            return $members;
        }
        return false;
    }

    public function get_members($id = '')
    {
        if($id)
        {
            $members = $this->_rep_member->get_member($id);
        }
        else
        {
            $members = $this->_rep_member->get_members();
            foreach($members as &$item)
            {
                $membergroup = $this->_rep_member->get_memberGroup($item['member_group_id']);
                if(!empty($membergroup))
                {
                    $item['member_group_name'] = $membergroup['member_group_name'];
                }
                else
                {
                    $item['member_group_name'] = "N/A";
                }
            }
        }

        return $members;
    }

    public function get_memberGroup_by_newAccount()
    {
        $member_groups = $this->_rep_member->get_memberGroup_by_newAccount();

        return $member_groups;
    }

    public function account_request($params)
    {
        $member_groups = $this->_rep_member->get_memberGroup_by_newAccount();

        $insert_params = [
            'member_name' => $params['member_name'],
            'member_email' => $params['member_email'],
            'company_name' => $params['company_name'],
            'contact_name' => $params['contact_name'],
            'contact_email' => $params['contact_email'],
            'note' => $params['note'],
            'member_status' => Null,
            'member_group_id' => $member_groups['id'],
            'member_passwd' => Hash::make($params['member_name']),
            'created_at' => date('Y-m-d H:i:s')
        ];

        $result = $this->_rep_member->insert_member($insert_params);

        return $result;
    }

    public function insert_member($params)
    {
        $insert_params = [
            'member_name' => $params['member_name'],
            'member_group_id' => $params['member_group_id'],
            'member_email' => $params['member_email'],
            'member_passwd' => Hash::make($params['member_passwd']),
            'created_at' => date('Y-m-d H:i:s'),
            'company_name' => $params['company_name'],
            'contact_name' => $params['contact_name'],
            'contact_email' => $params['contact_email'],
            'note' => $params['note'],
        ];

        if(isset($params['member_status']))
        {
            $insert_params['member_status'] = $params['member_status'];
        }

        $result  = $this->_rep_member->insert_member($insert_params);

        return $result;
    }

    public function update_member($params)
    {
        $update_params = [
            'member_name' => $params['member_name'],
            'member_group_id' => $params['member_group_id'],
            'member_email' => $params['member_email'],
            'id'=> $params['member_id']
        ];

        if(!is_null($params['member_passwd']))
        {
            $update_params['member_passwd'] = Hash::make($params['member_passwd']);
        }

        if(isset($params['member_status']))
        {
            $update_params['member_status'] = $params['member_status'];
        }
        else
        {
            $update_params['member_status']  = null;
        }

        $result  = $this->_rep_member->update_member($update_params);

        return $result;
    }

    public function get_memberGroups($id = '')
    {
        if($id)
        {
            $memberGroups = $this->_rep_member->get_memberGroup($id);
            $arr_role = json_decode($memberGroups['member_group_role'],true);

            $memberGroups['brands'] = $arr_role['brands'];
            $memberGroups['allow_manager'] = $arr_role['allow_manager'];
            $memberGroups['allow_actions'] = $arr_role['allow_actions'];
        }
        else
        {
            $memberGroups = $this->_rep_member->get_memberGroups();
        }

        return $memberGroups;
    }

    public function get_public_memberGroup()
    {
        $memberGroups = $this->_rep_member->get_memberGroups();

        return $memberGroups;
    }

    public function insert_membergroup($params)
    {
        if(isset($params['is_public']))
        {
            $insert_params['is_public'] = $params['is_public'];
            $allow_manager = $params['allow_manager'];
        }
        else
        {
            $insert_params['is_public']  = null;
            $allow_manager = [];
        }

        if(isset($params['is_login']))
        {
            $insert_params['is_login'] = $params['is_login'];
        }
        else
        {
            $insert_params['is_login']  = null;
        }

        $roles = [
            'brands' => $params['brands'],
            'allow_manager' => $allow_manager,
        ];

        if(isset($params['allow_actions']))
        {
            $roles['allow_actions'] = $params['allow_actions'];
        }
        else
        {
            $roles['allow_actions'] = [];
        }

        $insert_params['member_group_name'] = $params['member_group_name'];
        $insert_params['member_group_note'] = $params['member_group_note'];
        $insert_params['member_group_role'] = json_encode($roles);
        $insert_params['created_at'] = date('Y-m-d H:i:s');

        if(isset($params['is_public']))
        {
            $insert_params['is_public'] = $params['is_public'];
        }

        $result = $this->_rep_member->insert_membergroup($insert_params);

        return $result;

    }

    public function update_memberGroup($params)
    {
        if(isset($params['is_public']))
        {
            $update_params['is_public'] = $params['is_public'];
            $allow_manager = $params['allow_manager'];
        }
        else
        {
            $update_params['is_public']  = null;
            $allow_manager = [];
        }

        if(isset($params['is_login']))
        {
            $update_params['is_login'] = $params['is_login'];
        }
        else
        {
            $update_params['is_login']  = null;
        }

        $roles = [
            'brands' => $params['brands'],
            'allow_manager' => $allow_manager,
        ];

        if(isset($params['allow_actions']))
        {
            $roles['allow_actions'] = $params['allow_actions'];
        }
        else
        {
            $roles['allow_actions'] = [];
        }

        $update_params['id'] = $params['membergroup_id'];
        $update_params['member_group_name'] = $params['member_group_name'];
        $update_params['member_group_note'] = $params['member_group_note'];
        $update_params['member_group_role'] = json_encode($roles);
        $update_params['created_at'] = date('Y-m-d H:i:s');


        $result = $this->_rep_member->update_membergroup($update_params);

        return $result;
    }

    public function forget_passwd($email)
    {
        $member = $this->_rep_member->get_member_byemail($email);

        if ($member) {
            $token = [
                'id'=> $member['id'],
                'time'=>time()
            ];

            //$user轉成JSON字串用base64加密 -> 進入資料庫
            $token = base64_encode(json_encode($token));

            $this->_rep_member->update_member_token($token, $member['id']);
            $return_token = [
                'token' => $token,
                'member_group_id' => $member['member_group_id'],
                'member_name' => $member['member_name']
            ];

            return $return_token;
        }

        return false;
    }

    public function setting_passwd($member_id, $passwd)
    {
        $update = [
            'member_passwd' => Hash::make($passwd),
        ];

        $result = $this->_rep_member->reset_passwd($member_id, $update);

        return $result;
    }

    public function delete($id,$type)
    {
        $result = $this->_rep_member->delete($id,$type);

        return $result;
    }

    public function restore($id)
    {
        $result = $this->_rep_member->restore($id);

        return $result;
    }

    private function action_sp_login($params)
    {
        $sp_login = $this->_rep_member->sp_login_check($params);
        if($sp_login)
        {
            if(Hash::check($params['passwd'], $sp_login['member_passwd']))
            {
                $user = [
                    'id' => $sp_login['id'],
                    'name' => $sp_login['member_name'],
                    'email' => $sp_login['member_email'],
                    'member_group' => $sp_login['member_group_role'],
                    'access_control' => $sp_login['access_control'],
                    'member_group_name' => $sp_login['member_group_name'],
                    'times' => time()
                ];

                $this->_request->session()->put('user',$user);
                $this->_request->session()->save();

                return 'OK';
            }
        }

        return false;
    }


}
