<?php
namespace App\Services;

use App\Repositories\BrandRepository;

class BrandService
{
    public function __construct(
        BrandRepository $brandRepository
    )
    {
        $this->rep_brand = $brandRepository;
    }

    public function get_brands($idx = '')
    {
        if($idx)
        {
            $brands = $this->rep_brand->get_brand($idx);
        }
        else
        {
            $brands = $this->rep_brand->get_brands();
        }

        return $brands;
    }

    public function get_public_brands($user)
    {
        $brands = $this->rep_brand->get_public_brands($user);

        return $brands;
    }

    public function new_brand($params)
    {
        $max_order = $this->rep_brand->get_brand_maxorder();

        $brand_order = $max_order + 1;

        $insert_params = [
            'brand_name' => $params['brand_name'],
            'brand_short_name' => $params['brand_short_name'],
            'brand_order' => $brand_order,
            'created_at' => date('Y-m-d H:i:s')
        ];
        if(isset($params['is_public']))
        {
            $insert_params['is_public'] = $params['is_public'];

        }

        $result = $this->rep_brand->insert_brand($insert_params);

        return $result;
    }

    public function update_brand($params)
    {
        $update_arr = [
            'brand_name' => $params['brand_name'],
            'brand_short_name' => $params['brand_short_name'],
            'id' => $params['brand_id']
        ];
        if(isset($params['is_public']))
        {
            $update_arr['is_public'] = $params['is_public'];
        }
        else
        {
            $update_arr['is_public'] = null;
        }
        $result = $this->rep_brand->update_brand($update_arr);

        return $result;
    }

    public function update_brand_order($params)
    {
        $result = $this->rep_brand->update_brand_order($params);

        return $result;
    }

    public function check_brand($params)
    {
        $result = $this->rep_brand->check_brand_name($params);

        return $result;
    }

    public function delete($id)
    {
        $result = $this->rep_brand->delete($id);

        return $result;
    }
}
