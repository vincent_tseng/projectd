<?php

namespace App\Traits;


use Illuminate\Support\Facades\DB;

trait SystemLogTrait
{
    public function Actionlog($type,$action)
    {
        $ip = \Request::ip();
        $userid = \Request::session()->get('user');
        $action = json_encode($action);

        $insert_params = [
            'log_type' => $type,
            'log_ip' => $ip,
            'log_action' => $action,
            'log_userid' => $userid['id'],
            'created_at' => date('Y-m-d H:i:s')
        ];
        DB::table('systemlog')->insert($insert_params);
    }
}
