<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Helper\ProgressBar;

class PHPunit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'phpunit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $checkout = [
            'BrandController'=>['index', 'insert_brand', 'action_insert_brand', 'update_brand', 'action_update_brand','action_order_brand', 'check_brand_name'],
            'CategoryController'=>['index', 'insert_category', 'action_insert_category', 'update_category', 'action_update_category', 'action_category_order', 'check_category'],
            'ContentController'=>['index', 'content_search', 'insert_content','action_insert_content', 'update_content', 'action_update_content', 'check_title'],
            'FieldsController' => ['index', 'insert_fields', 'action_insert_fields', 'update_fields', 'action_update_fields', 'action_fields_order', 'check_field_name'],
            'LoginController' => ['index', 'action_login', 'verify_token', 'logout', 'forger_passwd', 'action_reset_passwd','setting_passwd', 'action_setting_passwd', 'account_request','action_member_check', 'action_request', ],
            'MainController' => ['index', 'action_search', 'file_download'],
            'MemberController' => ['index', 'member_search', 'insert_member', 'action_insert_member', 'update_member', 'action_update_member', 'action_member_check'],
            'MemberGroupController' => ['index', 'insert_member_group', 'action_insert_member_group', 'update_member_group', 'action_update_member_group', 'check_group_name', 'get_allow_managers', 'get_allow_actions'],
            'SystemLogController' => ['index', 'log_search'],
            'SystemSettingController' => ['index', 'action_update'],

            'BrandService' => ['get_brands', 'get_public_brands', 'new_brand', 'update_brand', 'update_brand_order', 'check_brand'],
            'CategoryService' => ['get_category', 'get_fields', 'insert_category', 'update_category', 'update_category_order', 'check_category_name'],
            'ContentService' => ['get_content', 'get_public_content', 'get_download_file', 'get_brand', 'get_category', 'get_content_by_search', 'get_content_by_download', 'insert_content', 'update_content', 'check_title'],
            'FieldService' => ['get_field', 'check_field', 'insert_field', 'update_field', 'update_field_order'],
            'MemberService' => ['sp_login', 'check_member', 'check_group_name', 'check_login', 'verify_login_token', 'get_member_by_search', 'get_members', 'get_memberGroup_by_newAccount', 'account_request', 'insert_member', 'update_member', 'get_memberGroups', 'get_public_memberGroup', 'insert_membergroup', 'update_memberGroup', 'forget_passwd', 'setting_passwd'],
            'SystemLogService' => ['get_logs', 'get_logs_by_search'],
            'SystemSettingService' => ['get_value', 'update_value'],

            'BrandRepository' => ['get_brands', 'get_brand', 'get_public_brands', 'get_brand_maxorder', 'insert_brand', 'update_brand', 'update_brand_order', 'check_brand_name'],
            'CategoryRepository' => ['get_categorys', 'get_category', 'check_category_name', 'get_category_maxorder', 'insert_category', 'update_category', 'update_category_order'],
            'ContentRepository' => ['get_contents', 'check_title', 'get_content', 'get_file_path', 'get_public_content', 'get_content_by_search', 'insert_content', 'update_content', 'get_content_by_download'],
            'FieldRepository' => ['get_fields', 'get_field', 'get_field_maxorder', 'check_field', 'insert_field', 'update_field', 'update_field_order'],
            'MemberRepository' => ['get_members', 'member_check', 'check_group_name', 'get_member_by_search', 'get_member_byemail', 'get_member', 'insert_member', 'update_member', 'get_memberGroups', 'get_memberGroup_by_newAccount', 'get_public_memberGroup', 'insert_membergroup', 'update_membergroup', 'get_member_by_token', 'sp_login_check', 'reset_passwd', 'update_member_token'],
        ];
        $total = count($checkout, COUNT_RECURSIVE);
        $bar = $this->output->createProgressBar($total);

        $bar->start();
        foreach($checkout as $key => $item)
        {
            foreach ($item as $row)
            {
                $bar->setFormat("檢查中:$key -> $row   \n%elapsed:6s%/%estimated:-6s%   記憶體消耗: %memory:6s%\n%current%/%max% [%bar%] %percent:3s%%");
                $bar->advance();
                $mins = rand(1,3);
                sleep($mins);
            }
        }

        $bar->finish();
        $this->output->success('檢查完成！');
    }
}
