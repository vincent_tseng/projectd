<?php
namespace App\Http\Controllers;

use App\Services\SystemLogService;
use Illuminate\Http\Request;

class SystemLogController extends Controller
{
    public function __construct(
        SystemLogService $systemLogService,
        Request $request
    )
    {
        $this->_ser_syslog = $systemLogService;
        $this->_request = $request;
    }

    public function index()
    {
        $options = $this->_select_menu();
        $params = $this->_request->get('log_type');
        $log_type = '';
        if(is_null($params))
        {
            $logs = $this->_ser_syslog->get_logs();
        }
        else
        {
            $search['log_type'] = $params;
            $logs = $this->_ser_syslog->get_logs_by_search($search);
            $log_type = $params;
        }

        $data = [
            'logs' => $logs,
            'title' => 'System Logs',
            'options' => $options,
            'log_type' => $log_type,
            'page_data' => '',
        ];
        $this->Actionlog('Click',['action'=>'SystemLogs.Index']);
        return view('SystemLog.index', $data);
    }

    public function log_search()
    {
        $params = $this->_request->post();
        $options = $this->_select_menu();

        $logs = $this->_ser_syslog->get_logs_by_search($params);

        $page_data = json_encode(['log_type' => $params['log_type']]);

        $data = [
            'logs' => $logs,
            'options' => $options,
            'log_type' => $params['log_type'],
            'page_data' => $page_data,
            'title' => 'System Logs'
        ];
        $this->Actionlog('Search',['action'=>'SystemLogs.Search','Params' => $params]);
        return view('SystemLog.index', $data);
    }

    private function _select_menu()
    {
        $options  = [
            "Click"=>"Click",
            "Insert"=>"Insert",
            "Update"=>"Update",
            "Search"=>"Search",
            "Login"=>"Login",
            "Logout"=>"Logout",
            "ForgetPasswd"=>"ForgetPasswd",
            "ResetPasswd"=>"ResetPasswd",
            "Download" => "Download",
            "Restore" => "Restore",
            "AccountRequest" => "AccountRequest",
        ];

        return $options;
    }
}
