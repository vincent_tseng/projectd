<?php

namespace App\Http\Controllers;

use App\Services\BrandService;
use App\Services\CategoryService;
use App\Services\ContentService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class MainController extends Controller
{

    private $_user;
    private $_content;
    private $_request;
    private $_brand;
    private $_category;

    public function __construct(
        Request $request,
        ContentService $contentService,
        CategoryService $categoryService,
        BrandService $brandService
    )
    {
        $this->_request = $request;
        $this->_content = $contentService;
        $this->_brand = $brandService;
        $this->_category = $categoryService;
    }

    //
    public function index()
    {
        $control_url = $this->_controlUrl();
        $brands = $this->_brand->get_public_brands($this->_user);
        $categories = $this->_category->get_category();

        //分頁用
        $brand_id = is_null($this->_request->get('brand_id')) ? '' : $this->_request->get('brand_id') ;
        $category_id = is_null($this->_request->get('category_id')) ? '' : $this->_request->get('category_id');
        $download_search = $this->_request->get('download_search');
        //搜尋用
        $used_brand_id = json_decode($brand_id,true);
        $used_category_id = json_decode($category_id,true);

        $link_search = [
            'brand_id' => $brand_id,
            'category_id' => $category_id,
            'download_search' => $download_search,
        ];

        if( ($brand_id == "") && ($category_id == "") && (is_null($download_search)) )
        {
            $user = $this->_user;
            $contents = $this->_content->get_public_content($user);
        }
        else
        {
            $search = [
                'brand_id' => $used_brand_id,
                'category_id' => $used_category_id,
                'download_search' => $download_search,
                'user' => $this->_user
            ];

            $contents = $this->_content->get_content_by_download($search);
        }


        $data = [
            'title' => 'Download',
            'data' => $contents,
            'brands' => $brands,
            'categories' => $categories,
            'access_control' => $this->_user['access_control'],
            'control_url' => $control_url,
            'used_brand_id' => is_null($used_brand_id) ? [] : $used_brand_id,
            'used_category_id' => is_null($used_category_id) ? [] : $used_category_id,
            'download_search' => $download_search,
            'link_search' => $link_search,
            'user' => $this->_user,
        ];

        $this->Actionlog('Click',['action'=>'Download.Index']);
        return view('download', $data);
    }

    public function action_search()
    {
        $params = $this->_request->post();

        $used_brand_id = json_decode($params['brand_id'],true);
        $used_category_id = json_decode($params['category_id'],true);
        $download_search = $params['download_search'];

        $this->_user = $this->_request->session()->get('user');
        //分頁搜尋用
        $link_search = [
            'brand_id' => $params['brand_id'],
            'category_id' => $params['category_id'],
            'download_search' => $params['download_search'],
        ];

        $search = [
            'brand_id' => $used_brand_id,
            'category_id' => $used_category_id,
            'download_search' => $download_search,
            'user' => $this->_user
        ];

        $control_url = $this->_controlUrl();

        $brands = $this->_brand->get_public_brands($this->_user);
        $categories = $this->_category->get_category();

        //之後搜尋要改方法
        $contents = $this->_content->get_content_by_download($search);

        $data = [
            'title' => 'Download',
            'data' => $contents,
            'brands' => $brands,
            'categories' => $categories,
            'access_control' => $this->_user['access_control'],
            'control_url' => $control_url,
            'used_brand_id' => $used_brand_id,
            'used_category_id' => $used_category_id,
            'download_search' => $download_search,
            'link_search' => $link_search,
            'user' => $this->_user,
        ];

        $this->Actionlog('Search',['action'=>'Download.Search', 'Params'=>['brand'=>$params['brand_id'],'category'=> $params['category_id'],'search_TXT'=>$params['download_search']] ]);
        return view('download',$data);
    }

    public function single_file_download($download_id)
    {

        $content = $this->_content->get_download_file($download_id,'single');
        $path = public_path('storage');

        $this->Actionlog('Download',['action'=>'Download.Single_Download',"Download_files"=>$download_id]);
        $file_path = explode('/',$content['content_upload_path']);

        return response()->download($path.'/'.$file_path[2]);
    }

    public function temp_download($token)
    {
        $result = $this->_content->check_download_token($token);
        if($result)
        {
            $path = public_path('storage');

            $this->Actionlog('Download',['action'=>'Download.Temp_Download',"Download_files"=>$result['content_id']]);
            $file_path = explode('/',$result['content_upload_path']);

            return response()->download($path.'/'.$file_path[2]);
        }

        return redirect(route('index.login'))->with('message','Download Path Expired.');
    }

    public function file_download()
    {
        $params = $this->_request->post();
        $downloads = json_decode($params['download_id'],true);
        $contents = $this->_content->get_download_file($downloads,'multi');

        $path = public_path('storage');
        $zips = new \Madnest\Madzipper\Madzipper();
        $zip_path = Storage::path('total_download.zip');

        if(Storage::exists('total_download.zip'))
        {
            Storage::delete('total_download.zip');
        }

        $zips->make($zip_path);

        foreach ($contents as $content)
        {
                $file_path = explode('/',$content['content_upload_path']);
                $zips->add($path.'/'.$file_path[2]);
                $logs[]  = $content['id'];
        }

        $zips->close();

        $this->Actionlog('Download',['action'=>'Download.Download',"Download_files"=>$logs]);
        return response()->download($zip_path);
    }

    private function _controlUrl()
    {
        $this->_user = $this->_request->session()->get('user');

        if(in_array('ALL',$this->_user['member_group']['allow_manager']))
        {
            $control_url = '/content/index';
        }
        else
        {
            if($this->_user['access_control'])
            {
                $role = [
                    'CTM' => '/content/index',
                    'CAM' => '/category/index',
                    'FM' => '/field/index',
                    'MM' => '/member/index',
                    'SL' => '/systemlog/index',
                    'SS' => '/systemsetting/index'
                ];

                $control_url = $role[$this->_user['member_group']['allow_manager'][0]];
            }
            else
            {
                $control_url = '';
            }
        }

        return $control_url;
    }
}
