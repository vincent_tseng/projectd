<?php
namespace App\Http\Controllers;

use App\Services\CategoryService;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function __construct(
        CategoryService $categoryService,
        Request $request
    )
    {
        $this->_ser_category = $categoryService;
        $this->_request = $request;
    }

    public function index()
    {
        $category = $this->_ser_category->get_category();

        $data = [
            'category' => $category,
            'title' => 'Category'
        ];
        $this->Actionlog('Click',['action'=>'Category.Index']);
        return view('Category.index', $data);
    }

    public function insert_category()
    {
        $fields = $this->_ser_category->get_fields();

        $data = [
            'fields' => $fields,
            'title' => 'Category'
        ] ;
        $this->Actionlog('Click',['action'=>'Category.Insert']);
        return view('Category.new', $data);
    }

    public function action_insert_category()
    {
        $params = $this->_request->post();

        $result = $this->_ser_category->insert_category($params);

        if($result)
        {
            if($params['renew'] == 0)
            {
                $this->Actionlog('Insert',['action'=>'Category.Insert', 'result'=>'success', 'redirect'=>'No', 'Params'=>$params]);
                return redirect(route('category.index'))->with('message',['msg'=> 'New Category Added.', 'type'=>'success']);
            }
            else
            {
                $this->Actionlog('Insert',['action'=>'Category.Insert', 'result'=>'success', 'redirect'=>'category.new', 'Params'=>$params]);
                return redirect(route('category.new'))->with('message',['msg'=> 'New Category Added. Please Add New One.', 'type'=>'success']);
            }
        }
        else
        {
            $this->Actionlog('Insert',['action'=>'Category.Insert', 'result'=>'False', 'Params'=>$params]);
            return redirect(route('category.index'))->with('message',['msg'=> 'New Category Added False.', 'type'=>'error']);
        }
    }

    public function update_category($idx)
    {
        $category = $this->_ser_category->get_category($idx);
        $fields = $this->_ser_category->get_fields();
        $data = [
            'category' => $category,
            'fields' => $fields,
            'title' => 'Category'
        ];
        $this->Actionlog('Click',['action'=>'Category.Update']);
        return view('Category.edit', $data);
    }

    public function action_update_category()
    {
        $params = $this->_request->post();

        $result = $this->_ser_category->update_category($params);

        if($result)
        {
            if($params['renew'] == 0)
            {
                $this->Actionlog('Update',['action'=>'Category.Update', 'result'=>'success', 'redirect'=>'No', 'Params'=>$params]);
                return redirect(route('category.index'))->with('message', ['msg'=> 'Category Edited.', 'type'=>'success']);
            }
            else
            {
                $this->Actionlog('Update',['action'=>'Category.Update', 'result'=>'success', 'redirect'=>'category.new', 'Params'=>$params]);
                return redirect(route('category.new'))->with('message',['msg'=> 'Category Edited. Please Add New One.', 'type'=>'success']);
            }
        }
        else
        {
            $this->Actionlog('Update',['action'=>'Category.Update', 'result'=>'False', 'Params'=>$params]);
            return redirect(route('category.index'))->with('message',['msg'=> 'Category Edited False.', 'type'=>'error']);
        }
    }

    public function action_category_order()
    {
        $orders = $this->_request->post();
        $arr_orders = json_decode($orders['orders'],True);

        $result = $this->_ser_category->update_category_order($arr_orders);
        if($result)
        {
            $this->Actionlog('Update',['action'=>'Category.Orderby', 'result'=>'success']);
            echo 'update order success';
        }
        else
        {
            $this->Actionlog('Update',['action'=>'Category.Orderby', 'result'=>'False']);
            echo 'update order fail';
        }
    }

    public function check_category()
    {
        $params = $this->_request->post();
        $result = $this->_ser_category->check_category_name($params['category_name']);

        if($result == 'existed')
        {
            echo 'Category Name Already Exists.';
        }
        else
        {
            echo 'Category Name Can Be Used.';
        }
    }

    public function action_delete()
    {
        $params = $this->_request->post();

        $id = $params['category_id'];

        $result = $this->_ser_category->delete($id);

        if($result)
        {
            $this->Actionlog('Delete',['action'=>'Category.Delete', 'result'=>'success', 'Params'=>$params]);
            return redirect(route('category.index'))->with('message',['msg'=> 'Category Delete Success.' , 'type'=>'success']);
        }
        else
        {
            $this->Actionlog('Delete',['action'=>'Category.Delete', 'result'=>'False', 'Params'=>$params]);
            return redirect(route('category.index'))->with('message',['msg'=> 'Category Delete False.' , 'type'=>'error']);
        }
    }
}
