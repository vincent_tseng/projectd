<?php
namespace App\Http\Controllers;

use App\Services\BrandService;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    public function __construct(
        Request $request,
        BrandService $brandService
    )
    {
        $this->request = $request;
        $this->ser_brand = $brandService;
    }

    //
    public function index()
    {
        $brands = $this->ser_brand->get_brands();

        $data = [
            'brands' => $brands,
            'title' => 'Brand'
        ];

        $this->Actionlog('Click',['action'=>'Brand.Index']);

        return view('Brand.index',$data);
    }

    public function insert_brand()
    {
        $this->Actionlog('Click',['action'=>'Brand.Insert']);
        $data = [
            'title' => 'Brand'
        ];
        return view('Brand.new', $data);
    }

    public function action_insert_brand()
    {
        $params = $this->request->post();

        $result = $this->ser_brand->new_brand($params);

        if($result)
        {
            if($params['renew'] == 0)
            {
                $this->Actionlog('Insert',['action'=>'Brand.Insert', 'result'=>'success', 'redirect'=>'No', 'Params'=>$params]);
                return redirect(route('brand.index'))->with('message',['msg'=> 'New Brand Added.', 'type'=>'success']);
            }
            else
            {
                $this->Actionlog('Insert',['action'=>'Brand.Insert', 'result'=>'success', 'redirect'=>'Brand.new', 'Params'=>$params]);
                return redirect(route('brand.new'))->with('message',['msg'=> 'New Brand Added. Please Add New One.', 'type'=>'success']);
            }
        }
        else
        {
            $this->Actionlog('Insert',['action'=>'Brand.Insert', 'result'=>'False', 'Params'=>$params]);
            return redirect(route('brand.index'))->with('message',['msg'=> 'New Brand Added False.', 'type'=>'error']);
        }
    }

    public function update_brand($idx)
    {
        $brand = $this->ser_brand->get_brands($idx);
        $data = [
            'brand' => $brand,
            'title' => 'Brand'
        ];

        $this->Actionlog('Click',['action'=>'Brand.Update']);
        return view('Brand.edit',$data);
    }

    public function action_update_brand()
    {
        $params = $this->request->post();

        $result = $this->ser_brand->update_brand($params);

        if($result)
        {
            if($params['renew'] == 0)
            {
                $this->Actionlog('Update',['action'=>'Brand.Update', 'result'=>'success', 'redirect'=>'No', 'Params'=>$params]);
                return redirect(route('brand.index'))->with('message',['msg'=> 'Brand Edited.', 'type'=>'success']);
            }
            else
            {
                $this->Actionlog('Update',['action'=>'Brand.Update', 'result'=>'success', 'redirect'=>'Brand.New', 'Params'=>$params]);
                return redirect(route('brand.new'))->with('message',['msg'=> 'Brand Edited. Please Add New One', 'type'=>'success']);
            }
        }
        else
        {
            $this->Actionlog('Update',['action'=>'Brand.Update', 'result'=>'False', 'redirect'=>'No', 'Params'=>$params]);
            return redirect(route('brand.index'))->with('message',['msg'=> 'Brand Edited False.', 'type'=>'error']);
        }
    }

    public function action_order_brand()
    {
        $orders = $this->request->post();
        $arr_orders = json_decode($orders['orders'],True);

        $result = $this->ser_brand->update_brand_order($arr_orders);
        if($result)
        {
            $this->Actionlog('Update',['action'=>'Brand.Orderby', 'result'=>'success', 'redirect'=>'No']);
            echo 'update order success';
        }
        else
        {
            $this->Actionlog('Update',['action'=>'Brand.Orderby', 'result'=>'False', 'redirect'=>'No']);
            echo 'update order fail';
        }
    }

    public function check_brand_name()
    {
        $params = $this->request->post();
        $result = $this->ser_brand->check_brand($params['brand_name']);

        if($result == 'existed')
        {
            echo 'Brand Name Already Exists.';
        }
        else
        {
            echo 'Brand Name Can Be Used.';
        }
    }

    public function action_delete()
    {
        $params = $this->request->post();

        $id = $params['brand_id'];

        $result = $this->ser_brand->delete($id);

        if($result)
        {
            $this->Actionlog('Delete',['action'=>'Brand.Delete', 'result'=>'success', 'Params'=>$params]);
            return redirect(route('brand.index'))->with('message',['msg'=> 'Brand Delete Success.', 'type'=>'success']);
        }
        else
        {
            $this->Actionlog('Delete',['action'=>'Brand.Delete', 'result'=>'False', 'Params'=>$params]);
            return redirect(route('brand.index'))->with('message',['msg'=> 'Brand Delete False.' , 'type'=>'error']);
        }
    }
}
