<?php

namespace App\Http\Controllers;

use App\Services\FieldService;
use Illuminate\Http\Request;

class FieldsController extends Controller
{
    public function __construct(
        Request $request,
        FieldService $fieldService
    )
    {
        $this->request = $request;
        $this->_ser_field = $fieldService;
    }

    public function index()
    {
        $fields = $this->_ser_field->get_field();

        $data = [
            'fields' => $fields,
            'title' => 'Fields'
        ];

        $this->Actionlog('Click',['action'=>'Fields.Index']);
        return view('Fields.index', $data);
    }

    public function insert_fields()
    {
        $this->Actionlog('Click',['action'=>'Fields.Insert']);
        return view('Fields.new',['title' => 'Fields']);
    }

    public function action_insert_fields()
    {
        $params = $this->request->post();

        $result = $this->_ser_field->insert_field($params);

        if($result)
        {
            if($params['renew'] == 0)
            {
                $this->Actionlog('Insert',['action'=>'Fields.Insert', 'result'=>'Success', 'redirect'=>'No', 'Params'=>$params]);
                return redirect(route('field.index'))->with('message','New Field Added.');
            }
            else
            {
                $this->Actionlog('Insert',['action'=>'Fields.Insert', 'result'=>'Success', 'redirect'=>'Field.New', 'Params'=>$params]);
                return redirect(route('field.new'))->with('message','New Field Added. Please Add New One.');
            }
        }
        else
        {
            $this->Actionlog('Insert',['action'=>'Fields.Insert', 'result'=>'Success', 'redirect'=>'No', 'Params'=>$params]);
            return redirect(route('field.index'))->with('message','New Field Added False.');
        }
    }

    public function update_fields($idx)
    {
        $field = $this->_ser_field->get_field($idx);

        $data = [
            'field' => $field,
            'title' => 'Fields'
        ];
        $this->Actionlog('Click',['action'=>'Fields.Update']);
        return view('Fields.edit', $data);
    }

    public function action_update_fields()
    {
        $params = $this->request->post();

        $result = $this->_ser_field->update_field($params);

        if($result)
        {
            if($params['renew'] == 0)
            {
                $this->Actionlog('Update',['action'=>'Fields.Update', 'result'=>'Success', 'redirect'=>'No', 'Params'=>$params]);
                return redirect(route('field.index'))->with('message','Field Edited.');
            }
            else
            {
                $this->Actionlog('Update',['action'=>'Fields.Update', 'result'=>'Success', 'redirect'=>'Fields.New', 'Params'=>$params]);
                return redirect(route('field.new'))->with('message','Field Edited. Please Add New One.');
            }
        }
        else
        {
            $this->Actionlog('Update',['action'=>'Fields.Update', 'result'=>'False', 'redirect'=>'No', 'Params'=>$params]);
            return redirect(route('field.index'))->with('message','Member Edited False.');
        }
    }

    public function action_fields_order()
    {
        $orders = $this->request->post();
        $arr_orders = json_decode($orders['orders'],True);

        $result = $this->_ser_field->update_field_order($arr_orders);
        if($result)
        {
            $this->Actionlog('Update',['action'=>'Fields.Orderby', 'result'=>'Success']);
            echo 'update order success';
        }
        else
        {
            $this->Actionlog('Update',['action'=>'Fields.Orderby', 'result'=>'False']);
            echo 'update order fail';
        }
    }

    public function check_field_name()
    {
        $params = $this->request->post();
        $result = $this->_ser_field->check_field($params['field_name']);

        if($result == 'existed')
        {
            echo 'Field Name Already Exists.';
        }
        else
        {
            echo 'Field Name Can Be Used.';
        }
    }

    public function action_delete()
    {
        $params = $this->request->post();

        $id = $params['field_id'];

        $result = $this->_ser_field->delete($id);

        if($result)
        {
            $this->Actionlog('Delete',['action'=>'Field.Delete', 'result'=>'success', 'Params'=>$params]);
            return redirect(route('field.index'))->with('message','Field Delete Success.');
        }
        else
        {
            $this->Actionlog('Delete',['action'=>'Field.Delete', 'result'=>'False', 'Params'=>$params]);
            return redirect(route('field.index'))->with('message','Field Delete False.');
        }
    }
}
