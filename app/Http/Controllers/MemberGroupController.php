<?php
namespace App\Http\Controllers;

use App\Services\BrandService;
use App\Services\MemberService;
use Illuminate\Http\Request;

class MemberGroupController extends Controller
{
    private $_ser_member;
    private $_ser_brand;
    private $_request;

    public function __construct(
        MemberService $memberService,
        BrandService $brandService,
        Request $request
    )
    {
        $this->_ser_member = $memberService;
        $this->_ser_brand = $brandService;
        $this->_request = $request;
    }

    public function index()
    {
        $memberGroups = $this->_ser_member->get_memberGroups();

        $data = [
            'memberGroups' => $memberGroups,
            'title' => 'Member Groups'
        ];
        $this->Actionlog('Click',['action'=>'MembersGroups.Index']);

        return view('Members_group.index', $data);
    }

    public function insert_member_group()
    {
        $user = $this->_request->session()->get('user');
        $brands =$this->_ser_brand->get_public_brands($user);
        $allow_managers = $this->get_allow_managers();
        $allow_action = $this->get_allow_actions();

        $data = [
            'brands' => $brands,
            'allow_managers' => $allow_managers,
            'allow_actions' => $allow_action,
            'title' => 'Member Groups'
        ];
        $this->Actionlog('Click',['action'=>'MembersGroups.Insert']);
        return view('Members_group.new', $data);
    }

    public function action_insert_member_group()
    {
        $params =$this->_request->post();

        $result = $this->_ser_member->insert_membergroup($params);

        if($result)
        {
            if($params['renew'] == 0)
            {
                $this->Actionlog('Insert',['action'=>'MembersGroups.Insert', 'result'=>'Success', 'redirect'=>'No', 'Params'=>$params]);
                return redirect(route('member_group.index'))->with('message',['msg'=> 'New Member Group Added.' , 'type'=>'success']);
            }
            else
            {
                $this->Actionlog('Insert',['action'=>'MembersGroups.Insert', 'result'=>'Success', 'redirect'=>'MembersGroups.New', 'Params'=>$params]);
                return redirect(route('member_group.new'))->with('message',['msg'=> 'New Member Group Added. Please Add New One.' , 'type'=>'success']);
            }
        }
        else
        {
            $this->Actionlog('Insert',['action'=>'MembersGroups.Insert', 'result'=>'False', 'redirect'=>'No', 'Params'=>$params]);
            return redirect(route('member_group.index'))->with('message',['msg'=> 'Member Group Added False.' , 'type'=>'error']);
        }
    }

    public function update_member_group($idx)
    {
        $user = $this->_request->session()->get('user');
        $memberGroup = $this->_ser_member->get_memberGroups($idx);
        $brands =$this->_ser_brand->get_public_brands($user);
        $allow_managers = $this->get_allow_managers();
        $allow_action = $this->get_allow_actions();

        $data = [
            'memberGroup' => $memberGroup,
            'brands' => $brands,
            'allow_managers' => $allow_managers,
            'allow_actions' => $allow_action,
            'title' => 'Member Groups'
        ];

        $this->Actionlog('Click',['action'=>'MembersGroups.Update']);
        return view('Members_group.edit', $data);
    }

    public function action_update_member_group()
    {
        $params =$this->_request->post();

        $result = $this->_ser_member->update_memberGroup($params);

        if($result)
        {
            if($params['renew'] == 0)
            {
                $this->Actionlog('Update',['action'=>'MembersGroups.Update', 'result'=>'Success', 'redirect'=>'No', 'Params'=>$params]);
                return redirect(route('member_group.index'))->with('message',['msg'=> 'Member Group Edited.' , 'type'=>'success']);
            }
            else
            {
                $this->Actionlog('Update',['action'=>'MembersGroups.Update', 'result'=>'Success', 'redirect'=>'MembersGroups.New', 'Params'=>$params]);
                return redirect(route('member_group.new'))->with('message',['msg'=> 'Member Group Edited. Please Add New One.' , 'type'=>'success']);
            }
        }
        else
        {
            $this->Actionlog('Update',['action'=>'MembersGroups.Update', 'result'=>'False', 'redirect'=>'No', 'Params'=>$params]);
            return redirect(route('member_group.index'))->with('message',['msg'=> 'Member Group Added False.' , 'type'=>'error']);
        }
    }

    public function check_group_name()
    {
        $params = $this->_request->post();
        $result = $this->_ser_member->check_group_name($params['member_group_name']);

        if($result == 'existed')
        {
            echo 'Group Name Already Exists.';
        }
        else
        {
            echo 'Group Name Can Be Used.';
        }
    }

    public function action_delete()
    {
        $params = $this->_request->post();

        $id = $params['membergroup_id'];

        $result = $this->_ser_member->delete($id,'rule');

        if($result)
        {
            $this->Actionlog('Delete',['action'=>'Member_rule.Delete', 'result'=>'success', 'Params'=>$params]);
            return redirect(route('member_group.index'))->with('message',['msg'=> 'message','Member_rule Delete Success.' , 'type'=>'success']);
        }
        else
        {
            $this->Actionlog('Delete',['action'=>'Member_rule.Delete', 'result'=>'False', 'Params'=>$params]);
            return redirect(route('member_group.index'))->with('message', ['msg'=> 'Member_rule Delete False.' , 'type'=>'error']);
        }
    }

    private function get_allow_managers()
    {
        $temp_allow_managers = ['ALL,ALL', 'Content Manager,CTM', 'Brand Manager,BM', 'Category Manager,CAM', 'Member Manager,MM', 'System Setting,SS', 'System Logs,SL'];
        foreach($temp_allow_managers as $rows)
        {
            $options = explode(',',$rows);
            $allow_managers[] = [
                'name' => $options[0],
                'values' => $options[1]
            ];
        }

        return $allow_managers;
    }

    private function get_allow_actions()
    {
        $temp_allow_actions = ['Create Content', 'Edit Own Contents', 'Delete Own Contents', 'Edit Contents. By Other', 'Delete Contents. By Others'];
        $values = 1;
        foreach($temp_allow_actions as $rows )
        {
            $allow_actions[] = [
                'name' => $rows,
                'values' => $values
            ];
            $values++;
        }

        return $allow_actions;
    }
}

