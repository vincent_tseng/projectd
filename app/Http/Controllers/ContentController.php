<?php

namespace App\Http\Controllers;

use App\Services\ContentService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ContentController extends Controller
{
    private $_ser_content;
    private $_request;

    public function __construct(
        ContentService $contentService,
        Request $request
    )
    {
        $this->_ser_content = $contentService;
        $this->_request = $request;
    }

    public function index()
    {

        $user = $this->_request->session()->get('user');
        $brands = $this->_ser_content->get_brand($user);
        $categorys = $this->_ser_content->get_category();

        $used_brand_id =  $this->_request->get('s_brand');
        $used_category_id =  $this->_request->get('s_category');
        $used_status =  $this->_request->get('s_status');
        $used_txt_search =  $this->_request->get('txt_search');

        $user = $this->_request->session()->get('user');

        //分頁用
        $link_search = [
            's_brand' => $used_brand_id,
            's_category' => $used_category_id,
            's_status' => $used_status,
            'txt_search' => $used_txt_search,
        ];

        if( (is_null($used_brand_id)) && (is_null($used_category_id)) && (is_null($used_status)) && (is_null($used_txt_search)))
        {
            $contents = $this->_ser_content->get_content();
        }
        else
        {
            $contents = $this->_ser_content->get_content_by_search($link_search);
        }



        $data = [
            'contents' => $contents,
            'brands' => $brands,
            'categorys' => $categorys,
            'title' => 'Content',
            'used_brand_id' => $used_brand_id,
            'used_category_id' => $used_category_id,
            'used_status' => $used_status,
            'used_txt_search' => $used_txt_search,
            'link_search' => $link_search,
            'user' => $user,
        ];

        $this->Actionlog('Click',['action'=>'Contents.Index']);
        return view('Contents.index', $data);
    }

    public function content_search()
    {
        $user = $this->_request->session()->get('user');
        $params = $this->_request->post();
        $brands = $this->_ser_content->get_brand($user);
        $categorys = $this->_ser_content->get_category();

        $used_brand_id = $params['s_brand'];
        $used_category_id = $params['s_category'];
        $used_status = $params['s_status'];
        $used_txt_search = $params['txt_search'];

        $contents = $this->_ser_content->get_content_by_search($params);

        $user = $this->_request->session()->get('user');

        //分頁用
        $link_search = [
            's_brand' => $params['s_brand'],
            's_category' => $params['s_category'],
            's_status' => $params['s_status'],
            'txt_search' => $params['txt_search'],
        ];

        $data = [
            'contents' => $contents,
            'brands' => $brands,
            'categorys' => $categorys,
            'used_brand_id' => $used_brand_id,
            'used_category_id' => $used_category_id,
            'used_status' => $used_status,
            'used_txt_search' => $used_txt_search,
            'title' => 'Content',
            'link_search' => $link_search,
            'user' => $user,
        ];

        $this->Actionlog('Search',['action'=>'Contents.Search','Params' => $params]);

        return view('Contents.index', $data);
    }

    public function insert_content()
    {
        $user = $this->_request->session()->get('user');

        $brands = $this->_ser_content->get_brand($user);
        $categorys = $this->_ser_content->get_category();

        $data = [
            'brands' => $brands,
            'categorys' => $categorys,
            'title' => 'New Content'
        ];
        $this->Actionlog('Click',['action'=>'Contents.Insert']);
        return view('Contents.new', $data);
    }

    public function action_insert_content()
    {
        $params = $this->_request->post();
        $user = $this->_request->session()->get('user');

        $zip_file = $this->_request->file('upload_data');
        $file_path = $zip_file->store('public');

        $params['content_upload_path'] = Storage::url($file_path);
        $params['created_by'] = $user['id'];

        //sample pic
        $validated = $this->_request->validate(
            [
                'sample_pic' => 'mimes:jpeg,jpg,png'
            ]
        );

        if($this->_request->hasFile('sample_pic'))
        {
            $sample_pic = $this->_request->file('sample_pic');
            $pic_path = $sample_pic->store('pics');

            $params['content_sample_pic'] = '/' . $pic_path;
        }

        $result = $this->_ser_content->insert_content($params);

        if($result)
        {

            if($params['renew'] == 0)
            {
                $this->Actionlog('Insert',['action'=>'Contents.Insert', 'result'=>'success', 'redirect'=>'No', 'Params'=>$params]);
                return redirect(route('Contents.index'))->with('message',['msg'=> 'New Content Added.', 'type'=>'success']);

            }
            else
            {
                $this->Actionlog('Insert',['action'=>'Contents.Insert', 'result'=>'success', 'redirect'=>'Contents.New', 'Params'=>$params]);
                return redirect(route('Contents.new'))->with('message',['msg'=> 'New Content Added. Please Add New One.', 'type'=>'success']);

            }
        }
        else
        {
            $this->Actionlog('Insert',['action'=>'Contents.Insert', 'result'=>'False', 'redirect'=>'No', 'Params'=>$params]);
            return redirect(route('Contents.index'))->with('message',['msg'=> 'New Content Added False.', 'type'=>'error']);
        }

    }

    public function update_content($idx)
    {
        $user = $this->_request->session()->get('user');

        $content = $this->_ser_content->get_content($idx);
        $brands = $this->_ser_content->get_brand($user);
        $categorys = $this->_ser_content->get_category();

        $data = [
            'content' => $content,
            'brands' => $brands,
            'categorys' => $categorys,
            'title' => 'Content Edit'
        ];
        $this->Actionlog('Click',['action'=>'Contents.Update']);
        return view('Contents.edit', $data);
    }

    public function action_update_content()
    {
        $params = $this->_request->post();

        if($this->_request->hasFile('upload_data'))
        {
            $zip_file = $this->_request->file('upload_data');
            $file_path = $zip_file->store('public');

            $params['content_upload_path'] = Storage::url($file_path);
        }

        if($this->_request->hasFile('sample_pic'))
        {
            //sample pic
            $validated = $this->_request->validate(
                [
                    'sample_pic' => 'mimes:jpeg,jpg,png'
                ]
            );

            $sample_pic = $this->_request->file('sample_pic');
            $pic_path = $sample_pic->store('pics');

            $params['content_sample_pic'] = '/'.$pic_path;
        }


        $result = $this->_ser_content->update_content($params);

        if($result)
        {
            if($params['renew'] == 0)
            {
                $this->Actionlog('Update',['action'=>'Contents.Update', 'result'=>'success', 'redirect'=>'No', 'Params'=>$params]);
                return redirect(route('Contents.index'))->with('message',['msg'=> 'Content Edited.', 'type'=>'success']);

            }
            else
            {
                $this->Actionlog('Update',['action'=>'Contents.Update', 'result'=>'success', 'redirect'=>'Contents.New', 'Params'=>$params]);
                return redirect(route('Contents.new'))->with('message',['msg'=> 'Content Edited. Please Add New One.', 'type'=>'success']);
            }
        }
        else
        {
            $this->Actionlog('Update',['action'=>'Contents.Update', 'result'=>'False', 'redirect'=>'No', 'Params'=>$params]);
            return redirect(route('Contents.index'))->with('message',['msg'=> 'Content Edited False.', 'type'=>'error']);

        }
    }

    public function check_title()
    {
        $params = $this->_request->post();
        $result = $this->_ser_content->check_title($params['content_title']);

        if($result == 'existed')
        {
            echo 'Title Already Exists.';
        }
        else
        {
            echo 'Title Can Be Used.';
        }
    }

    public function action_delete()
    {
        $params = $this->_request->post();

        $id = $params['content_id'];

        $result = $this->_ser_content->delete($id);

        if($result)
        {
            $this->Actionlog('Delete',['action'=>'Content.Delete', 'result'=>'success', 'Params'=>$params]);
            return redirect(route('Contents.index'))->with('message',['msg'=> 'Content Delete Success.', 'type'=>'success']);
        }
        else
        {
            $this->Actionlog('Delete',['action'=>'Content.Delete', 'result'=>'False', 'Params'=>$params]);
            return redirect(route('Contents.index'))->with('message',['msg'=> 'Content Delete False.', 'type'=>'error']);
        }
    }

    public function action_pic_delete()
    {
        $params = $this->_request->post();

        $id = $params['content_id'];

        $result = $this->_ser_content->delete_pic($id);

        if($result)
        {
            $this->Actionlog('Delete',['action'=>'Content.Delete', 'result'=>'success', 'Params'=>$params]);
            echo 'delete success';
        }
        else
        {
            $this->Actionlog('Delete',['action'=>'Content.Delete', 'result'=>'False', 'Params'=>$params]);
            echo 'delete false';
        }
    }

    //取得一次性下載連結
    public function action_get_temp_download_path()
    {
        $param = $this->_request->post();

        $result = $this->_ser_content->get_download_path($param);

        return redirect()->action(
            [ContentController::class, 'update_content'], ['idx' => $param['content_id']]
        )->with('message','Temp Download Link Created.');
    }

}
