<?php
namespace App\Http\Controllers;

use App\Services\MemberService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class MemberController extends Controller
{
    protected $_request;
    protected $_ser_member;

    public function __construct(
        Request $request,
        MemberService $memberService
    )
    {
        $this->_request = $request;
        $this->_ser_member = $memberService;
    }

    public function index()
    {
        $memberGroups = $this->_ser_member->get_public_memberGroup();

        $member_groups = $this->_request->get('member_groups');
        $member_status = $this->_request->get('member_status');
        $search_txt = $this->_request->get('search_txt');

        $link_search = [
            'member_groups' => $member_groups,
            'member_status' => $member_status,
            'search_txt' => $search_txt,
        ];

        if((is_null($member_groups)) && (is_null($member_status)) && (is_null($search_txt)))
        {
            $members = $this->_ser_member->get_members();
        }
        else
        {
            $members = $this->_ser_member->get_member_by_search($link_search);
        }

        $data = [
            'memberGroups' => $memberGroups,
            'members' => $members,
            'used_member_group' => $member_groups,
            'used_member_status' => $member_status,
            'used_member_search_txt' => $search_txt,
            'link_search' => $link_search,
            'title' => 'Members'
        ];

        $this->Actionlog('Click',['action'=>'Members.Index']);
        return view('Members.index', $data);
    }

    public function member_search()
    {
        $params = $this->_request->post();

        $used_member_group = $params['member_groups'];
        $used_member_status = $params['member_status'];
        $used_member_search_txt = $params['search_txt'];

        $memberGroups = $this->_ser_member->get_public_memberGroup();

        $members = $this->_ser_member->get_member_by_search($params);

        $link_search = [
            'member_groups' => $params['member_groups'],
            'member_status' => $params['member_status'],
            'search_txt' => $params['search_txt'],
        ];

        $data = [
            'memberGroups' => $memberGroups,
            'members' => $members,
            'used_member_group' => $used_member_group,
            'used_member_status' => $used_member_status,
            'used_member_search_txt' => $used_member_search_txt,
            'link_search' => $link_search,
            'title' => 'Members'
        ];

        $this->Actionlog('Search',['action'=>'Members.Search','Params' => $params]);
        return view('Members.index', $data);
    }

    public function insert_member()
    {
        $memberGroups = $this->_ser_member->get_public_memberGroup();

        $data = [
            'memberGroups' => $memberGroups,
            'title' => 'Members'
        ];
        $this->Actionlog('Click',['action'=>'Members.Insert']);
        return view('Members.new', $data);
    }

    public function action_insert_member()
    {
        $params = $this->_request->post();

        $result = $this->_ser_member->insert_member($params);

        if($result)
        {
            if($params['renew'] == 0)
            {
                $this->Actionlog('Insert',['action'=>'Members.Insert', 'result'=>'Success', 'redirect'=>'No', 'Params'=>$params]);
                return redirect(route('members.index'))->with('message', ['msg'=> 'New Member Added.' , 'type'=>'success']);
            }
            else
            {
                $this->Actionlog('Insert',['action'=>'Members.Insert', 'result'=>'Success', 'redirect'=>'Members.New', 'Params'=>$params]);
                return redirect(route('members.new'))->with('message',['msg'=> 'New Member Added. Please Add New One.' , 'type'=>'success']);
            }
        }
        else
        {
            $this->Actionlog('Insert',['action'=>'Members.Insert', 'result'=>'False', 'redirect'=>'No', 'Params'=>$params]);
            return redirect(route('members.index'))->with('message',['msg'=> 'New Member Added False.' , 'type'=>'error']);
        }
    }

    public function update_member($idx)
    {
        $memberGroups = $this->_ser_member->get_public_memberGroup();
        $member = $this->_ser_member->get_members($idx);
        $newaccount = $this->_ser_member->get_memberGroup_by_newAccount();

        if(in_array('ALL',Session::get('user')['member_group']['allow_manager']))
        {
            $admincheck = 1 ;
        }
        else
        {
            $admincheck = 0;
        }
        $data = [
            'memberGroups' => $memberGroups,
            'member' => $member,
            'newaccount' => $newaccount,
            'admincheck' => $admincheck,
            'title' => 'Members'
        ];

        $this->Actionlog('Click',['action'=>'Members.Update']);

        return view('Members.edit', $data);
    }

    public function action_update_member()
    {
        $params = $this->_request->post();

        $result = $this->_ser_member->update_member($params);

        if($result)
        {
            if($params['renew'] == 0)
            {
                $this->Actionlog('Update',['action'=>'Members.Update', 'result'=>'Success', 'redirect'=>'No', 'Params'=>$params]);
                return redirect(route('members.index'))->with('message',['msg'=> 'Member Edited.' , 'type'=>'success']);
            }
            else
            {
                $this->Actionlog('Update',['action'=>'Members.Update', 'result'=>'Success', 'redirect'=>'Members.New', 'Params'=>$params]);
                return redirect(route('members.new'))->with('message',['msg'=> 'Member Edited. Please Add New One.' , 'type'=>'success']);
            }
        }
        else
        {
            $this->Actionlog('Update',['action'=>'Members.Update', 'result'=>'False', 'redirect'=>'No', 'Params'=>$params]);
            return redirect(route('members.index'))->with('message',['msg'=> 'Member Edited False.' , 'type'=>'error']);
        }
    }

    public function action_member_check()
    {
        $params = $this->_request->post();
        $result = $this->_ser_member->check_member($params['member_email']);

        if($result == 'existed')
        {
            echo 'Member Email Already Exists.';
        }
        else
        {
            echo 'This Member Email Can Be Used.';
        }

//        echo $params['member_email'];
    }

    public function action_delete()
    {
        $params = $this->_request->post();

        $id = $params['member_id'];

        $result = $this->_ser_member->delete($id,'member');

        if($result)
        {
            $this->Actionlog('Delete',['action'=>'Member.Delete', 'result'=>'success', 'Params'=>$params]);
            return redirect(route('members.index'))->with('message',['msg'=> 'Member Delete Success.' , 'type'=>'success']);
        }
        else
        {
            $this->Actionlog('Delete',['action'=>'Member.Delete', 'result'=>'False', 'Params'=>$params]);
            return redirect(route('members.index'))->with('message',['msg'=> 'Member Delete False.' , 'type'=>'error']);
        }
    }

    public function action_restore()
    {
        $params = $this->_request->post();

        $id = $params['member_id'];

        $result = $this->_ser_member->restore($id);

        if($result)
        {
            $this->Actionlog('Restore',['action'=>'Member.Restore', 'result'=>'success', 'Params'=>$params]);
            return redirect(route('members.index'))->with('message', ['msg'=> 'Member Restore Success.' , 'type'=>'success']);
        }
        else
        {
            $this->Actionlog('Restore',['action'=>'Member.Restore', 'result'=>'False', 'Params'=>$params]);
            return redirect(route('members.index'))->with('message',['msg'=> 'Member Restore False.' , 'type'=>'error']);
        }

    }

}
