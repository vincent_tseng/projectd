<?php
namespace App\Http\Controllers;

use App\Services\SystemSettingService;
use Illuminate\Http\Request;

class SystemSettingController extends Controller
{
    public function __construct(
        SystemSettingService $systemSettingService,
        Request $request
    )
    {
        $this->_systemsetting = $systemSettingService;
        $this->_request = $request;
    }

    public function index()
    {
        $value = $this->_systemsetting->get_value();

        if(!isset($value['url_pi']))
        {
            $value['url_pi'] = '';
        }

        $data = [
            'value' => $value,
            'title' => 'SystemSetting'
        ];
        $this->Actionlog('Click',['action'=>'SystemSetting.Index']);
        return view('SystemSetting.index', $data);
    }

    public function action_update()
    {
        $options = $this->_request->post();

        $result = $this->_systemsetting->update_value($options);

        if($result)
        {
            $this->Actionlog('Update',['action'=>'SystemSetting.Setting', 'result'=>'Success']);
            return redirect(route('systemsetting.index'));
        }
        else
        {
            $this->Actionlog('Update',['action'=>'SystemSetting.Setting', 'result'=>'False']);
            return redirect(route('systemsetting.index'));
        }
    }
}
