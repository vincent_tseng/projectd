<?php

namespace App\Http\Controllers;

use App\Mail\AccountRequest;
use App\Mail\ForgetPasswd;
use App\Mail\NewAccount;
use App\Services\MemberService;
use App\Services\SystemSettingService;
use Illuminate\Http\Request;
use App\Mail\Loginver;
use Illuminate\Support\Facades\Mail;

class LoginController extends Controller
{
    public function __construct(
        Request $requests,
        MemberService  $memberService,
        SystemSettingService $systemSettingService
    )
    {
        $this->_request = $requests;
        $this->_ser_member = $memberService;
        $this->_ser_syssetting = $systemSettingService;
    }

    //
    public function index()
    {
        $value = $this->_ser_syssetting->get_value();

        $data = [
            'title' => 'Login',
            'value' => $value,
        ];
        return view('login', $data);
    }

    public function thanks(){
        $data = [
            'title' => 'Login',
            'message' => 'If the provided address is valid, you will receive a reset password email.'
        ];
        return view('thanks', $data);
    }

    public function action_login()
    {
        $systemsetting = $this->_ser_syssetting->get_value();

        $params = $this->_request->post();

        if(($params['email'] == 'root@123.123') || ($params['email'] == 'root@admin') )
        {
            $result = $this->sp_login($params);

            if($result == 'OK')
            {
                return redirect(route('download'));
            }

        }
        else
        {
            $token = $this->_ser_member->check_login($params['email'],$params['passwd']);

            if(($systemsetting['maintain'] == null) || ($token['member_group_id'] == 1))
            {
                if($token)
                {
                    if($token == 'deleted')
                    {
                        $this->Actionlog('Login',['action'=>'Login', 'result'=>'False', 'User' => $params['email'] ]);
                        return redirect(route('index.login'))->with('message','Account Is Deleted.');
                    }
                    else
                    {
                        if($token == 'Lfalse')
                        {
                            $this->Actionlog('Login',['action'=>'Login', 'result'=>'False', 'User' => $params['email'] ]);
                            return redirect(route('index.login'))->with('message','Member Group Is INACTIVE.');
                        }
                        else
                        {
                            //寄信
                            $value = $this->_ser_syssetting->get_value();

                            $mail_to = $params['email'];
                            Mail::to($mail_to)->send(new loginver($token['token'], $token['member_name'], $value['service_email']));
                            $this->Actionlog('Login',['action'=>'Login', 'result'=>'success', 'User' => $params['email'] ]);

                            return redirect(route('index.login'))->with('message','Sending Loging Url To Your Email Address.');
                        }
                    }
                }
                else
                {
                    $this->Actionlog('Login',['action'=>'Login', 'result'=>'False', 'User' => $params['email'] ]);
                    return redirect(route('index.login'))->with('message','Email or Password Error.');
                }
            }
            else
            {
                $this->Actionlog('Login',['action'=>'Login', 'result'=>'maintain', 'User' => '' ]);
                return redirect(route('index.login'))->with('message','System Now Maintaining.');
            }
        }
    }

    public function verify_token($token)
    {
//        dd($token);
        $result = $this->_ser_member->verify_login_token($token);
        switch($result)
        {
            case "OK":
                $this->Actionlog('Login',['action'=>'Verify_Token', 'result'=>'success', 'Token' => $token ]);
                return redirect(route('download'));
                break;
            case "FALSE":
                $this->Actionlog('Login',['action'=>'Verify_Token', 'result'=>'False', 'Token' => $token ]);
                return redirect(route('index.login'))->with('message','Eamil or Passwd Error.');
                break;
            case "EXPIRED":
                $this->Actionlog('Login',['action'=>'Verify_Token', 'result'=>'EXPIRED', 'Token' => $token ]);
                return redirect(route('index.login'))->with('message','Token EXPIRED.');
                break;
        }
    }

    public function logout()
    {
        $this->Actionlog('LogOut',['action'=>'LogOut', 'result'=>'Success']);
        $this->_request->session()->forget('user');
        return redirect(route('index.login'));
    }

    //忘記密碼處理
    public function forger_passwd()
    {
        $value = $this->_ser_syssetting->get_value();

        return view('forget',['title'=>'Forget Passwd','value' => $value]);
    }

    public function action_reset_passwd()
    {
        $params = $this->_request->post();

        $email = $params['email'];

        $token = $this->_ser_member->forget_passwd($email);

        if($token)
        {
            //寄信
            $value = $this->_ser_syssetting->get_value();

            $mail_to = $email;
            Mail::to($mail_to)->send(new ForgetPasswd($token['token'], $token['member_name'], $value['service_email']));
            $this->Actionlog('ForgetPasswd',['action'=>'ForgetPasswd', 'result'=>'success', 'User' => $email ]);
            return redirect(route('index.login'))->with('message','Sending Reset Url To Your Email Address.');
        }
        else
        {
            $this->Actionlog('ForgetPasswd',['action'=>'ForgetPasswd', 'result'=>'false', 'User' => '' ]);
            return redirect(route('index.login'))->with('message','Email Incorrect.');
        }

    }

    public function setting_passwd($token)
    {
        $result = $this->_ser_member->verify_login_token($token, 'Reset');
        $value = $this->_ser_syssetting->get_value();
        switch($result)
        {
            case "OK":
                $user = $this->_request->session()->get('user');
                $this->Actionlog('ResetPasswd',['action'=>'ResetPasswd', 'result'=>'success', 'Token' => $token ]);
                return view('setpasswd',['title' => 'Setting Passwd','member_id' => $user['id'], 'value'=>$value]);
                break;
            case "EXPIRED":
                $this->Actionlog('ResetPasswd',['action'=>'ResetPasswd', 'result'=>'EXPIRED', 'Token' => $token ]);
                return redirect(route('index.login'))->with('message','Token EXPIRED.');
                break;
            default:
                $this->Actionlog('ResetPasswd',['action'=>'ResetPasswd', 'result'=>'EXPIRED', 'Token' => $token ]);
                return redirect(route('index.login'))->with('message','Unknow Error.');
        }
    }

    public function action_setting_passwd()
    {
        $params = $this->_request->post();

        $result = $this->_ser_member->setting_passwd($params['member_id'], $params['passwd']);

        $datas = [
            'title' => 'Thanks',
            'message' => 'If the provided address is valid, you will receive a reset password email.'
        ];

        if($result)
        {
            $this->Actionlog('ResetPasswd',['action'=>'action_ResetPasswd', 'result'=>'success']);
            return view('thanks',$datas);
        }
    }


    //申請帳號需求
    public function account_request()
    {
        $value = $this->_ser_syssetting->get_value();

        $data = [
            'title' => 'Become Partner',
            'value' => $value
        ];
        return view('partner', $data);
    }

    public function action_member_check()
    {
        $params = $this->_request->post();
        $result = $this->_ser_member->check_member($params['member_email']);

        if($result == 'existed')
        {
            echo 'Member Email Already Exists.';
        }
        else
        {
            echo 'This Member Email Can Be Used.';
        }
    }

    public function action_request()
    {
        $params = $this->_request->post();
        $value = $this->_ser_syssetting->get_value();

        $result = $this->_ser_member->account_request($params);

        $datas = [
            'title' => 'Thanks',
            'message' => 'Please Waiting For Review.'
        ];

        if($result)
        {
            //寄信通知
            try {
                //For-Guest
                $mail_to_Guest = $params['member_email'];
                Mail::to($mail_to_Guest)->send(new NewAccount($params['member_name'], $value['service_email']));
                $this->Actionlog('AccountRequest',['action'=>'AccountRequest', 'result'=>'success']);

                //For-Amdin
                $mail_to = $value['service_email'];
                Mail::to($mail_to)->send(new AccountRequest());
                $this->Actionlog('AccountRequest',['action'=>'AccountRequest', 'result'=>'success']);

                return view('thanks',$datas);
            }
            catch (\Exception $e)
            {
                $this->Actionlog('AccountRequest',['action'=>'AccountRequest', 'result'=>'False']);
                return redirect(route('index.login'));
            }
        }
    }

    //免兩段驗證登入
    private function sp_login($params)
    {
        $result = $this->_ser_member->sp_login($params);
        return $result;
    }
}
