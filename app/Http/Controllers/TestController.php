<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\Loginver;

class TestController extends Controller
{
    public function index()
    {
        $data = [
            'title' => 'Login',
        ];
        return view('login', $data);
    }

    public function download()
    {
        $data = [
            'title' => 'Download',
        ];
        return view('download', $data);
    }

    public function forget_passwd()
    {
        $data = [
            'title' => 'Forget Password',
        ];
        return view('forget', $data);
    }

    public function new_account()
    {
        $data = [
            'title' => 'Account request'
        ];
        return view('new_acc', $data);
    }

    public function content()
    {
        $data = [
            'title' => 'Content'
        ];
        return view('Contents.index', $data);
    }

    public function content_manager()
    {
        $data = [
            'title' => 'Content Edit'
        ];
        return view('Contents.edit', $data);
    }

    public function brand()
    {
        $data = [
            'title' => 'Brand'
        ];
        return view('Brand.index', $data);
    }

    public function brand_manager()
    {
        $data = [
            'title' => 'Brand Edit'
        ];

        return view('Brand.edit', $data);
    }

    public function category()
    {
        $data = [
            'title' => 'Brand Edit'
        ];
        return view('Category.index', $data);
    }

    public function category_man()
    {
        $data = [
            'title' => 'Brand Edit'
        ];
        return view('Category.edit', $data);
    }

    public function fields()
    {
        $data = [
            'title' => 'Brand Edit'
        ];
        return view('Fields.index', $data);
    }

    public function fields_man()
    {
        $data = [
            'title' => 'Brand Edit'
        ];
        return view('Fields.edit', $data);
    }

    public function member()
    {
        $data = [
            'title' => 'Brand Edit'
        ];
        return view('Members.index', $data);
    }

    public function member_man()
    {
        $data = [
            'title' => 'Brand Edit'
        ];
        return view('Members.edit', $data);
    }

    public function member_group()
    {
        $data = [
            'title' => 'Brand Edit'
        ];
        return view('Members_group.index', $data);
    }

    public function member_group_man()
    {
        $data = [
            'title' => 'Brand Edit'
        ];
        return view('Members_group.edit', $data);
    }

    public function mail_test()
    {

//        dd('12345');
        $mail_to = 'vincent.ubestream@gmail.com';
        $result = Mail::to($mail_to)->send(new loginver());
        dd($result);
    }
}
