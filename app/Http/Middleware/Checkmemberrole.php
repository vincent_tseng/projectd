<?php

namespace App\Http\Middleware;

use \Illuminate\Http\Request;
use Closure;

class Checkmemberrole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $role = [
            'content' => 'CTM',
            'brand' => 'BM',
            'category' => 'CAM',
            'field' => 'FM',
            'member' => 'MM',
            'member_group' => 'MM',
            'systemlog' => 'SL',
            'systemsetting' => 'SS'
        ];

        $user = $request->session()->get('user');

        $path = $request->path();

        if(!in_array('ALL',$user['member_group']['allow_manager']))
        {
            $arr_path = explode('/',$path);
            if(!in_array($role[$arr_path[0]],$user['member_group']['allow_manager']))
            {
                return redirect(route('download'));
            }
        }



        return $next($request);
    }
}
