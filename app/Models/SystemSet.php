<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SystemSet extends Model
{
    use HasFactory;
    protected $table = 'system_set';
    protected $primaryKey = 'id';
}
