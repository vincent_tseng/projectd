<?php
namespace App\Repositories;

use App\Models\Members;
use App\Models\MemberGroup;
use Illuminate\Support\Facades\Session;

use DB;

class MemberRepository
{
    private $_members;
    private $_member_group;

    public function __construct(
        Members $member,
        MemberGroup $memberGroup
    )
    {
        $this->_members = $member;
        $this->_member_group = $memberGroup;
    }

    public function get_members()
    {
        if(in_array('ALL',Session::get('user')['member_group']['allow_manager']))
        {
            $members = $this->_members->paginate(10);
        }
        else
        {
            $members = $this->_members->where('is_delete',0)->paginate(10);
        }


        return $members;
    }

    public function member_check($account)
    {
        $result = $this->_members->where('member_email',$account)->get()->first();

        if($result)
        {
            return 'existed';
        }
        else
        {
            return 'empty';
        }

    }

    public function check_group_name($params)
    {
        $result = $this->_member_group->where('member_group_name',$params)->get()->first();

        if($result)
        {
            return 'existed';
        }
        else
        {
            return 'empty';
        }
    }

    public function get_member_by_search($params)
    {
        $member_groups = false;
        $txt_search = false;

        if($params['member_groups'] != 'ALL')
        {
            $member_groups = $params['member_groups'];
        }

        if(isset($params['member_status']))
        {
            $member_status = $params['member_status'];
        }
        else
        {
            $member_status = '';
        }

        if(isset($params['is_delete']))
        {
            $member_delete = $params['is_delete'];
        }
        else
        {
            $member_delete = '0';
        }

        if(!is_null($params['search_txt']))
        {
            $txt_search = $params['search_txt'];
        }

        $members = $this->_members
            ->when($txt_search,function($query,$txt_search){
                return $query->where('member_name','like', "%$txt_search%");
            })
            ->when($member_groups,function($query,$member_groups){
                return $query->where('member_group_id',$member_groups);
            })
            ->when($member_status =='on',function($query){
                return $query->where('member_status','on');
            })
            ->when($member_delete == '0', function($query){
                return $query->where('is_delete','0');
            })
            ->when($member_delete == '1', function($query){
               return $query->where('is_delete','1');
            })
            ->when($member_status == '0',function($query){
                return $query->whereNull('member_status');
            });

            if(in_array('ALL',Session::get('user')['member_group']['allow_manager']))
            {
                $members = $members->paginate(10);
            }
            else
            {
                $members = $members->where('is_delete',0)->paginate(10);
            }

        return $members;
    }

    public function get_member_byemail($email)
    {
        $member = $this->_members->where('member_email',$email)
                  ->get()
                  ->first();

        if($member)
        {
            $member = $member->toArray();
            $member_role = $this->get_memberGroup($member['member_group_id']);
            $member['member_group_role'] = json_decode($member_role['member_group_role'],true);
            $member['member_group_access'] = $member_role['is_public'];
            return $member;
        }

        return false;
    }

    public function get_member($id)
    {
        $member = $this->_members->where('id',$id)->get()->first()->toArray();

        return $member;
    }

    public function insert_member($params)
    {
        $result = $this->_members->insert($params);
        if($result)
        {
            return true;
        }
        return false;
    }

    public function update_member($params)
    {
        $id = $params['id'];
        $result = $this->_members->where(['id'=>$id])->update($params);

        if($result)
        {
            return true;
        }

        return false;
    }

    public function get_memberGroups()
    {
        $memberGroups = $this->_member_group->get();

        return $memberGroups;
    }

    public function get_memberGroup_by_newAccount()
    {
        $memberGroups = $this->_member_group->where('member_group_name','New Account')->get()->first();
        if($memberGroups)
        {
            return $memberGroups->toArray();
        }

        return ['id'=>0];
    }

    public function get_memberGroup($id)
    {
        $memberGroup = $this->_member_group->where('id',$id)->get()->first();
        if($memberGroup)
        {
            $memberGroup = $memberGroup->toArray();
            return $memberGroup;
        }

        return [];
    }

    public function insert_membergroup($params)
    {
        $result = $this->_member_group->insert($params);
        if($result)
        {
            return true;
        }
        return false;
    }

    public function update_membergroup($params)
    {
        $id = $params['id'];
        $result = $this->_member_group->where(['id'=>$id])->update($params);

        if($result)
        {
            return true;
        }

        return false;
    }

    public function get_member_by_token($token)
    {
        $member = $this->_members->where('member_token',$token)->get()->first();
        if($member)
        {
            $member = $member->toArray();
            $member_role = $this->get_memberGroup($member['member_group_id']);
            $member['member_group_role'] = json_decode($member_role['member_group_role'],true);
            $member['access_control'] = $member_role['is_public'];
            $member['member_group_name'] = $member_role['member_group_name'];
            $this->update_member_token('',$member['id']);
            return $member;
        }

        return false;
    }

    public function sp_login_check($params)
    {
        $member = $this->_members->where('member_email',$params['email'])->get()->first();

        if($member)
        {
            $member = $member->toArray();
            $member_role = $this->get_memberGroup($member['member_group_id']);
            $member['member_group_role'] = json_decode($member_role['member_group_role'],true);
            $member['access_control'] = $member_role['is_public'];
            $member['member_group_name'] = $member_role['member_group_name'];
            return $member;
        }

        return false;
    }

    public function reset_passwd($user_id, $arr_passwd)
    {
        $result = $this->_members->where(['id'=> $user_id])->update($arr_passwd);

        if($result)
        {
            return true;
        }

        return false;
    }

    public function update_member_token($token, $userid)
    {
        $id = $userid;
        $result = $this->_members->where(['id'=>$id])->update(['member_token' => $token]);

        if($result)
        {
            return true;
        }
    }

    public function delete($id,$type)
    {
        if($type == 'rule')
        {
            $result = $this->_member_group->where('id',$id)->delete();
        }
        else
        {
//            $result = $this->_members->where('id',$id)->delete();
            $result = $this->_members->where('id',$id)->update(['is_delete' => 1]);
        }

        return $result;
    }

    public function restore($id)
    {
        $result = $this->_members->where('id',$id)->update(['is_delete' => 0]);

        return $result;
    }
}
