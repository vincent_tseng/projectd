<?php

namespace App\Repositories;
use App\Models\Category;

class CategoryRepository
{
    public function __construct(Category $category)
    {
        $this->_category = $category;
    }


    public function get_categorys()
    {
        $categorys = $this->_category->orderby('category_order')->get()->toArray();

        return $categorys;
    }

    public function get_category ($idx)
    {
        $category = $this->_category->where('id',$idx)->get()->first();
        if($category)
        {
            $category = $category->toArray();
        }

        return $category;
    }

    public function check_category_name($params)
    {
        $result = $this->_category->where('category_name',$params)->get()->first();

        if($result)
        {
            return 'existed';
        }
        else
        {
            return 'empty';
        }
    }

    public function get_category_maxorder()
    {
        $max_order = $this->_category->max('category_order');

        return $max_order;
    }

    public function insert_category($params)
    {
        $result = $this->_category->insert($params);
        if($result)
        {
            return true;
        }
        return false;
    }

    public function update_category($params)
    {
        $id = $params['id'];
        $result = $this->_category->where(['id'=>$id])->update($params);

        if($result)
        {
            return true;
        }

        return false;
    }

    public function update_category_order($params)
    {
        foreach($params as $item)
        {
            $this->_category->where('id',$item['category_id'])->update(['category_order' => $item['neworder']]);
        }

        return true;
    }

    public function delete($id)
    {
        $result = $this->_category->where('id',$id)->delete();

        return $result;
    }
}
