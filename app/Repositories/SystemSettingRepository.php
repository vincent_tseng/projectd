<?php
namespace App\Repositories;

use App\Models\SystemSet;

class SystemSettingRepository
{
    public function __construct(SystemSet $systemSet)
    {
        $this->_setting = $systemSet;
    }

    public function get_value()
    {
        $result = $this->_setting->get()->toArray();

        return $result;
    }

    public function update_value($params)
    {
        $tmp_params = $params;

        unset($tmp_params['_token']);
        $result = $this->_setting->where('system_key','url_pi')->get();

        if(count($result) == 0 )
        {
            $this->_setting->insert([
                'system_key' => 'url_pi',
                'system_value' => $tmp_params['url_pi']
            ]);
        }
        if(!isset($params['maintain']))
        {
            $tmp_params['maintain'] = null;
        }

        foreach ($tmp_params as $key => $items) {
            $this->_setting->where('system_key',$key)->update(['system_value' => $items]);
        }

        return true;
    }

    private function _insert_value()
    {

    }
}
