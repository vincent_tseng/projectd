<?php
namespace App\Repositories;

use App\Models\Content;
use DB;

class ContentRepository
{
    public function __construct(
        Content $content
    )
    {
        $this->_content = $content;
    }

    public function get_contents()
    {
        $contents = $this->_content->where('is_delete',0)->paginate(10);

        return $contents;
    }

    public function check_title($params)
    {
        $result = $this->_content->where('content_title',$params)->get()->first();

        if($result)
        {
            return 'existed';
        }
        else
        {
            return 'empty';
        }
    }

    public function get_content($idx)
    {
        $content = $this->_content->where('id',$idx)->get()->first()->toArray();

        return $content;
    }

    public function get_file_path($params)
    {
        $contents = $this->_content->whereIn('id',$params)->get()->toArray();
        return $contents;
    }

    public function get_signle_file($download_id)
    {
        $contents = $this->_content->where('id',$download_id)->get()->first()->toArray();

        return $contents;
    }

    public function get_public_content($user)
    {
        if($user['member_group']['brands'][0] =="ALL")
        {
            $content = $this->_content->where(['is_public'=>'on', 'is_delete'=>0])->paginate(10);
        }
        else
        {
            $brands = $user['member_group']['brands'];
            $content = $this->_content->where(['is_public' => 'on', 'is_delete' => 0])->whereIn('content_brand_id', $brands)->paginate(10);
        }

//        dd($content);

        return $content;
    }

    public function get_content_by_search($params)
    {
        $brand = false;
        $category = false;
        $txt_search = false;

        $public = $params['s_status'];

        if($params['s_brand'] != 0)
        {
            $brand = $params['s_brand'];
        }

        if($params['s_category'] != 0 )
        {
            $category = $params['s_category'];
        }

        if(!is_null($params['txt_search']))
        {
            $txt_search = $params['txt_search'];
        }

        $contents = $this->_content
            ->when($txt_search,function($query,$txt_search){
                return $query->where('content_title','like', "%$txt_search%")->orwhere('content_note','like', "%$txt_search%");
            })
            ->when($public == 'on',function($query,$public){
                return $query->where('is_public','on');
            })
            ->when($public == "0", function($query){
                return $query->whereNull('is_public');
            })
            ->when($brand,function($query,$brand){
                return $query->where('content_brand_id',$brand);
            })
            ->when($category,function($query,$category){
                return $query->where('content_category_id',$category);
            })
            ->where('is_delete',0)->paginate(10);
//        ->toSql();

//        dd($contents);

        return $contents;
    }

    public function insert_content($params)
    {
        $result = $this->_content->insert($params);
        if($result)
        {
            return true;
        }
        return false;
    }

    public function update_content($params)
    {
        $id = $params['id'];
        $result = $this->_content->where(['id'=>$id])->update($params);

        if($result)
        {
            return true;
        }

        return false;
    }

    public function get_content_by_download($params)
    {
        $brand = false;
        $category = false;
        $txt_search = false;

        if(count($params['brand_id']) > 0)
        {
            $brand = $params['brand_id'];
        }

        if(count($params['category_id']) > 0 )
        {
            $category = $params['category_id'];
        }

        if(!is_null($params['download_search']))
        {
            $txt_search = $params['download_search'];
        }

        $contents = $this->_content
            ->when($txt_search,function($query,$txt_search){
                return $query->where('content_title','like', "%$txt_search%")->orwhere('content_note','like', "%$txt_search%");
            })
            ->when($brand,function($query,$brand){
                return $query->whereIn('content_brand_id',$brand);
            })
            ->when($category,function($query,$category){
                return $query->whereIn('content_category_id',$category);
            })
            ->where('is_public','on')->where('is_delete',0)->paginate(10);
//        dd($contents);
        return $contents;

    }

    public function delete($id)
    {
//        $result = $this->_content->where('id',$id)->update(['is_delete'=>1]);
        $result = $this->_content->where('id',$id)->delete();

        return $result;
    }

    public function delete_pic($id)
    {
        $result = $this->_content->where('id',$id)->update(['content_sample_pic'=> '']);

        return $result;
    }

    public function download_path($token,$id)
    {
        $this->_content->where('id',$id)->update(['content_temp_download_path'=>$token]);
    }

    public function get_file_path_bytoken($token)
    {
        $result = $this->_content->where('content_temp_download_path',$token)->get()->first();

        if($result)
        {
            $result = $result->toArray();

            $files = [
                'content_id' => $result['id'],
                'content_upload_path' => $result['content_upload_path']
            ];

            //清空token
            $this->_content->where('content_temp_download_path',$token)->update(['content_temp_download_path' => '']);

            return $files;
        }
    }
}
