<?php
namespace App\Repositories;

use App\Models\Field;

class FieldsRepository
{
    public function __construct(
        Field $field
    )
    {
        $this->_field = $field;
    }

    public function get_fields()
    {
        $fields = $this->_field->orderby('field_order')->get()->toArray();

        return $fields;
    }

    public function get_field($idx)
    {
        $field = $this->_field->where('id',$idx)->get()->first();

        if($field)
        {
            $field = $field->toArray();
        }

        return $field;
    }

    public function get_field_maxorder()
    {
        $max_oreder = $this->_field->max('field_order');

        return $max_oreder;
    }

    public function check_field($params)
    {
        $result = $this->_field->where('field_name',$params)->get()->first();

        if($result)
        {
            return 'existed';
        }
        else
        {
            return 'empty';
        }
    }

    public function insert_field($params)
    {
        $result = $this->_field->insert($params);
        if($result)
        {
            return true;
        }
        return false;
    }

    public function update_field($params)
    {
        $id = $params['id'];
        $result = $this->_field->where(['id'=>$id])->update($params);

        if($result)
        {
            return true;
        }

        return false;
    }

    public function update_field_order($params)
    {
        foreach($params as $item)
        {
            $this->_field->where('id',$item['field_id'])->update(['field_order' => $item['neworder']]);
        }

        return true;
    }

    public function delete($id)
    {
        $result = $this->_field->where('id',$id)->delete();

        return $result;
    }
}
