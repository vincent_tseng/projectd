<?php
namespace App\Repositories;

use App\Models\SystemLog;

class SystemLogRepository
{
    public function __construct(SystemLog $systemLog)
    {
        $this->_systemlog = $systemLog;
    }

    public function get_logs()
    {
        $logs = $this->_systemlog->orderByDesc('id')->paginate(10);

        return $logs;
    }

    public function get_logs_by_search($params)
    {
        $log_type = false;

        if($params['log_type'] != 'ALL')
        {
            $log_type = $params['log_type'];
        }

        $logs = $this->_systemlog
            ->when($log_type,function($query,$log_type){
                return $query->where('log_type',$log_type);
            })
            ->paginate(10);

        return $logs;
    }
}
