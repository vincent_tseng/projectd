<?php

namespace App\Repositories;

use App\Models\Brand;

class BrandRepository
{
    public function __construct(Brand $brand)
    {
        $this->brand = $brand;
    }


    public function get_brands()
    {
        $brands = $this->brand->orderby('brand_order')->paginate(10);

        return $brands;
    }

    public function get_brand ($idx)
    {
        $brand = $this->brand->where('id',$idx)->get()->first();
        if($brand)
        {
            $brand = $brand->toArray();
        }

        return $brand;
    }

    public function get_public_brands($user)
    {
        if($user['member_group']['brands'][0] == 'ALL')
        {
            $brands = $this->brand->where('is_public','on')->orderby('brand_order')->get()->toArray();
        }
        else
        {
            $brands = $user['member_group']['brands'];
            $brands = $this->brand->where('is_public','on')->whereIn('id', $brands)->orderby('brand_order')->get()->toArray();
        }

        return $brands;
    }

    public function get_brand_maxorder()
    {
        $max_oreder = $this->brand->max('brand_order');

        return $max_oreder;
    }

    public function insert_brand($params)
    {
        $result = $this->brand->insert($params);
        if($result)
        {
            return true;
        }
        return false;
    }

    public function update_brand($params)
    {
        $id = $params['id'];
        $result = $this->brand->where(['id'=>$id])->update($params);

        if($result)
        {
            return true;
        }

        return false;
    }

    public function update_brand_order($params)
    {
        foreach($params as $item)
        {
            $this->brand->where('id',$item['brand_id'])->update(['brand_order' => $item['neworder']]);
        }

        return true;
    }

    public function check_brand_name($params)
    {
        $result = $this->brand->where('brand_name',$params)->get()->first();

        if($result)
        {
            return 'existed';
        }
        else
        {
            return 'empty';
        }
    }

    public function delete($id)
    {
        $result = $this->brand->where('id',$id)->delete();

        return $result;
    }
}
