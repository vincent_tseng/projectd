<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Loginver extends Mailable
{
    use Queueable, SerializesModels;

    public $token;
    public $member_name;
    public $email;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($token, $member_name, $service_email)
    {
        //
        $this->token = $token;
        $this->member_name = $member_name;
        $this->email = $service_email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Gogoro Digital Brand Hub Verification link')->view('emails.logincheck');
    }
}
