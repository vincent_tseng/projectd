<?php

use Illuminate\Support\Facades\Route;

//Controllers
use App\Http\Controllers\LoginController;
use App\Http\Controllers\ContentController;
use App\Http\Controllers\BrandController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\FieldsController;
use App\Http\Controllers\MemberGroupController;
use App\Http\Controllers\MemberController;
use App\Http\Controllers\SystemLogController;
use App\Http\Controllers\SystemSettingController;
use App\Http\Controllers\MainController;


//測試用Controller
use App\Http\Controllers\TestController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',[LoginController::class,'index'])->name('index.login');
Route::post('/login',[LoginController::class,'action_login']);
Route::get('/logout',[LoginController::class,'logout']);
Route::get('/token/{token}',[LoginController::class,'verify_token']);

Route::get('/thanks',[LoginController::class,'thanks']);


Route::get('/partner',[LoginController::class,'account_request']);
Route::post('/partner',[LoginController::class,'action_request']);

Route::post('/partner/check_member',[LoginController::class,'action_member_check']);
Route::get('/tmp_download/{token}',[MainController::class,'temp_download']);

Route::prefix('forget_passwd')->group(function(){
    Route::get('/',[LoginController::class,'forger_passwd']);
    Route::post('/',[LoginController::class,'action_reset_passwd']);

    Route::get('/setting/{token}',[LoginController::class,'setting_passwd']);
    Route::post('/setting',[LoginController::class,'action_setting_passwd']);
});


//下載頁面處理
Route::prefix('download')->middleware('Checkloginstatus')->group(function(){
    Route::get('/',[MainController::class,'index'])->name('download');
    Route::post('/',[MainController::class,'action_search']);
    Route::post('/files',[MainController::class,'file_download']);
    Route::get('/single/{download_id}',[MainController::class,'single_file_download']);
});

//內容管理
Route::prefix('content')->middleware('Checkloginstatus','Checkmemberrole')->group(function() {
    Route::get('/index',[ContentController::class,'index'])->name('Contents.index');
    Route::post('/index',[ContentController::class,'content_search']);

    Route::get('/new',[ContentController::class,'insert_content'])->name('Contents.new');
    Route::post('/new',[ContentController::class,'action_insert_content']);

    Route::get('/modify/{idx}',[ContentController::class,'update_content'])->name('Contents.edit');
    Route::post('/modify',[ContentController::class,'action_update_content']);

    Route::post('/check',[ContentController::class,'check_title']);

    Route::post('/delete',[ContentController::class,'action_delete']);
    Route::post('/delete/pic',[ContentController::class,'action_pic_delete']);

    Route::post('/create_link',[ContentController::class,'action_get_temp_download_path']);
});

//品牌分類
Route::prefix('brand')->middleware('Checkloginstatus','Checkmemberrole')->group(function() {
    Route::get('/index',[BrandController::class,'index'])->name('brand.index');

    Route::get('/new',[BrandController::class,'insert_brand'])->name('brand.new');
    Route::post('/new',[BrandController::class,'action_insert_brand']);

    Route::get('/modify/{idx}',[BrandController::class,'update_brand'])->name('brand.edit');
    Route::post('/modify',[BrandController::class,'action_update_brand']);

    Route::post('/order',[BrandController::class,'action_order_brand']);

    Route::post('/check',[BrandController::class,'check_brand_name']);

    Route::post('/delete',[BrandController::class,'action_delete']);
});

//分類
Route::prefix('category')->middleware('Checkloginstatus','Checkmemberrole')->group(function(){
    Route::get('/index',[CategoryController::class,'index'])->name('category.index');

    Route::get('/new',[CategoryController::class,'insert_category'])->name('category.new');
    Route::post('/new',[CategoryController::class,'action_insert_category']);

    Route::get('/modify/{idx}',[CategoryController::class,'update_category'])->name('category.edit');
    Route::post('/modify',[CategoryController::class,'action_update_category']);

    Route::post('/order',[CategoryController::class,'action_category_order']);

    Route::post('/check',[CategoryController::class,'check_category']);

    Route::post('/delete',[CategoryController::class,'action_delete']);
});

//欄位
Route::prefix('field')->middleware('Checkloginstatus','Checkmemberrole')->group(function(){
    Route::get('/index',[FieldsController::class,'index'])->name('field.index');

    Route::get('/new',[FieldsController::class,'insert_fields'])->name('field.new');
    Route::post('/new',[FieldsController::class,'action_insert_fields']);

    Route::get('/modify/{idx}',[FieldsController::class,'update_fields'])->name('field.edit');
    Route::post('/modify',[FieldsController::class,'action_update_fields']);

    Route::post('/order',[FieldsController::class,'action_fields_order']);

    Route::post('/check',[FieldsController::class,'check_field_name']);

    Route::post('/delete',[FieldsController::class,'action_delete']);
});

//member
Route::prefix('member')->middleware('Checkloginstatus','Checkmemberrole')->group(function(){
    Route::get('/index',[MemberController::class,'index'])->name('members.index');
    Route::post('/index',[MemberController::class,'member_search']);

    Route::get('/new',[MemberController::class,'insert_member'])->name('members.new');
    Route::post('/new',[MemberController::class,'action_insert_member']);

    Route::get('/modify/{idx}',[MemberController::class,'update_member'])->name('members.edit');
    Route::post('/modify',[MemberController::class,'action_update_member']);

    Route::post('/check_member',[LoginController::class,'action_member_check']);

    Route::post('/delete',[MemberController::class,'action_delete']);

    Route::post('/restore',[MemberController::class,'action_restore']);
});

//member權限分類
Route::prefix('member_group')->middleware('Checkloginstatus','Checkmemberrole')->group(function(){
    Route::get('/index',[MemberGroupController::class,'index'])->name('member_group.index');

    Route::get('/new',[MemberGroupController::class,'insert_member_group'])->name('member_group.new');
    Route::post('/new',[MemberGroupController::class,'action_insert_member_group']);

    Route::get('/modify/{idx}',[MemberGroupController::class,'update_member_group'])->name('member_group.edit');
    Route::post('/modify',[MemberGroupController::class,'action_update_member_group']);

    Route::post('/check',[MemberGroupController::class,'check_group_name']);

    Route::post('/delete',[MemberGroupController::class,'action_delete']);
});

//systemlog
Route::prefix('systemlog')->middleware('Checkloginstatus','Checkmemberrole')->group(function(){
    Route::get('/index',[SystemLogController::class,'index'])->name('systemlog.index');
    Route::post('/index',[SystemLogController::class,'log_search']);
});

//systemsetting
Route::prefix('systemsetting')->middleware('Checkloginstatus','Checkmemberrole')->group(function(){
    Route::get('/index',[SystemSettingController::class,'index'])->name('systemsetting.index');
    Route::post('/index',[SystemSettingController::class,'action_update']);
});

//頁面處理測試
Route::prefix('test')->group(function(){
    Route::get('/index',[TestController::class,'index']);
    Route::get('/forget',[TestController::class,'forget_passwd']);//新增忘記密碼頁面
    Route::get('/new_account',[TestController::class,'new_account']);//新增要求註冊頁面
    Route::get('/download',[TestController::class,'download']);

    Route::get('/content',[TestController::class,'content']);
    Route::get('/content_man',[TestController::class,'content_manager']);

    Route::get('/brand',[TestController::class,'brand']);
    Route::get('/brand_man',[TestController::class,'brand_manager']);

    Route::get('/category',[TestController::class,'category']);
    Route::get('/category_man',[TestController::class,'category_man']);

    Route::get('/fields',[TestController::class,'fields']);
    Route::get('/fields_man',[TestController::class,'fields_man']);

    Route::get('/member',[TestController::class,'member']);
    Route::get('/member_man',[TestController::class,'member_man']);

    Route::get('/member_group',[TestController::class,'member_group']);
    Route::get('/member_group_man',[TestController::class,'member_group_man']);

    Route::get('/mail/test',[TestController::class,'mail_test']);
});
